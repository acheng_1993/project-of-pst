
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_1按业务模块生成(泛型主键),不向前兼容 添加时间:2024/2/20 14:22:04
using System;
using CodeCheck.Service.Core;
namespace CodeCheck.Service.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    [MyTableName("CodeRecord")]
    [MyPrimaryKey("Id",AutoIncrement =false)]
    public class CodeRecordEntity: BaseField,IEntity<long>
    {

       public  CodeRecordEntity()
       {
                    Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
                         }
       public long Id{get;set;}
            /// <summary>
        ///  条码
        /// </summary>
                  public string Code {get;set;}
        
          /// <summary>
        ///  条码时间
        /// </summary>
                  public DateTime? CodeTime {get;set;}
        
          /// <summary>
        ///  状态1：OK，2：重复
        /// </summary>
                  public int? Status {get;set;}
        
          /// <summary>
        ///  预留1
        /// </summary>
                  public string Attribute1 {get;set;}
        
          /// <summary>
        ///  预留2
        /// </summary>
                  public string Attribute2 {get;set;}
        
          /// <summary>
        ///  预留3
        /// </summary>
                  public string Attribute3 {get;set;}
        
     

    }

   
}
