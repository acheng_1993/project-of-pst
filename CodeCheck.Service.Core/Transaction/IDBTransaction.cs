﻿using System;

namespace CodeCheck.Service.Core
{
    public interface IDBTransaction : IDisposable
    {
        void Complete();
    }
}
