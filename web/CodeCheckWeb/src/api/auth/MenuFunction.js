import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'MenuFunction'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class MenuFunction {
  getlist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  insertMenuFunction(query) {
    return ajaxService.post(UserRouter, 'insert', query)
  }
  deleteMenuFunction(query) {
    return ajaxService.delete(UserRouter, 'delete', query)
  }
  saveMenu(query) {
    return ajaxService.post(UserRouter, 'SaveMenu', query)
  }
}

export default MenuFunction
