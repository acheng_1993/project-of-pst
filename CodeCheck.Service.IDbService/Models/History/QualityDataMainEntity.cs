
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键) 添加时间:2023/7/18 10:24:09
using SqlSugar;
using CodeCheck.Service.Core;
namespace CodeCheck.Service.IDbService
{
    /// <summary>
    /// 质量数据总表
    /// </summary>
    [SugarTable("DataCollection_QualityDataMain")]
    public class QualityDataMainEntity: BaseField
    {

       public  QualityDataMainEntity()
       {
                    Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
                         }
         [SugarColumn(ColumnName = "Id", IsPrimaryKey = true)]
       public long Id{get;set;}
            /// <summary>
        ///  线体编码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string LineCode {get;set;}
        
          /// <summary>
        ///  产线名称
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string LineName {get;set;}
        
          /// <summary>
        ///  工序编码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string ProcessCode {get;set;}
        
          /// <summary>
        ///  工步编码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string StepCode {get;set;}
        
          /// <summary>
        ///  工步名称
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string StepName {get;set;}
        
          /// <summary>
        ///  工序名称
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string ProcessName {get;set;}
        
          /// <summary>
        ///  工位编码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string StationCode {get;set;}
        
          /// <summary>
        ///  工站名
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string StationName {get;set;}
        
          /// <summary>
        ///  条码类型3 电芯\模组\PACK
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string BarCodeType3 {get;set;}
        
          /// <summary>
        ///  条码类型3名称
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string BarCodeTypeName {get;set;}
        
          /// <summary>
        ///  条码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string ProcessBarCode {get;set;}
        
          /// <summary>
        ///  托盘码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string TrayCode {get;set;}
        
          /// <summary>
        ///  工单号
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string WONo {get;set;}
        
          /// <summary>
        ///  NG类型编号
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string NGTypeCode {get;set;}
        
          /// <summary>
        ///  NG类型名称
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string NGTypeName {get;set;}
        
          /// <summary>
        ///  参数质量状态1：OK,2:NG
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public int? QualityStatus {get;set;}
        
     

    }

   
}
