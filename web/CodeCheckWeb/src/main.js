import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import i18n from './lang' // Internationalization
import './icons' // icon
import './permission' // permission control
import './utils/errorLog' // error log

import * as filters from './filters' // global filters

// import { mockXHR } from '../mock' // simulation data

// mock api in github pages site build
// if (process.env.NODE_ENV === 'production') { mockXHR() }

import buttonPermission from './directive/permission' // 按钮权限

import './assets/icon/iconfont.js'
// eslint-disable-next-line new-cap
// const signalRClientClass = new signalRClient(common.signalrUrl)
import './assets/icon/iconfont.css'
import 'lib-flexible/flexible.js'
Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})

// register global utility filters
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})

Vue.use(buttonPermission)
// Vue.prototype.SignalRClient = signalRClientClass.Connected()
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: (h) => h(App)
})
