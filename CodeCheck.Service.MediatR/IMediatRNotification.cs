﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeCheck.Service.MediatR
{
    public interface IMediatRNotification : INotification
    {
    }
}
