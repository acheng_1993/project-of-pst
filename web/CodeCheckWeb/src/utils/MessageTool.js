import { MessageBox, Message, Notification } from 'element-ui'

class MessageTool {
  NotifySuccessShow(message, title = '成功', duration = 2000) {
    Notification({
      title: title,
      message: message,
      type: 'success',
      duration: duration
    })
  }
  NotifyErrorShow(message, title = '失败', duration = 2000) {
    Notification({
      title: title,
      message: message,
      type: 'error',
      duration: duration
    })
  }
  MessageInfoShow(message) {
    Message({
      type: 'info',
      message: message
    })
  }
  ConfirmShow(message, title = '提示') {
    return MessageBox.confirm(message, title, {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    })
  }
  PromptShow(message, value, errorMessage) {
    return MessageBox.prompt(message, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      inputValidator: function(v) {
        if (v !== value) {
          return errorMessage
        }
      },
      inputErrorMessage: ''
    })
  }
}
export default MessageTool
