import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const errorMessageRouter = 'ErrorMessage'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class ErrorMessage {
  getErrorMessageGetListPaged(query) {
    return ajaxService.get(errorMessageRouter, 'GetListPaged', query)
  }
  getErrorMessageGetList(query) {
    return ajaxService.get(errorMessageRouter, 'GetList', query)
  }
  createErrorMessage(model) {
    return ajaxService.post(errorMessageRouter, 'insert', model)
  }
  updateErrorMessage(model) {
    return ajaxService.post(errorMessageRouter, 'UpdateById/' + model.id, model)
  }
  deleteErrorMessage(model) {
    return ajaxService.delete(errorMessageRouter, 'delete', model.id)
  }
  batchUpdateErrorMessage(model) {
    return ajaxService.post(errorMessageRouter, 'UpdateBatch', model)
  }
  getErrorMessage(model) {
    return ajaxService.get(errorMessageRouter, 'Get', { id: model.id })
  }
  deleteBatchErrorMessage(ids) {
    return ajaxService.delete(errorMessageRouter, 'deleteBatch', ids)
  }
}

export default ErrorMessage
