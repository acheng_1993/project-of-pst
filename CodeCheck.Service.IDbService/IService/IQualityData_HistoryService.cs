﻿

using CodeCheck.Service.IDbService.Models;

namespace CodeCheck.Service.IDbService
{
    public interface IQualityData_HistoryService : IBaseService<QualityData_History>
    {

    }
}