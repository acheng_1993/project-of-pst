import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'System'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class SystemManage {
  getSystemPagelist(query) {
    return ajaxService.get(UserRouter, 'GetListPaged', query)
  }
  getSystemlist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  insertSystemManage(query) {
    return ajaxService.post(UserRouter, 'insert', query)
  }
  updateSystemManage(query) {
    return ajaxService.post(UserRouter, 'UpdateById/' + query.id, query)
  }
  deleteSystemManage(query) {
    return ajaxService.delete(UserRouter, 'delete', query)
  }
  deleteBatchSystemManage(query) {
    return ajaxService.delete(UserRouter, 'DeleteBatch', query)
  }
  copySystemManage(query) {
    return ajaxService.get(UserRouter, 'CopySystem?id=' + query, query)
  }
}

export default SystemManage
