import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'UserScreenFunction'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class UserScreenFunction {
  getUserScreenFunctionlist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  saveUserScreenMenu(query) {
    return ajaxService.post(UserRouter, 'SaveUserScreenMenu', query)
  }
  saveUserScreenMenuByFunctionids(query) {
    return ajaxService.post(UserRouter, 'SaveUserScreenMenuByFunctionids', query)
  }
}

export default UserScreenFunction
