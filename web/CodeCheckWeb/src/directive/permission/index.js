export default {
  install(Vue) {
    Vue.directive('permission', {
      inserted(el, binding, vnode) {
        const { value } = binding
        if (value && Array.isArray(value) && value.length > 0) {
          const ownMenuFunctions = JSON.parse(sessionStorage.getItem('menuFunctions'))
          const menuCode = vnode.context.$route.name
          const menuFunctions = getMenuFuctions(ownMenuFunctions, menuCode)
          const hasPermission = arrayHasAny(menuFunctions, value)
          if (!hasPermission) {
            if (binding.modifiers.hidden) {
              el.style.display = 'none'
              return
            }
            el.parentNode && el.parentNode.removeChild(el)
          }
        } else {
          throw new Error(`need permissions! Like v-permission="['xxxx']"`)
        }
      }
    })
  }
}

/**
 * 当前菜单下的功能权限
 * @param array {Array<String>} 数组
 * @param obj {Array<String>} 值
 * @returns {boolean}
 */
function getMenuFuctions(menus, menuCode) {
  const arr = [];
  (function fn(menus, menuCode) {
    for (let i = 0; i < menus.length; i++) {
      const item = menus[i]
      if (item.code === menuCode) {
        arr.push(...item.functions)
        return
      } else {
        fn(item.children, menuCode)
      }
    }
  })(menus, menuCode)

  return arr
}

/**
 * 数组是否有任意值
 * @param array {Array<String>} 数组
 * @param obj {String, Array<String>} 值
 * @returns {boolean}
 */
function arrayHasAny(array, obj) {
  if (!obj) {
    return true
  }
  if (!array) {
    return false
  }
  if (Array.isArray(obj)) {
    for (let i = 0; i < obj.length; i++) {
      if (array.some(item => { return item.code === obj[i] })) {
        return true
      }
    }
    return false
  }
  return array.indexOf(obj) !== -1
}
