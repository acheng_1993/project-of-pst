import request from '@/utils/ajaxService'
import Vue from 'vue'

export function login(data) {
  // return request({
  //   url: '/user/login',
  //   method: 'post',
  //   data:data
  // })
  return request({
    url: Vue.prototype.BaseConfig.tokenApiUrl, // + '/User/Login',
    method: 'post',
    data: data

  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function getUserMenus(token) {
  return request({
    url: Vue.prototype.BaseConfig.authApiUrl + '/User/GetUserInfoByToken',
    method: 'get',
    params: { token: token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
