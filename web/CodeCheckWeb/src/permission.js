import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken, getAuthToken } from '@/utils/auth' // get token from cookie
import request from '@/utils/request'
import Vue from 'vue'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()
  // determine whether the user has logged in
  // 加载url 配置
  if (!Vue.prototype.BaseConfig) {
    Vue.prototype.BaseConfig = ApiConfig
    // await request({
    //   url: '/static/config.json',
    //   method: 'get',
    // }).then(res => {
    //   //console.log(res)
    //   Vue.prototype.BaseConfig = res;
    // });
  }
  const hasToken = getAuthToken()
  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      // determine whether the user has obtained his permission roles through getInfo
      // const menuList = store.getters.menuList
      const hasMenuList =
        store.getters.menuList && store.getters.menuList.length > 0
      //  alert(hasMenuList)
      if (hasMenuList) {

        next()
      } else {
        try {

          // 根据登录获取的menu集合匹配相应的路由
           const userMenus = await store.dispatch('user/getUserMenus')
          const accessRoutes = await store.dispatch(
            'permission/generateRoutes',
            userMenus
          )
          // dynamically add accessible routes
          router.addRoutes(accessRoutes)

          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({ ...to, replace: true })
        } catch (error) {
          // alert(error)
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
