﻿using CodeCheck.Service.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeCheck.Service.Repository
{
    internal interface IScopeDBFactory
    {
        CustomDatabase GetScopeDb();
    }
}
