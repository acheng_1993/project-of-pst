

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_1按业务模块生成(泛型主键),不向前兼容 添加时间:2024/2/20 14:22:04
using System;
using System.Collections.Generic;
using NPoco;
using System.Data.SqlClient;
using System.Threading.Tasks;

using CodeCheck.Service.Core;
using CodeCheck.Service.Basic.Abstractions;
using CodeCheck.Service.Utility;


namespace CodeCheck.Service.Repository
{
    internal class CodeRecordRepositoryImpl : AbstractRepository<CodeRecordEntity,BaseCodeRecordCondition,long>,IQueryCodeRecordRepository,ICommandCodeRecordRepository<long>, IAutoInject
    {

	     public CodeRecordRepositoryImpl (IScopeDBFactory scopeDBFactory) : base(scopeDBFactory)
        {

        }

         

        
            

        
            

        
            

        
            

        
            

        
            

        
            
        public override Sql TrunConditionToSql(Sql sql,BaseCodeRecordCondition condition)
        {
            if (condition != null)
            {
                        if (!string.IsNullOrEmpty(condition.Code))
        {
            sql.Append(" And Code LIKE @Code ", new { Code= $"%{condition.Code}%"});
        }
         if (condition.CodeTimeStart.HasValue && condition.CodeTimeEnd.HasValue)
        {
            sql.Append("And (@CodeTimeStart<=CodeTime And CodeTime<=@CodeTimeEnd)", new { CodeTimeStart=condition.CodeTimeStart.Value, CodeTimeEnd=condition.CodeTimeEnd.Value});
        }
            if(condition.Status.HasValue)
            {
                 if (condition.Status > 0)
                {
                    sql.Append(" And Status=@Status", new { Status=condition.Status.Value });
                }
            }
                if (!string.IsNullOrEmpty(condition.Attribute1))
                {
                    sql.Append(" And Attribute1 LIKE @Attribute1 ", new { Attribute1= $"%{condition.Attribute1}%"});
                }
                    if (!string.IsNullOrEmpty(condition.Attribute2))
                    {
                        sql.Append(" And Attribute2 LIKE @Attribute2 ", new { Attribute2= $"%{condition.Attribute2}%"});
                    }
                        if (!string.IsNullOrEmpty(condition.Attribute3))
                        {
                            sql.Append(" And Attribute3 LIKE @Attribute3 ", new { Attribute3= $"%{condition.Attribute3}%"});
                        }
                         if (condition.CreateTimeStart.HasValue && condition.CreateTimeEnd.HasValue)
                        {
                            sql.Append("And (@CreateTimeStart<=CreateTime And CreateTime<=@CreateTimeEnd)", new { CreateTimeStart=condition.CreateTimeStart.Value, CreateTimeEnd=condition.CreateTimeEnd.Value});
                        }

            }
            return sql;
        }
    }
}

