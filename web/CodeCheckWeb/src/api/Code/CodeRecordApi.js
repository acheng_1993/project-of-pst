
import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const router = 'CodeRecord'

const vueAction = new AjaxService(ApiConfig.baseUrl)

class CodeRecordRequestApi {
  getList(query) {
    return vueAction.get(router, 'GetList', query)
  }
  getPagedList(query) {
    return vueAction.get(router, 'GetListPaged', query)
  }
  insert(model) {
    return vueAction.post(router, 'insert', model)
  }
  greenAlert(model) {
    return vueAction.post(router, 'GreenAlert', model)
  }
  redAlert(model) {
    return vueAction.post(router, 'RedAlert', model)
  }
  closeAlert(model) {
    return vueAction.post(router, 'CloseAll', model)
  }

  insert(model) {
    return vueAction.post(router, 'insert', model)
  }

  update(model) {
    return vueAction.post(router, 'UpdateById/' + model.id, model)
  }
  delete(model) {
    return vueAction.delete(router, 'delete', model.id)
  }
  deleteBatch(ids) {
    return vueAction.delete(router, 'DeleteBatch', ids)
}
exportToExcel(query) {
  return vueAction.get(router, 'ImportExcel', query)
}
}

export default CodeRecordRequestApi
