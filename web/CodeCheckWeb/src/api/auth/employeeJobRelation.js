import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const employeeJobRelationRouter = 'EmployeeJobRelation'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class employeeJobRelation {
  getEmployeeJobRelationListPaged(query) {
    return ajaxService.get(employeeJobRelationRouter, 'GetListPaged', query)
  }
  getEmployeeJobRelationById(query) {
    return ajaxService.get(employeeJobRelationRouter, 'Get', query)
  }
  getEmployeeJobRelationList(query) {
    return ajaxService.get(employeeJobRelationRouter, 'GetList', query)
  }
  createEmployeeJobRelation(model) {
    return ajaxService.post(employeeJobRelationRouter, 'insert', model)
  }
  createBatchmployeeJobRelation(model) {
    return ajaxService.post(employeeJobRelationRouter, 'InsertBatch', model)
  }
  updateEmployeeJobRelation(model) {
    return ajaxService.post(employeeJobRelationRouter, 'UpdateById/' + model.id, model)
  }
  deleteEmployeeJobRelation(model) {
    return ajaxService.delete(employeeJobRelationRouter, 'delete', model.id)
  }
  deleteBatchmployeeJobRelation(model) {
    return ajaxService.delete(employeeJobRelationRouter, 'deletebatch', model)
  }
  savemployeeJobRelation(model) {
    return ajaxService.post(employeeJobRelationRouter, 'save', model)
  }
}

export default employeeJobRelation
