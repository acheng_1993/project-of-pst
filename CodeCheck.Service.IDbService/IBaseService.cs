﻿
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CodeCheck.Service.IDbService
{
    /// <summary>
    /// 基础接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseService<T> where T : class, new()
    {
        /// <summary>
        /// 插入，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Saveable(T model);

        /// <summary>
        /// 插入，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveableReturnCommand(T model);

        /// <summary>
        /// 插入，返回类
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        T SaveableReturnEntity(T model);

        /// <summary>
        /// 根据主键判段是否存在，如果存在则更新，不存在则插入
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        int Saveable(IEnumerable<T> models);

        /// <summary>
        /// 根据主键判段是否存在，如果存在则更新，不存在则插入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateColumns">指定更新列</param>
        /// <returns></returns>
        bool Saveable(T model, Expression<Func<T, object>> updateColumns);
        
        
        /// <summary>
        /// 根据更新指定列修改,返回受影响的行数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateColumns"></param>
        /// <returns></returns>
        int SaveableReturnCommand(T model, Expression<Func<T, object>> updateColumns);

        /// <summary>
        /// 批量根据更新指定列修改,返回受影响的行数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateColumns"></param>
        /// <returns></returns>
        int SaveableReturnCommand(IEnumerable<T> model, Expression<Func<T, object>> updateColumns);

        /// <summary>
        /// 插入 ，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int InsertReturnCommand(T model);

        /// <summary>
        /// 插入 ，返回影响行数
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        int InsertRangeReturnCommand(IEnumerable<T> models);

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int UpdateReturnCommand(T model);
        
        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        int UpdateReturnCommand(T model, Expression<Func<T, T>> columns);

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int UpdateRangeReturnCommand(IEnumerable<T> model);

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        int UpdateRangeReturnCommand(IEnumerable<T> model, Expression<Func<T, T>> columns);

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="columns">更新指定列</param>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        int UpdateRangeReturnCommand(Expression<Func<T, T>> columns, Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 根据id获取
        /// </summary>
        /// <param name="objId">主键</param>
        /// <returns></returns>
        T GetById(dynamic objId);

        /// <summary>
        /// 异步根据id获取
        /// </summary>
        /// <param name="objId">主键</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(dynamic objId);

        /// <summary>
        /// 根据where条件查询单条，超过一条会报错!!!
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        T GetSingle(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="orderExpression">orderBy表达式</param>
        /// <param name="orderByType">排序类型</param>
        /// <returns></returns>
        T GetFirst(Expression<Func<T, bool>> whereExpression, Expression<Func<T, object>> orderExpression, OrderByType orderByType);

        /// <summary>
        /// 异步根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        Task<T> GetFirstAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 异步根据where条件查询单条，超过一条会报错!!!
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        Task<T> GetSingleAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 根据id获取，可使用缓存
        /// </summary>
        /// <param name="objId">主键</param>
        /// <param name="blnUseCache">是否使用缓存</param>
        /// <returns></returns>
        T GetById(dynamic objId, bool blnUseCache);

        /// <summary>
        /// 异步根据id获取，可使用缓存
        /// </summary>
        /// <param name="objId">主键</param>
        /// <param name="blnUseCache">是否使用缓存</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(dynamic objId, bool blnUseCache);

        /// <summary>
        /// 根据id集合获取
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <returns></returns>
        List<T> ListGetByIds(IEnumerable<dynamic> listId);

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="ids"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<TResult> GetByIds<TResult>(IEnumerable<dynamic> ids, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 异步根据id集合获取
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <returns></returns>
        Task<List<T>> ListGetByIdsAsync(IEnumerable<dynamic> listId);

        /// <summary>
        /// 新增，返回自增id
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        int InsertReturnIdentity(T model);

        /// <summary>
        /// 新增，返回模型
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        T InsertReturnEntity(T model);

        /// <summary>
        /// 异步新增，返回模型
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        Task<T> InsertReturnEntityAsync(T model);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        bool Insert(T model);

        /// <summary>
        /// 异步新增
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        Task<bool> InsertAsync(T model);

        /// <summary>
        /// 异步新增
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        Task<int> InsertReturnIdentityAsync(T model);

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="dbModels">模型集合</param>
        /// <returns></returns>
        bool InsertRange(T[] dbModels);

        /// <summary>
        /// 异步批量新增
        /// </summary>
        /// <param name="dbModels">模型集合</param>
        /// <returns></returns>
        Task<bool> InsertRangeAsync(T[] dbModels);

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="lisT">模型集合</param>
        /// <returns></returns>
        bool InsertRange(List<T> lisT);

        /// <summary>
        /// 异步批量新增
        /// </summary>
        /// <param name="lisT">模型集合</param>
        /// <returns></returns>
        Task<bool> InsertRangeAsync(List<T> lisT);


        /// <summary>
        /// 根据id删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        bool DeleteById(dynamic id);

        /// <summary>
        /// 异步根据id删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<bool> DeleteByIdAsync(dynamic id);

        /// <summary>
        /// 根据对象删除，模型内主键一定要有值
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        bool Delete(T model);

        /// <summary>
        /// 异步根据对象删除，模型内主键一定要有值
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T model);

        /// <summary>
        /// 根据where条件删除
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        bool Delete(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 异步根据where条件删除
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 根据id集合删除
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <returns></returns>
        bool DeleteByIds(dynamic[] listId);

        /// <summary>
        /// 异步根据id集合删除
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <returns></returns>
        Task<bool> DeleteByIdsAsync(dynamic[] listId);

        /// <summary>
        /// 根据id集合删除
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <returns></returns>
        bool DeleteByIds(List<dynamic> listId);

        /// <summary>
        /// 异步根据id集合删除
        /// </summary>
        /// <param name="listId">主键集合</param>
        /// <returns></returns>
        Task<bool> DeleteByIdsAsync(List<dynamic> listId);

        /// <summary>
        /// 根据对象更新，模型内主键必须有值
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        bool Update(T model);

        /// <summary>
        /// 根据对象更新，模型内主键必须有值
        /// </summary>
        /// <param name="model">模型</param>
        /// <param name="columns">指定列</param>
        /// <returns></returns>
        bool Update(T model, Expression<Func<T, T>> columns);

        /// <summary>
        /// 异步根据对象更新，模型内主键必须有值
        /// </summary>
        /// <param name="model">模型</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T model);

        /// <summary>
        /// 根据where条件更新指定列
        /// </summary>
        /// <param name="columns">待更新列</param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        bool Update(Expression<Func<T, T>> columns, Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 根据where条件更新指定列
        /// </summary>
        /// <param name="columns">待更新列</param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(Expression<Func<T, T>> columns, Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 批量根据模型更新，模型主键必须有值
        /// </summary>
        /// <param name="updateObjs">模型集合</param>
        /// <returns></returns>
        bool UpdateRange(IEnumerable<T> updateObjs);

        /// <summary>
        /// 批量根据模型更新，模型主键必须有值
        /// </summary>
        /// <param name="updateObjs">模型集合</param>
        /// <param name="columns">指定列</param>
        /// <returns></returns>
        bool UpdateRange(IEnumerable<T> updateObjs, Expression<Func<T, T>> columns);

        /// <summary>
        /// 异步批量根据模型更新，模型主键必须有值
        /// </summary>
        /// <param name="updateObjs">模型集合</param>
        /// <returns></returns>
        Task<int> UpdateRangeAsync(IEnumerable<T> updateObjs);

        /// <summary>
        /// 有则更新无则插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回插入数量和更新属性</returns>
        int[] AddOrUpdate(T model);

        /// <summary>
        /// 有则更新无则插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回插入数量和更新属性</returns>
        int[] AddOrUpdate(List<T> model);
        
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="t">数据表</param>
        /// <returns></returns>
        int BulkCopy(List<T> t);

        /// <summary>
        /// 查全部数据，不分页
        /// </summary>
        /// <returns></returns>
        List<T> ListGet();

        /// <summary>
        /// 异步查全部数据，不分页
        /// </summary>
        /// <returns></returns>
        Task<List<T>> ListGetAsync();

        /// <summary>
        /// 根据where条件查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        List<T> ListGet(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<TResult> ListGet<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 异步根据where条件查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        Task<List<T>> ListGetAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 异步根据where条件查询。不建议使用
        /// </summary>
        /// <param name="strWhere">where条件</param>
        /// <returns></returns>
        [Obsolete("请使用Lambda重载")]
        Task<List<T>> ListGetAsync(string strWhere);

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <param name="whereExpression">where条件</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<TResult> ListPageGet<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize
            , ref int totalCount, Expression<Func<T, TResult>> expression);
        

        /// <summary>
        /// 根据分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <returns></returns>
        List<T> ListPageGet(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount);

        /// <summary>
        /// 分页查询指定列自定义排序
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<TResult> ListGet<TResult>(Expression<Func<T, bool>> whereExpression, OrderByType orderByType, Expression<Func<T, object>> orderByExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 分页查询指定列升序
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<TResult> ListGetOrderByAsc<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, object>> orderByExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 分页查询指定列降序
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<TResult> ListGetOrderByDesc<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, object>> orderByExpression, Expression<Func<T, TResult>> expression);
        
        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="expression">指定列</param>
        /// <returns></returns>
        List<TResult> ListPageGet<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>>? orderByExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 升序分页查询指定列
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="expression">指定列</param>
        /// <returns></returns>
        List<TResult> ListPageGetOrderByAsc<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount,
            Expression<Func<T, object>>? orderByExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 降序分页查询指定列
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="expression">指定列</param>
        /// <returns></returns>
        List<TResult> ListPageGetOrderByDesc<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount,
            Expression<Func<T, object>>? orderByExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 升序分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        List<T> ListPageGetOrderByAsc(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression);

        /// <summary>
        /// 降序序分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        List<T> ListPageGetOrderByDesc(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression);


        /// <summary>
        /// 自定义条件排序分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序类型</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        List<T> ListPageGet(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>> orderByExpression);

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <param name="conditionalList">where表达式集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="expression">指定列</param>
        /// <returns></returns>
        List<TResult> ListPageGet<TResult>(List<SqlSugar.IConditionalModel> conditionalList, int intPageIndex, int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>>? orderByExpression, Expression<Func<T, TResult>> expression);

        /// <summary>
        /// 多条件分页查询
        /// </summary>
        /// <param name="conditionalList">条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <returns></returns>
        [Obsolete("请使用Lambda重载")]
        List<T> ListPageGet(List<SqlSugar.IConditionalModel> conditionalList, int intPageIndex, int intPageSize, ref int totalCount);

        /// <summary>
        /// 多条件升序分页查询
        /// </summary>
        /// <param name="conditionalList">条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        [Obsolete("请使用Lambda重载")]
        List<T> ListPageGetOrderByAsc(List<SqlSugar.IConditionalModel> conditionalList, int intPageIndex, int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression);

        /// <summary>
        /// 多条件降序序分页查询
        /// </summary>
        /// <param name="conditionalList">条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        [Obsolete("请使用Lambda重载")]
        List<T> ListPageGetOrderByDesc(List<SqlSugar.IConditionalModel> conditionalList, int intPageIndex, int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression);

        /// <summary>
        /// 自定义多条件排序分页查询
        /// </summary>
        /// <param name="conditionalList">多条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序类型</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        [Obsolete("请使用Lambda重载")]
        List<T> ListPageGet(List<SqlSugar.IConditionalModel> conditionalList, int intPageIndex, int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>> orderByExpression);

        /// <summary>
        /// 根据条件查询是否存在记录
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        bool IsAny(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 异步根据条件查询是否存在记录
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        Task<bool> IsAnyAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 根据条件查询总数
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        int Count(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 异步根据条件查询总数
        /// </summary>
        /// <param name="whereExpression">条件</param>
        /// <returns></returns>
        Task<int> CountAsync(Expression<Func<T, bool>> whereExpression);

        /// <summary>
        /// 求和
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="sumExpression"></param>
        /// <returns></returns>
        TResult Sum<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, TResult>> sumExpression);

        /// <summary>
        /// 最大
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="maxExpression"></param>
        /// <returns></returns>
        TResult Max<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, TResult>> maxExpression);


        /// <summary>
        /// 最小
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="minExpression"></param>
        /// <returns></returns>
        TResult Min<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, TResult>> minExpression);

        /// <summary>
        /// 平均
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="avgExpression"></param>
        /// <returns></returns>
        TResult Avg<TResult>(Expression<Func<T, bool>> whereExpression, Expression<Func<T, TResult>> avgExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        List<TResult> ListPageGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        List<TResult> ListPageGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, T3, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, T3, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        List<TResult> ListPageGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        List<TResult> ListPageGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount);

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        List<TResult> ListPageGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, T3, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType, Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount);



        /// <summary>
        /// 切换数据库
        /// </summary>
        void ChangeDataBase(string baseKey);

        DbSet<T> GetDb();
        /// <summary>
        /// 分表插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool InsertSplitTable(T model);
        /// <summary>
        /// 分表批量插入
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        bool InsertRangeSplitTable(IEnumerable<T> models);
        /// <summary>
        /// 分表删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool DeleteSplitTable(T model);
        /// <summary>
        /// 分表批量删除
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        bool DeleteRangeSplitTable(IEnumerable<T> models);
        /// <summary>
        /// 分表更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateSplitTable(T model);
        /// <summary>
        /// 分表批量更新
        /// </summary>
        /// <param name="updateObjs"></param>
        /// <returns></returns>
        bool UpdateRangeSplitTable(IEnumerable<T> updateObjs);
        /// <summary>
        /// 分表获取列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByField"></param>
        /// <returns></returns>
        List<T> ListGet_SplitTable(Expression<Func<T, bool>> whereExpression, DateTime startTime, DateTime endTime);

        /// <summary>
        /// 分表获取分页列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByField"></param>
        /// <returns></returns>
        List<T>  ListPageGet_SplitTable(Expression<Func<T, bool>> whereExpression, DateTime startTime, DateTime endTime, int intPageIndex, int intPageSize,
            ref int totalCount);
    }

    /// <summary>
    /// 业务抽象
    /// </summary>
    public interface IBaseService
    {

    }
}
