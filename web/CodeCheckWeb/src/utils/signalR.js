import * as signalR from '@aspnet/signalr'
class signalRClient {
  constructor(signalRUrl) {
    this.signalRUrl = signalRUrl
  }
  Connected() {
    return new signalR.HubConnectionBuilder().withUrl(this.signalRUrl).build()
  }
}
export default signalRClient
