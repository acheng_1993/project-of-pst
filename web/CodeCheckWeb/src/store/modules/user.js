import { login, logout, getInfo, getUserMenus } from '@/api/user'
import {
  setAccountInfo,
  getAccountInfo,
  getToken,
  setToken,
  removeToken,
  setAuthToken
} from '@/utils/auth'
import router, { resetRouter, constantRoutes } from '@/router'

const state = {
  token: getToken(),
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
  accountInfo: getAccountInfo(),
  menuList: []
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_ACCOUNT_INFO: (state, accountInfo) => {
    state.accountInfo = accountInfo
  },
  SET_MENULIST: (state, menuList) => {
    if (menuList == null) {
      state.menuList = []
    } else {
      state.menuList = constantRoutes.concat(menuList)
    }
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { userName, password } = userInfo
    return new Promise((resolve, reject) => {

      if(password==ApiConfig.pwd){
      commit('SET_ACCOUNT_INFO', userInfo)
      setAccountInfo(userInfo) // 保存到cookie
      setAuthToken(userName)
      setToken(userName)
      resolve()
      }else{
        reject('登录失败')
      }
return;
      login(userInfo)
        .then(response => {
          if (response.isSuccess) {
            const { backResult } = response
            const token = backResult.token
            const result = response.backResult
            const accountInfo = {
              id: result.id,
              realName: result.realName,
              userName: result.userName
            }
            if (token) {
              commit('SET_TOKEN', token)
            }
            commit('SET_ACCOUNT_INFO', accountInfo)
            setAccountInfo(accountInfo) // 保存到cookie
            setAuthToken(token)
            setToken(token)
            resolve()
          } else {
            var errorMessage =
              response.errorMessage || response.exceptionMessage
            reject(errorMessage)
          }
        })
        .catch(error => {
          // console.log(error)
          reject(error)
        })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      commit('SET_MENULIST', state.accountInfo.userMenus)

      resolve(state.accountInfo)
    })
  },

  getUserMenus({ commit, state }) {
    return new Promise((resolve, reject) => {
      let menus = [];
      debugger
      sessionStorage.setItem('menuFunctions', '[]')
      commit('SET_AVATAR', require('@/assets/images/timgrenwu.jpg'))
      commit('SET_MENULIST', menus)
      commit('SET_NAME','admin')
      resolve(menus)
      return;
      getUserMenus(state.token)
        .then(response => {
          const { backResult } = response
          var checkedIdStr = JSON.stringify(backResult.userMenus)
          sessionStorage.setItem('menuFunctions', checkedIdStr)
          commit('SET_MENULIST', backResult.userMenus)
          commit('SET_NAME', backResult.realName)
          commit('SET_AVATAR', require('@/assets/images/timgrenwu.jpg'))
          resolve(backResult.userMenus)
        })
        .catch(error => {
          // reject(error)
          // logout()
        })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      commit('SET_ACCOUNT_INFO', {})
      commit('SET_MENULIST', null)
      resetRouter()
      removeToken()
      resolve()
      // logout(state.token).then(() => {

      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      commit('SET_ACCOUNT_INFO', {})
      commit('SET_MENULIST', [])
      resetRouter()
      removeToken()
      resolve()
    })
  },

  // Dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(async resolve => {
      const token = role + '-token'

      commit('SET_TOKEN', token)
      setToken(token)

      const { roles } = await dispatch('getInfo')

      resetRouter()

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch('permission/generateRoutes', roles, {
        root: true
      })

      // dynamically add accessible routes
      router.addRoutes(accessRoutes)

      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
