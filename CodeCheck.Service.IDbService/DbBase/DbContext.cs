﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar;
using CodeCheck.Service.Core;

namespace CodeCheck.Service.IDbService
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class DbContext
    {
        /// <summary>
        /// 用来处理Db操作
        /// </summary>
        protected SqlSugarClient Db;

        public void BeginTran() { 
            Db.BeginTran();
        }
        public void CommitTran()
        {
            Db.CommitTran();
        }
        public void RollbackTran()
        {
            Db.RollbackTran();
        }
        /// <summary>
        /// 获取某个对象中的属性值
        /// </summary>
        /// <param name="info"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static object GetPropertyValue(object info, string field)
        {
            if (info == null) return null;
            Type t = info.GetType();
            IEnumerable<System.Reflection.PropertyInfo> property = from pi in t.GetProperties() where pi.Name.ToLower() == field.ToLower() select pi;
            return property.First().GetValue(info, null);
        }

        /// <summary>
        /// 初始化数据库上下文实例
        /// </summary>
        public DbContext()
        {
            //判断DbList是否为空
            if (DatabaseList.DbList.Count > 0)
            {
                Db = new SqlSugarClient(DatabaseList.DbList.First(a => a.ConfigId == "DefultDataBase")
                    .ConnectionConfig); //注入默认数据库连接
            }
            else
            {
                //获取appsettings.json中的链接数据库数组
                DatabaseList.DbList = ListGetConnectionConfig();
                //选择默认数据库
                Db = new SqlSugarClient(DatabaseList.DbList.First(a => a.ConfigId == "DefultDataBase")
                    .ConnectionConfig); //注入默认数据库连接
            }

            Db.Aop.DataExecuting = (oldValue, entityInfo) =>
            {
                if (entityInfo.PropertyName == "CreateTime" && entityInfo.OperationType == DataFilterType.InsertByObject)
                {
                    var propNameVal = GetPropertyValue(entityInfo.EntityValue,entityInfo.PropertyName);
                    if (propNameVal == null)
                    {
                        entityInfo.SetValue(DateTime.Now);//修改UpdateTime字段
                    }
                    /*当前列获取特性*/
                    //5.1.3.23 +
                    //entityInfo.IsAnyAttribute<特性>()
                    //entityInfo.GetAttribute<特性>()
                }

                /*** 列级别事件 ：更新的每一列都会进事件 ***/
                if (entityInfo.PropertyName == "ModifyTime" && entityInfo.OperationType == DataFilterType.UpdateByObject)
                {
                    entityInfo.SetValue(DateTime.Now);//修改UpdateTime字段
                }


                /*** 行级别事件 ：更新一条记录只进一次 ***/
                if (entityInfo.EntityColumnInfo.IsPrimarykey)
                {
                    //entityInfo.EntityValue 拿到单条实体对象
                }

                /***  根据当前列修改另一列 ***/
                //if(当前列逻辑==XXX)
                //var properyDate = entityInfo.EntityValue.GetType().GetProperty("Date");
                //if(properyDate!=null)
                //properyDate.SetValue(entityInfo.EntityValue,1);


                //可以写多个IF


            };


            //执行SQL前触发
            Db.Aop.OnLogExecuting = (sql, pars) =>
            {
                //Console.WriteLine(sql + "\r\n" +
                //Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                //Console.WriteLine();
            };

            //执行SQL完触发
            Db.Aop.OnLogExecuted = (sql, pars) =>
            {
                //SQL执行时间
                //TimeSpan excuteTime = Db.Ado.SqlExecutionTime;
                //StringBuilder sb = new StringBuilder();
                //sb.AppendLine($"SQL：{sql}");
                //sb.AppendLine($"SQL参数：{Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value))}");
                //sb.AppendLine($"SQL执行时间：{excuteTime}");

                //LogUtil.Info(sb.ToString(), "SQL执行日志");
            };

            //执行SQL出现错误事件
            Db.Aop.OnError = (exp) =>
            {
                // StringBuilder sb = new StringBuilder();
                // sb.AppendLine($"SQL：{exp.Sql}");
                // sb.AppendLine($"SQL参数：{exp.Parametres}");
                // LogUtil.Error(sb.ToString(), "SQL执行错误日志", exp);
            };
        }

        /// <summary>
        /// 获取数据库链接集合
        /// </summary>
        /// <returns></returns>
        public List<DBConConnection> ListGetConnectionConfig()
        {
            List<DBConConnection> dBConConnections = new List<DBConConnection>();
            var list = AppsettingsConfig.DBList;
            foreach (var item in list)
            {
                var conStr2 = item;
                DBConConnection dBConConnection = new DBConConnection();
                dBConConnection.ConfigId = item.ConfigId;
                ConnectionConfig con = new ConnectionConfig();
                string? connectionString = item.ConnectionString;
                //DBstring 支持加密识别
                if (connectionString != null && connectionString.Trim().Contains("Server=")) //表示DBstring没有加密
                {
                    connectionString = connectionString?.Trim();
                }
             
                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new ArgumentNullException("数据库连接字符串为空");
                }

                con.ConnectionString = connectionString;

                con.DbType = UPdateType(item.DatabaseType);

                con.IsAutoCloseConnection = true;
                dBConConnection.ConnectionConfig = con;
                dBConConnections.Add(dBConConnection);
            }

            return dBConConnections;
        }

        /// <summary>
        /// 判断数据库类型
        /// </summary>
        /// <param name="type">数据库类型</param>
        /// <returns></returns>
        public DbType UPdateType(string type)
        {
            switch (type.ToLower())
            {
                case "mysql":
                    return DbType.MySql;
                case "sqlserver":
                    return DbType.SqlServer;
                case "sqlite":
                    return DbType.Sqlite;
                case "oracle":
                    return DbType.Oracle;
                case "postgresql":
                    return DbType.PostgreSQL;
                case "questdb":
                    return DbType.QuestDB;
                default:
                    return DbType.SqlServer;
            }
        }

        /// <summary>
        /// 获取数据库处理对象
        /// </summary>
        /// <typeparam name="T">数据库模型</typeparam>
        /// <returns>返回值</returns>
        public DbSet<T> GetDbSet<T>() where T : class, new()
        {
            return new DbSet<T>(Db);
        }

      
    }
}