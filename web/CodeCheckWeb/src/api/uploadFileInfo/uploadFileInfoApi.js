import Vue from 'vue'
import AjaxService from '@/utils/ajaxService'

const module = 'UploadFileInfo'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.baseUrl)

export default class UploadFileInfoApi {
  getlist(query) {
    return ajaxService.get(module, 'GetList', query)
  }
  getPagelist(query) {
    return ajaxService.get(module, 'GetListPaged', query)
  }
  insert(model) {
    return ajaxService.post(module, 'insert', model)
  }
  update(model) {
    return ajaxService.post(module, 'UpdateById/' + model.id, model)
  }
}
