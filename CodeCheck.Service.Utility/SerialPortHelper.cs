﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;


namespace CodeCheck.Service.Utility
{
    public enum StopBit
    {
        None,
        One,
        Two,
        OnePointFive
    }
    public enum SerialParity
    {
        None,
        Odd,
        Even,
        Mark,
        Space
    }

    public class SerialPortHelper
    {
        private SerialPort _serialPort;
        private Thread _readThread;
        private bool _continueReading;
        public Action<string> DataReceiveAction; 



        /// <summary>
        /// 
        /// </summary>
        /// <param name="portName">串口</param>
        /// <param name="baudRate">波特率</param>
        /// <param name="parity">校验位</param>
        /// <param name="dataBits">数据位</param>
        /// <param name="stopBits">停止位</param>
        public SerialPortHelper(string portName, int baudRate, SerialParity parity, int dataBits, StopBit stopBits)
        {
            var p = (Parity)parity.ToInt();
            var s = (StopBits)stopBits.ToInt();
            _serialPort = new SerialPort(portName, baudRate, p, dataBits,s );
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
        }

        /// <summary>
        /// 获取串口列表
        /// </summary>
        /// <returns></returns>
        public static List<string> GetSerialPortList() {
            string[] portNames = SerialPort.GetPortNames();
            return portNames.ToList();

        }


        public bool Connect()
        {
            if (!_serialPort.IsOpen)
            {
                try
                {
                    _continueReading = true;
                    _readThread = new Thread(new ThreadStart(ReadThread));
                    _readThread.Start();
                    //_serialPort.Open();
                 
                
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to connect to serial port. {ex.Message}");
                }
            }
            return false;
        }

        public void Disconnect()
        {
            if (_serialPort.IsOpen)
            {
                _continueReading = false;
                //_readThread.Join();
                _serialPort.Close();
            }
        }

        public bool IsConnected()
        {
            return _serialPort.IsOpen;
        }
        public bool SendHex(string message)
        {
            try
            {
                string hexString = message;// "FE FE 01 06 01 F9"; // 这是一个示例HEX字符串
                byte[] dataBytes = hexString.ToBytesFromHexString(); // 每两个字符代表一个字节
                // 确保串口已打开且准备好发送数据
                if (_serialPort.IsOpen)
                {
                    _serialPort.Write(dataBytes, 0, dataBytes.Length); // 发送字节数组
                    return true;
                }
            }
            catch (Exception ex){ 

            }
            return false;
        }
        public bool Send(string message)
        {
            if (_serialPort.IsOpen)
            {
                _serialPort.Write(message);
                return true;
            }
            return false;
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string data = sp.ReadExisting();
            DataReceiveAction?.Invoke(data);
            // do something with the received data
        }

        private void ReadThread()
        {
            while (_continueReading)
            {
                if (!_serialPort.IsOpen)
                {
                    Console.WriteLine("Serial port is not open. Reconnecting...");
                    try
                    {
                        _serialPort.Open();
                    }catch (Exception ex)
                    {

                    }
                }
                else {
                    try
                    {
                        Send("1");
                    }
                    catch {
                        Disconnect();
                        Connect();
                    }
                }
                Thread.Sleep(5000);
            }
        }
    }

}
