import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'UserMenuRelation'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class UserMenuRelation {
  getUserMenuRelationlist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  saveUserPrivateMenu(query) {
    return ajaxService.post(UserRouter, 'SaveUserPrivateMenu', query)
  }
  saveUserPrivateMenuByFunctionids(query) {
    return ajaxService.post(UserRouter, 'SaveUserPrivateMenuByFunctionids', query)
  }
}

export default UserMenuRelation
