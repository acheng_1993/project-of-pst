import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const dictionaryTypeRouter = 'BusinessDictionaryType'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class DictionaryType {
  getDictionaryTypeListPaged(query) {
    return ajaxService.get(dictionaryTypeRouter, 'GetListPaged', query)
  }
  getDictionaryTypeList(query) {
    return ajaxService.get(dictionaryTypeRouter, 'GetList', query)
  }
  createDictionaryType(model) {
    return ajaxService.post(dictionaryTypeRouter, 'insert', model)
  }
  updateDictionaryType(model) {
    return ajaxService.post(dictionaryTypeRouter, 'UpdateById/' + model.id, model)
  }
  deleteDictionaryType(model) {
    return ajaxService.delete(dictionaryTypeRouter, 'delete', model.id)
  }
}

export default DictionaryType
