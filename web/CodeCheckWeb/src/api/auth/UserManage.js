import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const userRouter = 'User'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class UserManage {
  getUserPagelist(query) {
    return ajaxService.get(userRouter, 'GetListPaged', query)
  }
  createUser(query) {
    return ajaxService.post(userRouter, 'Insert', query)
  }
  updateUser(query) {
    return ajaxService.post(userRouter, 'UpdateById/' + query.employeeId, query)
  }
  deleteUser(query) {
    return ajaxService.delete(userRouter, 'delete', query)
  }
  deleteUserBatch(query) {
    return ajaxService.post(userRouter, 'DeleteBatch', query)
  }
  updateUserEnableBatch(query) {
    return ajaxService.post(userRouter, 'UpdateEnabledBatch', query)
  }
  getUserListByids(query) {
    return ajaxService.post(userRouter, 'GetListByIds', query)
  }
  editPwd(query) {
    return ajaxService.post(userRouter, 'EditPassword', query)
  }
}

export default UserManage
