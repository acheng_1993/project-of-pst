import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const dictionaryRouter = 'BusinessDictionary'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class Dictionary {
  getDictionaryList(query) {
    return ajaxService.get(dictionaryRouter, 'GetList', query)
  }
  getDictionary(query) {
    return ajaxService.get(dictionaryRouter, 'Get', query)
  }
  getDictionaryListPaged(query) {
    return ajaxService.get(dictionaryRouter, 'GetListPaged', query)
  }
  createDictionary(model) {
    return ajaxService.post(dictionaryRouter, 'insert', model)
  }
  updateDictionary(model) {
    return ajaxService.post(dictionaryRouter, 'UpdateById/' + model.id, model)
  }
  deleteDictionary(model) {
    return ajaxService.delete(dictionaryRouter, 'delete', model.id)
  }
}

export default Dictionary
