import request from '@/utils/request'
import baseConfig from './baseConfig'

/**
 * MQ服务基础配置
 * @param {查询条件} query
 */
export function getMQBasicConfigList(query) {
  return request({
    url: baseConfig.authApiUrl + '/BasicConfig/GetListPaged',
    method: 'get',
    params: query
  })
}
export function createMQBasicConfig(model) {
  return request({
    url: baseConfig.authApiUrl + '/BasicConfig/insert',
    method: 'post',
    data: model
  })
}

export function updateMQBasicConfig(model) {
  return request({
    url: baseConfig.authApiUrl + '/BasicConfig/UpdateById/' + model.id,
    method: 'post',
    data: model
  })
}

export function deleteMQBasicConfig(model) {
  return request({
    url: baseConfig.authApiUrl + '/BasicConfig/delete',
    method: 'delete',
    data: model.id
  })
}

/**
 * MQ交换器API
 */
export function getExchangeList(query) {
  return request({
    url: baseConfig.authApiUrl + '/Exchange/GetListPaged',
    method: 'get',
    params: query
  })
}
export function createExchange(model) {
  return request({
    url: baseConfig.authApiUrl + '/Exchange/insert',
    method: 'post',
    data: model
  })
}

export function updateExchange(model) {
  return request({
    url: baseConfig.authApiUrl + '/Exchange/UpdateById/' + model.id,
    method: 'post',
    data: model
  })
}

export function deleteExchange(model) {
  return request({
    url: baseConfig.authApiUrl + '/Exchange/delete',
    method: 'delete',
    data: model.id
  })
}

/**
 * 队列信息配置
 */

export function getQueuecConfigList(query) {
  return request({
    url: baseConfig.authApiUrl + '/Queue/GetListPaged',
    method: 'get',
    params: query
  })
}
export function createQueueConfig(model) {
  return request({
    url: baseConfig.authApiUrl + '/Queue/insert',
    method: 'post',
    data: model
  })
}

export function updateQueueConfig(model) {
  return request({
    url: baseConfig.authApiUrl + '/Queue/UpdateById/' + model.id,
    method: 'post',
    data: model
  })
}

export function deleteQueueConfig(model) {
  return request({
    url: baseConfig.authApiUrl + '/Queue/delete',
    method: 'delete',
    data: model.id
  })
}
