import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const staffManageRouter = 'Employee'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class StaffManage {
  getStaffManageListPaged(query) {
    return ajaxService.get(staffManageRouter, 'GetListPaged', query)
  }
  getStaffManageList(query) {
    return ajaxService.get(staffManageRouter, 'GetList', query)
  }
  createStaffManage(model) {
    return ajaxService.post(staffManageRouter, 'insert', model)
  }
  updateStaffManage(model) {
    return ajaxService.post(staffManageRouter, 'UpdateById/' + model.id, model)
  }
  deleteStaffManage(model) {
    return ajaxService.delete(staffManageRouter, 'delete', model.id)
  }
}

export default StaffManage
