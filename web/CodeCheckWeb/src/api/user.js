import request from '@/utils/request'
import Vue from 'vue'

// 登录
export function login(data) {
  return request({
    url: Vue.prototype.BaseConfig.tokenApiUrl,
    method: 'post',
    data: data
  })
}

// 获取用户信息
export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

// 获取用户菜单
export function getUserMenus(token) {
  return request({
    url: Vue.prototype.BaseConfig.authApiUrl + '/user/GetUserInfoByToken',
    method: 'get',
    params: { token: token }
  })
}

// 退出
export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

/**
 * 获取用户列表
 * @returns 返回用户数据
 */
export function getUserList() {
  return request({
    url: Vue.prototype.BaseConfig.authApiUrl + '/User/GetList',
    method: 'get'
  })
}
