import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'Role'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class RoleManager {
  getRolelist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  getRolePagelist(query) {
    return ajaxService.get(UserRouter, 'GetListPaged', query)
  }
  insertRole(query) {
    return ajaxService.post(UserRouter, 'insert', query)
  }
  updateRole(query) {
    return ajaxService.post(UserRouter, 'UpdateById/' + query.id, query)
  }
  deleteRole(query) {
    return ajaxService.delete(UserRouter, 'delete', query)
  }
  copyRolePermissionsByids(query) {
    return ajaxService.post(UserRouter, 'CopyRolePermissionsByids', query)
  }
}

export default RoleManager
