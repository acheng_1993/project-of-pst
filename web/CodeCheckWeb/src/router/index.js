import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/** note: sub-menu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
    affix: true                  if true, the tag will affix in the tags-view
  }
**/

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 * */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index'),
        meta: { keepAlive: true }
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/Code/CodeRecord/CodeRecordList.vue'),
        name: 'dashboard',
        meta: {
          title: 'PST-条码查询',
          icon: 'aliapp',
          noCache: false,
          affix: true
        }
      }
    ]
  },
  {
    path: '/UnshowSet',
    component: Layout,
    redirect: '/UnshowSet/index',
    name: 'UnshowSet',
    alwaysShow: true, // will always show the root menu
    hidden: true,
    meta: {
      title: '隐藏菜单',
      icon: 'alishouqicaidan',
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'MenuButtonSetting',
        component: () => import('@/views/GeneralPowerSet/MenuButtonSetting'),
        name: 'MenuButtonSetting',
        meta: {
          title: '菜单按钮设置',

          icon: 'aliicon_setting',
          noCache: false
        },
        hidden: true
      },
      {
        path: 'UserMenuButtonSetting',
        component: () =>
          import('@/views/GeneralPowerSet/UserMenuButtonSetting'),
        name: 'UserMenuButtonSetting',
        meta: {
          title: '个人菜单按钮设置',
          icon: 'aliicon_setting',
          noCache: false
        },
        hidden: true
      },
      {
        path: 'UserScreenMenuButtonSetting',
        component: () =>
          import('@/views/GeneralPowerSet/UserScreenMenuButtonSetting'),
        name: 'UserScreenMenuButtonSetting',
        meta: {
          title: '屏蔽个人菜单按钮设置',
          icon: 'aliicon_setting',
          noCache: false
        },
        hidden: true
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/error',
    component: Layout,
    redirect: 'noredirect',
    name: 'ErrorPages',
    meta: {
      title: 'errorPages',
      icon: '404'
    },
    children: [
      {
        path: '401',
        component: () => import('@/views/errorPage/401'),
        name: 'Page401',
        meta: { title: 'page401', noCache: true }
      },
      {
        path: '404',
        component: () => import('@/views/errorPage/404'),
        name: 'Page404',
        meta: { title: 'page404', noCache: true }
      }
    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () =>
  new Router({
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
