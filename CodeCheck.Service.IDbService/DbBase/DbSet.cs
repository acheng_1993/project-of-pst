﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SqlSugar;

namespace CodeCheck.Service.IDbService
{
    /// <summary>
    /// 数据库上下文扩展
    /// </summary>
    public class DbSet<T> : SimpleClient<T> where T : class, new()
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context">数据库上下文</param>
        public DbSet(SqlSugarClient context) : base(context)
        {

        }


        /// <summary>
        /// 插入，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Saveable(T model)
        {
            return Context.Insertable(model).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 插入保存，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int SaveableReturnCommand(T model)
        {
            return Context.Insertable(model).ExecuteCommand();
        }

        /// <summary>
        /// 插入保存，返回对象
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public T SaveableReturnEntity(T model)
        {
            return Context.Insertable(model).ExecuteReturnEntity();
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件），返回影响行数
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public int Saveable(IEnumerable<T> models)
        {
            return Context.Storageable(models.ToList()).ExecuteCommand();
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件），返回是否成功
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <param name="setColumns">指定更新列</param>
        /// <returns></returns>
        public bool Saveable(T model, Expression<Func<T, object>> setColumns)
        {
            return Context.Updateable(model).UpdateColumns(setColumns).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件），返回影响行数
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <param name="setColumns">指定更新列</param>
        /// <returns></returns>
        public int SaveableReturnCommand(T model, Expression<Func<T, object>> setColumns)
        {
            return Context.Updateable(model).UpdateColumns(setColumns).ExecuteCommand();
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件），返回影响行数
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <param name="setColumns">指定更新列</param>
        /// <returns></returns>
        public int SaveableReturnCommand(IEnumerable<T> model, Expression<Func<T, object>> setColumns)
        {
            return Context.Updateable(model.ToList()).UpdateColumns(setColumns).ExecuteCommand();
        }

        /// <summary>
        /// 插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public bool InsertWithOutNull(T model)
        {
            return Context.Insertable<T>(model).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public bool InsertWithOutNull(string tableName, T model)
        {
            return Context.Insertable<T>(model).AS(tableName).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public int InsertWithOutNullReturnCommand(T model)
        {
            return Context.Insertable<T>(model).ExecuteCommand();
        }

        /// <summary>
        /// 插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public int InsertWithOutNullReturnCommand(string tableName, T model)
        {
            return Context.Insertable<T>(model).AS(tableName).ExecuteCommand();
        }


        /// <summary>
        /// 批量插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="models">Db模型集合</param>
        /// <returns></returns>
        public bool InsertRangeWithOutNull(IEnumerable<T> models)
        {
            return Context.Insertable<T>(models.ToList()).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 批量插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="models">Db模型集合</param>
        /// <returns></returns>
        public bool InsertRangeWithOutNull(string tableName, IEnumerable<T> models)
        {
            return Context.Insertable<T>(models.ToList()).AS(tableName).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 批量插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="models">Db模型集合</param>
        /// <returns></returns>
        public int InsertRangeWithOutNullReturnCommand(IEnumerable<T> models)
        {
            return Context.Insertable<T>(models.ToList()).ExecuteCommand();
        }


        /// <summary>
        /// 批量插入 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="models">Db模型集合</param>
        /// <returns></returns>
        public int InsertRangeWithOutNullReturnCommand(string tableName, IEnumerable<T> models)
        {
            return Context.Insertable<T>(models.ToList()).AS(tableName).ExecuteCommand();
        }


        /// <summary>
        /// 异步插入
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public override async Task<bool> InsertAsync(T model)
        {
            return await Context.Insertable<T>(model).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 异步插入
        /// </summary>
        /// <param name="models">Db模型集合</param>
        /// <returns></returns>
        public async Task<bool> InsertAsync(T[] models)
        {
            return await Context.Insertable<T>(models).IgnoreColumns(true, true).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 异步插入
        /// </summary>
        /// <param name="models">Db模型集合</param>
        /// <returns></returns>
        public async Task<bool> InsertAsync(List<T> models)
        {
            return await Context.Insertable<T>(models).IgnoreColumns(true, true).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 插入返回自增id 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public int InsertWithOutNullReturnIdentity(T model)
        {
            return Context.Insertable<T>(model).IgnoreColumns(true, true).ExecuteReturnIdentity();
        }

        /// <summary>
        /// 插入返回自增id 设置NULL列不插入和强制插入自增列
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public int InsertWithOutNullReturnIdentity(string tableName, T model)
        {
            return Context.Insertable<T>(model).AS(tableName).IgnoreColumns(true, true).ExecuteReturnIdentity();
        }


        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public override T InsertReturnEntity(T model)
        {
            return Context.Insertable<T>(model).IgnoreColumns(true, true).ExecuteReturnEntity();
        }


        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public T InsertReturnEntity(string tableName, T model)
        {
            return Context.Insertable<T>(model).AS(tableName).IgnoreColumns(true, true).ExecuteReturnEntity();
        }

        /// <summary>
        /// 异步插入
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public override async Task<T> InsertReturnEntityAsync(T model)
        {
            return await Context.Insertable<T>(model).IgnoreColumns(true, true).ExecuteReturnEntityAsync();
        }

        /// <summary>
        /// 异步插入
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public override async Task<int> InsertReturnIdentityAsync(T model)
        {
            return await Context.Insertable<T>(model).IgnoreColumns(true, true).ExecuteReturnIdentityAsync();
        }

        /// <summary>
        /// 异步删除
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public override async Task<bool> DeleteAsync(T model)
        {
            return await Context.Deleteable<T>(model).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 异步删除
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public override async Task<bool> DeleteAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await Context.Deleteable<T>(whereExpression).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 异步根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        public override async Task<bool> DeleteByIdAsync(dynamic id)
        {
            return await Context.Deleteable<T>(id).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 异步根据主键集合删除
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public override async Task<bool> DeleteByIdsAsync(dynamic[] ids)
        {
            return await Context.Deleteable<T>(ids).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 根据主键集合删除
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public bool DeleteByIds(List<dynamic> ids)
        {
            return Context.Deleteable<T>(ids).ExecuteCommand() > 0;
        }


        /// <summary>
        /// 根据主键集合删除
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public bool DeleteByIds(string tableName, List<dynamic> ids)
        {
            return Context.Deleteable<T>(ids).AS(tableName).ExecuteCommand() > 0;
        }


        /// <summary>
        /// 异步根据主键集合删除
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdsAsync(List<dynamic> ids)
        {
            return await Context.Deleteable<T>(ids).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 根据主键集合查询
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public List<T> GetByIds(IEnumerable<dynamic> ids)
        {
            return Context.Queryable<T>().In(ids).ToList();
        }

        /// <summary>
        /// 根据主键集合查询
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public List<T> GetByIds(string tableName, IEnumerable<dynamic> ids)
        {
            return Context.Queryable<T>().AS(tableName).In(ids).ToList();
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="ids"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<TResult> GetByIds<TResult>(IEnumerable<dynamic> ids, Expression<Func<T, TResult>> expression)
        {
            return Context.Queryable<T>().In(ids).Select<TResult>(expression).ToList();
        }


        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="ids"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<TResult> GetByIds<TResult>(string tableName, IEnumerable<dynamic> ids,
            Expression<Func<T, TResult>> expression)
        {
            return Context.Queryable<T>().AS(tableName).In(ids).Select<TResult>(expression).ToList();
        }


        /// <summary>
        /// 求和
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="sumExpression"></param>
        /// <returns></returns>
        public TResult Sum<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> sumExpression)
        {
            return Context.Queryable<T>().Where(whereExpression).Sum(sumExpression);
        }


        /// <summary>
        /// 最大
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="maxExpression"></param>
        /// <returns></returns>
        public TResult Max<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> maxExpression)
        {
            return Context.Queryable<T>().Where(whereExpression).Max(maxExpression);
        }


        /// <summary>
        /// 最小
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="minExpression"></param>
        /// <returns></returns>
        public TResult Min<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> minExpression)
        {
            return Context.Queryable<T>().Where(whereExpression).Min(minExpression);
        }


        /// <summary>
        /// 平均
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="avgExpression"></param>
        /// <returns></returns>
        public TResult Avg<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> avgExpression)
        {
            return Context.Queryable<T>().Where(whereExpression).Avg(avgExpression);
        }


        /// <summary>
        /// 异步根据主键集合查询
        /// </summary>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        public async Task<List<T>> GetByIdsAsync(IEnumerable<dynamic> ids)
        {
            return await Context.Queryable<T>().In(ids).ToListAsync();
        }

        /// <summary>
        /// 异步查询全部
        /// </summary>
        /// <returns></returns>
        public async Task<List<T>> ListGetAsync()
        {
            return await Context.Queryable<T>().ToListAsync();
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<TResult> ListGet<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> expression)
        {
            return Context.Queryable<T>().Where(whereExpression).Select<TResult>(expression).ToList();
        }


        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="orderByType"></param>
        /// <returns></returns>
        public List<T> ListGet(Expression<Func<T, bool>> whereExpression, Expression<Func<T, object>> orderByExpression,
            OrderByType orderByType)
        {
            return Context.Queryable<T>().Where(whereExpression).OrderBy(orderByExpression, orderByType).Select<T>()
                .ToList();
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<TResult> ListGet<TResult>(Expression<Func<T, bool>> whereExpression, OrderByType orderByType,
            Expression<Func<T, object>> orderByExpression, Expression<Func<T, TResult>> expression)
        {
            return Context.Queryable<T>().Where(whereExpression).OrderBy(orderByExpression, orderByType)
                .Select<TResult>(expression).ToList();
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="expression">指定列</param>
        /// <returns></returns>
        public List<TResult> ListPageGet<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex,
            int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>>? orderByExpression, Expression<Func<T, TResult>> expression)
        {
            List<TResult> result = Context.Queryable<T>()
                .OrderByIF(orderByExpression != null, orderByExpression, orderByType)
                .Where(whereExpression)
                .Select<TResult>(expression)
                .ToPageList(intPageIndex, intPageSize, ref totalCount);
            return result;
        }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="conditionalList">where表达式集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="expression">排序表达式</param>
        /// <returns></returns>
        public List<TResult> ListPageGet<TResult>(List<IConditionalModel> conditionalList, int intPageIndex,
            int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>>? orderByExpression, Expression<Func<T, TResult>> expression)
        {
            List<TResult> result = Context.Queryable<T>()
                .OrderByIF(orderByExpression != null, orderByExpression, orderByType)
                .Where(conditionalList)
                .Select<TResult>(expression)
                .ToPageList(intPageIndex, intPageSize, ref totalCount);
            return result;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="whereExpression">where条件</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<TResult> ListPageGet<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex,
            int intPageSize
            , ref int totalCount, Expression<Func<T, TResult>> expression)
        {
            List<TResult> result = Context.Queryable<T>().Where(whereExpression)
                .Select<TResult>(expression)
                .ToPageList(intPageIndex, intPageSize, ref totalCount);

            return result;
        }


        /// <summary>
        /// 异步查询全部
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public async Task<List<T>> ListGetAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await Context.Queryable<T>().Where(whereExpression).ToListAsync();
        }

        /// <summary>
        /// 通过开始时间和结束时间找到对应时间的表
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="beginDate">分表开始时间</param>
        /// <param name="endDate">分表结束时间</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">条数</param>
        /// <returns></returns>
        public List<T> GetSplitTablePageList(Expression<Func<T, bool>> whereExpression, DateTime beginDate, DateTime endDate,
            int pageIndex, int pageSize)
        {
            return Context.Queryable<T>().Where(whereExpression)
                .SplitTable(beginDate, endDate).ToPageList(pageIndex, pageSize);
        }

        /// <summary>
        /// 通过开始时间和结束时间找到对应时间的表
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="beginDate">分表开始时间</param>
        /// <param name="endDate">分表结束时间</param>
        /// <returns></returns>
        public List<T> GetSplitTableList(Expression<Func<T, bool>> whereExpression, DateTime beginDate, DateTime endDate)
        {
            return Context.Queryable<T>().Where(whereExpression)
                .SplitTable(beginDate, endDate).ToList();
        }
        
        /// <summary>
        /// 所有分表检索找到对应表
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">条数</param>
        /// <returns></returns>
        public List<T> GetSplitTablePageList(Expression<Func<T, bool>> whereExpression, int pageIndex, int pageSize)
        {
            return Context.Queryable<T>().Where(whereExpression)
                .SplitTable(tabs => tabs).ToPageList(pageIndex, pageSize);
        }
        
        /// <summary>
        /// 所有分表检索找到对应表
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public List<T> GetSplitTableList(Expression<Func<T, bool>> whereExpression)
        {
            return Context.Queryable<T>().Where(whereExpression)
                .SplitTable(tabs => tabs).ToList();
        }

        /// <summary>
        /// 查询全部
        /// </summary>
        /// <param name="strWhere">where条件</param>
        /// <returns></returns>
        public List<T> ListGet(string strWhere)
        {
            return Context.Queryable<T>().WhereIF(!string.IsNullOrEmpty(strWhere), strWhere).ToList();
        }

        /// <summary>
        /// 异步查询全部
        /// </summary>
        /// <param name="strWhere">where条件</param>
        /// <returns></returns>
        public async Task<List<T>> ListGetAsync(string strWhere)
        {
            return await Context.Queryable<T>().WhereIF(!string.IsNullOrEmpty(strWhere), strWhere).ToListAsync();
        }

        /// <summary>
        /// 根据主键查询
        /// </summary>
        /// <param name="objId">主键</param>
        /// <param name="blnUseCache">是否使用缓存</param>
        /// <returns></returns>
        public T GetById(dynamic objId, bool blnUseCache)
        {
            return Context.Queryable<T>().WithCacheIF(blnUseCache).InSingle(objId);
        }

        /// <summary>
        /// 异步根据主键查询
        /// </summary>
        /// <param name="objId">主键</param>
        /// <param name="blnUseCache">是否使用缓存</param>
        /// <returns></returns>
        public async Task<T> GetByIdAsync(dynamic objId, bool blnUseCache)
        {
            return await Task.Run(Context.Queryable<T>().WithCacheIF(blnUseCache).InSingle(objId));
        }

        /// <summary>
        /// 异步根据主键查询
        /// </summary>
        /// <param name="objId">主键</param>
        /// <returns></returns>
        public override async Task<T> GetByIdAsync(dynamic objId)
        {
            return await Task.Run(() => Context.Queryable<T>().InSingle(objId));
        }

        /// <summary>
        /// 根据where查询单条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public override async Task<T> GetSingleAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await Context.Queryable<T>().SingleAsync(whereExpression);
        }

        /// <summary>
        /// 根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="orderExpression">orderBy表达式</param>
        /// <param name="orderByType">排序类型</param>
        /// <returns></returns>
        public T GetFirst(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderExpression, OrderByType orderByType)
        {
            return Context.Queryable<T>().Where(whereExpression).OrderBy(orderExpression, orderByType).First();
        }

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int UpdateReturnCommand(T model)
        {
            return Context.Updateable(model).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommand();
        }

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int UpdateRangeReturnCommand(IEnumerable<T> model)
        {
            return Context.Updateable(model.ToList()).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommand();
        }

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public int UpdateRangeReturnCommand(Expression<Func<T, T>> columns, Expression<Func<T, bool>> whereExpression)
        {
            return Context.Updateable<T>().SetColumns(columns).Where(whereExpression).ExecuteCommand();
        }

        /// <summary>
        /// 异步更新
        /// </summary>
        /// <param name="model">Db模型</param>
        /// <returns></returns>
        public override async Task<bool> UpdateAsync(T model)
        {
            return await Context.Updateable<T>(model).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync() >
                   0;
        }

        /// <summary>
        /// 异步更新
        /// </summary>
        /// <param name="columns">更新列表达式</param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public override async Task<bool> UpdateAsync(Expression<Func<T, T>> columns,
            Expression<Func<T, bool>> whereExpression)
        {
            return await Context.Updateable<T>().SetColumns(columns).Where(whereExpression).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 异步批量更新
        /// </summary>
        /// <param name="updateObjs">Db模型集合</param>
        /// <returns></returns>
        public Task<int> UpdateRangeAsync(IEnumerable<T> updateObjs)
        {
            foreach (var entity in updateObjs.ToList())
            {
                Context.Updateable(entity).IgnoreColumns(ignoreAllNullColumns: true).AddQueue();
            }

            return Context.SaveQueuesAsync();
        }

        /// <summary>
        /// 异步是否存在
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public override async Task<bool> IsAnyAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await Context.Queryable<T>().AnyAsync();
        }

        /// <summary>
        /// 异步合计
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public override async Task<int> CountAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await Context.Queryable<T>().CountAsync();
        }

        /// <summary>
        /// 更新忽略NUll列
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateWithOutNull(T model)
        {
            return Context.Updateable(model).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="model"></param>
        /// <param name="setColumns"></param>
        /// <returns></returns>
        public bool Update(T model, Expression<Func<T, T>> setColumns)
        {
            return Context.Updateable(model).SetColumns(setColumns).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <param name="setColumns"></param>
        /// <returns></returns>
        public bool Update(string tableName, T model, Expression<Func<T, T>> setColumns)
        {
            return Context.Updateable(model).AS(tableName).SetColumns(setColumns).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="model"></param>
        /// <param name="setColumns"></param>
        /// <returns></returns>
        public int UpdateReturnCommand(T model, Expression<Func<T, T>> setColumns)
        {
            return Context.Updateable(model).SetColumns(setColumns).ExecuteCommand();
        }

        /// <summary>
        /// 更新忽略NUll列，返回影响行数
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public int UpdateRangeWithOutNullReturnCommand(IEnumerable<T> models)
        {
            return Context.Updateable(models.ToList()).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommand();
        }

        /// <summary>
        /// 更新忽略NUll列
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public bool UpdateRangeWithOutNull(IEnumerable<T> models)
        {
            return Context.Updateable(models.ToList()).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="t">数据表</param>
        /// <returns></returns>
        public int BulkCopy(List<T> t)
        {
            return Context.Fastest<T>().BulkCopy(t);
        }

        /// <summary>
        /// 有则更新无则插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回插入数量和更新属性</returns>
        public int[] AddOrUpdate(T model)
        {
            var storageableResult = Context.Storageable(model).ToStorage(); //将数据进行分组 
            storageableResult.AsInsertable.ExecuteReturnSnowflakeId(); //执行插入 （可以换成雪花ID和自增）
            storageableResult.AsUpdateable.ExecuteCommand();
            int[] addorupdatenum = new[] {storageableResult.InsertList.Count, storageableResult.UpdateList.Count};
            return addorupdatenum;
        }

        /// <summary>
        /// 有则更新无则插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回插入数量和更新属性</returns>
        public int[] AddOrUpdate(List<T> model)
        {
            var storageableResult = Context.Storageable(model).ToStorage(); //将数据进行分组 
            storageableResult.AsInsertable.ExecuteCommand(); //执行插入 （可以换成雪花ID和自增）
            storageableResult.AsUpdateable.ExecuteCommand();
            int[] addorupdatenum = new[] {storageableResult.InsertList.Count, storageableResult.UpdateList.Count};
            return addorupdatenum;
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="models"></param>
        /// <param name="setColumns"></param>
        /// <returns></returns>
        public bool UpdateRange(IEnumerable<T> models, Expression<Func<T, T>> setColumns)
        {
            return Context.Updateable(models.ToList()).SetColumns(setColumns).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="models"></param>
        /// <param name="setColumns"></param>
        /// <returns></returns>
        public int UpdateRangeReturnCommand(IEnumerable<T> models, Expression<Func<T, T>> setColumns)
        {
            return Context.Updateable(models.ToList()).SetColumns(setColumns).ExecuteCommand();
        }

        /// <summary>
        /// 更新忽略NUll列，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int UpdateWithOutNullReturnCommand(T model)
        {
            return Context.Updateable(model).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommand();
        }
    }
}