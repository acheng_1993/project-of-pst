export default {
   CodeRecord_lang: {
              code:"条码",
           codeTime:"条码时间",
           status:"状态",
           attribute1:"预留1",
           attribute2:"预留2",
           attribute3:"预留3",
           keyword: '关键词',
         edit:'编辑',
         delete:'删除',
         operation:'操作',
  },
    CodeRecordError: {
              codeRequired:"",
           codeTimeRequired:"",
           statusRequired:"",
           attribute1Required:"",
           attribute2Required:"",
           attribute3Required:"",
    },
  }