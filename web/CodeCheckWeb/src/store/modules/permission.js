import { constantRoutes } from '@/router'
import Layout from '@/layout'
import empty from '@/layout/empty'
import error_404 from '@/views/errorPage/404_making'

/**
 * 通过meta.name判断是否与当前用户权限匹配
 * @param menuList
 * @param route
 */
function hasPermission(menuList, route) {
  if (route.meta) {
    return menuList.some(menu => {
      if (menu.code == route.name) {
        return true
      }
      if (menu.children && menu.children.length > 0) {
        return hasPermission(menu.children, route)
      }
    })
  } else {
    return true
  }
}

function lazyLoadView(AsyncView) {
  const AsyncHandler = () => ({
    component: AsyncView,
    delay: 200,
    // Time before giving up trying to load the component.
    // Default: Infinity (milliseconds).
    timeout: 10000
  })
  return Promise.resolve({
    functional: true,
    render(h, { data, children }) {
      // Transparently pass any props or children
      // to the view component.
      return h(AsyncHandler, data, children)
    }
  })
}

/**
 * 递归过滤异步路由表，返回符合用户角色权限的路由表
 * @param menuList menu
 */
export function filterAsyncRoutes(menuList, isFirst) {
  const res = []
  let tempLayout = Layout
  if (isFirst) {
    tempLayout = empty
  }

  menuList.forEach(menu => {
    const tmp = {
      path: menu.route,
      redirect: 'noredirect',
      component: tempLayout,
      name: menu.code,
      meta: {
        code: menu.code,
        title: menu.name,
        icon: menu.icon
      },
      hidden: menu.hidden
    }
    if (menu.children && menu.children.length > 0) {
      tmp.children = filterAsyncRoutes(menu.children, 1)
    } else {
      try {
        if (isFirst == 1) {
          let tempPath = menu.route
          let tempFullPath = menu.route
          tmp.queryStr = ''
          if (tempPath.indexOf('?') > -1) {
            tempPath = tempPath.substring(0, tempPath.indexOf('?'))
            tmp.queryStr = '?' + encodeURIComponent(menu.route.substring(menu.route.indexOf('?') + 1))
            tmp.meta.queryStr = tmp.queryStr
            tempFullPath = tempPath + tmp.queryStr
            // tmp.query =getQueryStringArgs(menu.route.substring(tempPath.indexOf('?')+1))
          }
          const viewPath = tempPath
          tmp.fullPath = tempFullPath
          tmp.redirect = undefined
          tmp.path = '/' + menu.code
          tmp.meta.icon = menu.icon
          tmp.hidden = menu.hidden
          tmp.meta.affix = false

          if (viewPath.indexOf('http') != 0) {
            var pulseLoader = require(`@/views${viewPath}.vue`)
            tmp.component = r => require.ensure([], () => r(pulseLoader), 'index')
          } else {
            tmp.path = viewPath
          }
        } else {
          // 菜单在根目录
          tmp.name = ''
          tmp.path =  '/' + menu.code
          const tempFirstMenu = {
            path: menu.route,
            redirect: 'noredirect',
            component: tempLayout,
            name: menu.code,
            meta: {
              title: menu.name,
              code: menu.code,
              icon: menu.icon
            },
            hidden: menu.hidden
          }
          let tempPath = menu.route
          let tempFullPath = menu.route
          tempFirstMenu.queryStr = ''
          if (tempPath.indexOf('?') > -1) {
            tempPath = tempPath.substring(0, tempPath.indexOf('?'))
            tempFirstMenu.meta.queryStr = '?' + encodeURIComponent(menu.route.substring(menu.route.indexOf('?') + 1))
            tempFirstMenu.queryStr = tempFirstMenu.meta.queryStr
            tempFullPath = tempPath + tempFirstMenu.queryStr
            // tmp.query =getQueryStringArgs(menu.route.substring(tempPath.indexOf('?')+1))
          }
          const viewPath = tempPath
          tempFirstMenu.fullPath = tempFullPath
          tempFirstMenu.redirect = undefined
          tempFirstMenu.path = '/' + menu.code
          tempFirstMenu.meta.icon = menu.icon
          tempFirstMenu.hidden = menu.hidden
          tempFirstMenu.meta.affix = false
          if (tempPath.indexOf('http') != 0) {
            try{
            var pulseLoader = require(`@/views${viewPath}.vue`)

            tempFirstMenu.component = r => require.ensure([], () => r(pulseLoader), 'index')
            }catch{
              tempFirstMenu.component = error_404
            }
          } else {
            tempFirstMenu.path = tempPath
          }
          tmp.children = []

          tmp.children.push(tempFirstMenu)
        }
      } catch {
        tmp.component = error_404
      }
    }
    res.push(tmp)
  })

  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, menuList) {
    return new Promise(resolve => {

      let accessedRoutes=[];
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
      return;
      // if (roles.includes('admin')) {
      //   accessedRoutes = asyncRoutes
      // } else {
      // console.log(asyncRoutes)
      accessedRoutes = filterAsyncRoutes(menuList)
      // }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
