import { MessageBox, Message, Notification } from 'element-ui'

// 消息服务
class MessageService {
  // 通知-成功
  notifySuccess(message, title = '成功', duration = 3000) {
    Notification.success({
      title: title,
      message: message,
      duration: duration
    })
  }
  // 通知-失败
  notifyError(message, title = '失败', duration = 3000) {
    Notification.error({
      title: title,
      message: message,
      duration: duration
    })
  }
  // 消息提示-成功
  messageSuccess(message) {
    Message.success(message)
  }
  // 消息提示-错误
  messageError(message) {
    Message.error(message)
  }
  // 弹窗-提示
  alertInfo(message, title = '提示') {
    return MessageBox.alert(message, title)
  }
  // 弹窗-提示
  alertSuccess(message, title = '提示') {
    return MessageBox.alert(message, title, { type: 'success' })
  }
  // 弹窗-警告
  alertError(message, title = '提示') {
    return MessageBox.alert(message, title, { type: 'error' })
  }
  // 弹窗-确认
  confirmShow(message, title = '提示') {
    return MessageBox.confirm(message, title, {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    })
  }
  // 弹窗-提交内容
  promptShow(message, value, errorMessage) {
    return MessageBox.prompt(message, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      inputValidator: function(v) {
        if (v !== value) {
          return errorMessage
        }
      },
      inputErrorMessage: ''
    })
  }
}
export default MessageService
