﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeCheck.Service.Core;

namespace CodeCheck.Service.IDbService
{

    /// <summary>
    /// 质量数据子表
    /// </summary>
  [SugarTable("DataCollection_QualityData")]
    public class TestQualityDataEntity : BaseField, IEntity<long>
    {
        public TestQualityDataEntity()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
            //AcquisitionTime = DateTime.Now;
        }

        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true)]
        public long Id { get; set; }

        /// <summary>
        ///  产线编号
        /// </summary>
        public string LineCode { get; set; }

        /// <summary>
        ///  产线名称
        /// </summary>
        public string LineName { get; set; }

        /// <summary>
        ///  工序编码
        /// </summary>
        public string ProcessCode { get; set; }

        /// <summary>
        ///  工序名称
        /// </summary>
        public string ProcessName { get; set; }

        /// <summary>
        ///  工站编码
        /// </summary>
        public string WorkstationCode { get; set; }

        /// <summary>
        ///  工站名称
        /// </summary>
        public string WorkstationName { get; set; }

        /// <summary>
        ///  工步编码
        /// </summary>
        public string StepCode { get; set; }

        /// <summary>
        ///  工步名称
        /// </summary>
        public string StepName { get; set; }

        /// <summary>
        ///  条码类型3
        /// </summary>
        public string BarCodeType3 { get; set; }

        /// <summary>
        ///  过程条码
        /// </summary>
        public string ProcessBarCode { get; set; }

        /// <summary>
        ///  参数类型
        /// </summary>
        public string ParameterType { get; set; }

        /// <summary>
        ///  参数编码
        /// </summary>
        public string ParameterCode { get; set; }

        /// <summary>
        ///  参数编号
        /// </summary>
        public string ParameterNum { get; set; }

        /// <summary>
        ///  参数名称
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        ///  采集值
        /// </summary>
        public string CollectionValue { get; set; }

        /// <summary>
        ///  标准值
        /// </summary>
        public string StandardValues { get; set; }

        /// <summary>
        ///  上限值
        /// </summary>
        public decimal? UpperValue { get; set; }

        /// <summary>
        ///  下限值
        /// </summary>
        public decimal? DownValue { get; set; }

        /// <summary>
        ///  参数质量状态1：OK,2:NG
        /// </summary>
        public int? QualityStatus { get; set; }

        /// <summary>
        ///  采集时间
        /// </summary>
        public DateTime? AcquisitionTime => DateTime.Now;

        /// <summary>
        ///  托盘码
        /// </summary>
        public string TrayCode { get; set; }

        /// <summary>
        ///  主表ID
        /// </summary>
        public long? Pid { get; set; }

        /// <summary>
        ///  是否上传
        /// </summary>
        public bool? IsUpload { get; set; }

        /// <summary>
        ///  位置序号
        /// </summary>
        public int? PositionNo { get; set; }

        /// <summary>
        ///  工单号
        /// </summary>
        public string WONo { get; set; }

        /// <summary>
        /// 判定方
        /// </summary>
        public string JudgementBody { get; set; }

        /// <summary>
        /// 判定类型编码：【标准值，上限值，下限值，区间值】
        /// </summary>
        public string DetermineTypeCode { get; set; }

        /// <summary>
        /// 判定类型名称：【标准值，上限值，下限值，区间值】
        /// </summary>
        public string DetermineTypeName { get; set; }

        /// <summary>
        /// 采集值（字符串）
        /// </summary>
        public string CollectionStr { get; set; }
    }
 
    [SugarTable("DataCollection_QualityData")]
    [SugarIndex(null, nameof(ProcessBarCode), OrderByType.Desc)]//索引
    public class QuestDb_QualityDataEntity : BaseField, IEntity<long>
    {
        public QuestDb_QualityDataEntity()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
            //AcquisitionTime = DateTime.Now;
        }

        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true)]
        public long Id { get; set; }

        /// <summary>
        ///  产线编号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string LineCode { get; set; }

        /// <summary>
        ///  产线名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string LineName { get; set; }

        /// <summary>
        ///  工序编码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ProcessCode { get; set; }

        /// <summary>
        ///  工序名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ProcessName { get; set; }

        /// <summary>
        ///  工站编码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string WorkstationCode { get; set; }

        /// <summary>
        ///  工站名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string WorkstationName { get; set; }

        /// <summary>
        ///  工步编码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string StepCode { get; set; }

        /// <summary>
        ///  工步名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string StepName { get; set; }

        /// <summary>
        ///  条码类型3
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string BarCodeType3 { get; set; }

        /// <summary>
        ///  过程条码
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "过程条码", ColumnDataType = "symbol")]
        public string ProcessBarCode { get; set; }

        /// <summary>
        ///  参数类型
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ParameterType { get; set; }

        /// <summary>
        ///  参数编码
        /// </summary>
        public string ParameterCode { get; set; }

        /// <summary>
        ///  参数编号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ParameterNum { get; set; }

        /// <summary>
        ///  参数名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ParameterName { get; set; }

        /// <summary>
        ///  采集值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string CollectionValue { get; set; }

        /// <summary>
        ///  标准值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string StandardValues { get; set; }

        /// <summary>
        ///  上限值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public decimal? UpperValue { get; set; }

        /// <summary>
        ///  下限值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public decimal? DownValue { get; set; }

        /// <summary>
        ///  参数质量状态1：OK,2:NG
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public int? QualityStatus { get; set; }

        /// <summary>
        ///  采集时间
        /// </summary>
        [SugarColumn(IsNullable = false)]
        [TimeDbSplitField(DateType.Month)] //分区策略
        public DateTime? AcquisitionTime => DateTime.Now;

        /// <summary>
        ///  托盘码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string TrayCode { get; set; }

        /// <summary>
        ///  主表ID
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public long? Pid { get; set; }

        /// <summary>
        ///  是否上传
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public bool? IsUpload { get; set; }

        /// <summary>
        ///  位置序号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public int? PositionNo { get; set; }

        /// <summary>
        ///  工单号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string WONo { get; set; }

        /// <summary>
        /// 判定方
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string JudgementBody { get; set; }

        /// <summary>
        /// 判定类型编码：【标准值，上限值，下限值，区间值】
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string DetermineTypeCode { get; set; }

        /// <summary>
        /// 判定类型名称：【标准值，上限值，下限值，区间值】
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string DetermineTypeName { get; set; }

        /// <summary>
        /// 采集值（字符串）
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string CollectionStr { get; set; }
    }

    [SugarTable("Basic_AutoNumber")]
    public class BasicAutonumber
    {
        /// <summary>
        /// 主键 
        ///</summary>
        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true)]
        public long Id { get; set; }
        /// <summary>
        /// 线别编码 
        ///</summary>
        [SugarColumn(ColumnName = "Code")]
        public string Code { get; set; }
        /// <summary>
        /// 归属日 
        ///</summary>
        [SugarColumn(ColumnName = "DateTime")]
        public DateTime? DateTime { get; set; }
        /// <summary>
        /// 当前号 
        ///</summary>
        [SugarColumn(ColumnName = "AutoNumber")]
        public int? AutoNumber { get; set; }
        /// <summary>
        /// 是否启用 
        ///</summary>
        [SugarColumn(ColumnName = "IsEnable")]
        public bool IsEnable { get; set; }
        /// <summary>
        /// 是否删除 
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "IsDeleted")]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// 创建人id 
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "CreaterId")]
        public long CreaterId { get; set; }
        /// <summary>
        /// 创建时间 
        /// 默认值: (getdate())
        ///</summary>
        [SugarColumn(ColumnName = "CreateTime")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新人id 
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "ModifierId")]
        public long? ModifierId { get; set; }
        /// <summary>
        /// 更新时间 
        /// 默认值: (getdate())
        ///</summary>
        [SugarColumn(ColumnName = "ModifyTime")]
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 标签样式编码 
        ///</summary>
        [SugarColumn(ColumnName = "StyleCode")]
        public string StyleCode { get; set; }
    }
}
