﻿using CodeCheck.Service.Core;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeCheck.Service.IDbService.Models
{

    [SplitTable(SplitType.Month)]//指定按照时间分表
    [SugarTable("QualityData_{year}{month}{day}")]
    [SugarIndex(null, nameof(ProcessBarCode), OrderByType.Desc)]//索引
    public class QualityData_History : BaseField
    {
        public QualityData_History()
        {
            Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
            //AcquisitionTime = DateTime.Now;
        }

        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true)]
        public long Id { get; set; }

        /// <summary>
        ///  产线编号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string LineCode { get; set; }

        /// <summary>
        ///  产线名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string LineName { get; set; }

        /// <summary>
        ///  工序编码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ProcessCode { get; set; }

        /// <summary>
        ///  工序名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ProcessName { get; set; }

        /// <summary>
        ///  工站编码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string WorkstationCode { get; set; }

        /// <summary>
        ///  工站名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string WorkstationName { get; set; }

        /// <summary>
        ///  工步编码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string StepCode { get; set; }

        /// <summary>
        ///  工步名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string StepName { get; set; }

        /// <summary>
        ///  条码类型3
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string BarCodeType3 { get; set; }

        /// <summary>
        ///  过程条码
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "过程条码")]
        public string ProcessBarCode { get; set; }

        /// <summary>
        ///  参数类型
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ParameterType { get; set; }

        /// <summary>
        ///  参数编码
        /// </summary>
        public string ParameterCode { get; set; }

        /// <summary>
        ///  参数编号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ParameterNum { get; set; }

        /// <summary>
        ///  参数名称
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ParameterName { get; set; }

        /// <summary>
        ///  采集值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string CollectionValue { get; set; }

        /// <summary>
        ///  标准值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string StandardValues { get; set; }

        /// <summary>
        ///  上限值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public decimal? UpperValue { get; set; }

        /// <summary>
        ///  下限值
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public decimal? DownValue { get; set; }

        /// <summary>
        ///  参数质量状态1：OK,2:NG
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public int? QualityStatus { get; set; }

        /// <summary>
        ///  采集时间
        /// </summary>
        [SugarColumn(IsNullable = false)]
        [SplitField]
        public DateTime? AcquisitionTime { set; get; } = DateTime.Now;

        /// <summary>
        ///  托盘码
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string TrayCode { get; set; }

        /// <summary>
        ///  主表ID
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public long? Pid { get; set; }

        /// <summary>
        ///  是否上传
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public bool? IsUpload { get; set; }

        /// <summary>
        ///  位置序号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public int? PositionNo { get; set; }

        /// <summary>
        ///  工单号
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string WONo { get; set; }

        /// <summary>
        /// 判定方
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string JudgementBody { get; set; }

        /// <summary>
        /// 判定类型编码：【标准值，上限值，下限值，区间值】
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string DetermineTypeCode { get; set; }

        /// <summary>
        /// 判定类型名称：【标准值，上限值，下限值，区间值】
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string DetermineTypeName { get; set; }

        /// <summary>
        /// 采集值（字符串）
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string CollectionStr { get; set; }
    }

}
