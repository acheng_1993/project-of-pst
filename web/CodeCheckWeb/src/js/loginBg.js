class ShaderProgram {
  constructor(holder, mycanvas, options = {}) {
    options = Object.assign({
      antialias: false,
      depthTest: false,
      mousemove: false,
      autosize: true,
      side: 'front',
      vertex: `
          precision highp float;
  
          attribute vec4 a_position;
          attribute vec4 a_color;
  
          uniform float u_time;
          uniform vec2 u_resolution;
          uniform vec2 u_mousemove;
          uniform mat4 u_projection;
  
          varying vec4 v_color;
  
          void main() {
  
            gl_Position = u_projection * a_position;
            gl_PointSize = (10.0 / gl_Position.w) * 100.0;
  
            v_color = a_color;
  
          }`,
      fragment: `
          precision highp float;
  
          uniform sampler2D u_texture;
          uniform int u_hasTexture;
  
          varying vec4 v_color;
  
          void main() {
  
            if ( u_hasTexture == 1 ) {
  
              gl_FragColor = v_color * texture2D(u_texture, gl_PointCoord);
  
            } else {
  
              gl_FragColor = v_color;
  
            }
  
          }`,
      uniforms: {},
      buffers: {},
      camera: {},
      texture: null,
      onUpdate: () => {},
      onResize: () => {}
    }, options)

    const uniforms = Object.assign({
      time: { type: 'float', value: 0 },
      hasTexture: { type: 'int', value: 0 },
      resolution: { type: 'vec2', value: [0, 0] },
      mousemove: { type: 'vec2', value: [0, 0] },
      projection: { type: 'mat4', value: [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1] }
    }, options.uniforms)

    const buffers = Object.assign({
      position: { size: 3, data: [] },
      color: { size: 4, data: [] }
    }, options.buffers)

    const camera = Object.assign({
      fov: 60,
      near: 1,
      far: 10000,
      aspect: 1,
      z: 100,
      perspective: true
    }, options.camera)

    const gl = mycanvas.getContext('webgl', { antialias: options.antialias })

    if (!gl) return false

    this.count = 0
    this.gl = gl
    this.canvas = mycanvas
    this.camera = camera
    this.holder = holder
    this.onUpdate = options.onUpdate
    this.onResize = options.onResize
    this.data = {}

    // holder.appendChild( canvas )

    this.createProgram(options.vertex, options.fragment)

    this.createBuffers(buffers)
    this.createUniforms(uniforms)

    this.updateBuffers()
    this.updateUniforms()

    this.createTexture(options.texture)

    gl.enable(gl.BLEND)
    gl.enable(gl.CULL_FACE)
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE)
    gl[ options.depthTest ? 'enable' : 'disable' ](gl.DEPTH_TEST)

    if (options.autosize) { window.addEventListener('resize', e => this.resize(e), false) }
    if (options.mousemove) { window.addEventListener('mousemove', e => this.mousemove(e), false) }

    this.resize()

    this.update = this.update.bind(this)
    this.time = { start: performance.now(), old: performance.now() }
    this.update()
  }

  mousemove(e) {
    const x = e.pageX / this.width * 2 - 1
    const y = e.pageY / this.height * 2 - 1

    this.uniforms.mousemove = [x, y]
  }

  resize(e) {
    const holder = this.holder
    const canvas = this.canvas
    const gl = this.gl

    const width = this.width = holder.offsetWidth
    const height = this.height = holder.offsetHeight
    const aspect = this.aspect = width / height
    const dpi = this.dpi = devicePixelRatio

    canvas.width = width * dpi
    canvas.height = height * dpi
    canvas.style.width = width + 'px'
    canvas.style.height = height + 'px'

    gl.viewport(0, 0, width * dpi, height * dpi)
    gl.clearColor(0, 0, 0, 0)

    this.uniforms.resolution = [width, height]
    this.uniforms.projection = this.setProjection(aspect)

    this.onResize(width, height, dpi)
  }

  setProjection(aspect) {
    const camera = this.camera

    if (camera.perspective) {
      camera.aspect = aspect

      const fovRad = camera.fov * (Math.PI / 180)
      const f = Math.tan(Math.PI * 0.5 - 0.5 * fovRad)
      const rangeInv = 1.0 / (camera.near - camera.far)

      const matrix = [
        f / camera.aspect, 0, 0, 0,
        0, f, 0, 0,
        0, 0, (camera.near + camera.far) * rangeInv, -1,
        0, 0, camera.near * camera.far * rangeInv * 2, 0
      ]

      matrix[ 14 ] += camera.z
      matrix[ 15 ] += camera.z

      return matrix
    } else {
      return [
        2 / this.width, 0, 0, 0,
        0, -2 / this.height, 0, 0,
        0, 0, 1, 0,
        -1, 1, 0, 1
      ]
    }
  }

  createShader(type, source) {
    const gl = this.gl
    const shader = gl.createShader(type)

    gl.shaderSource(shader, source)
    gl.compileShader(shader)

    if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      return shader
    } else {
      console.log(gl.getShaderInfoLog(shader))
      gl.deleteShader(shader)
    }
  }

  createProgram(vertex, fragment) {
    const gl = this.gl

    const vertexShader = this.createShader(gl.VERTEX_SHADER, vertex)
    const fragmentShader = this.createShader(gl.FRAGMENT_SHADER, fragment)

    const program = gl.createProgram()

    gl.attachShader(program, vertexShader)
    gl.attachShader(program, fragmentShader)
    gl.linkProgram(program)

    if (gl.getProgramParameter(program, gl.LINK_STATUS)) {
      gl.useProgram(program)
      this.program = program
    } else {
      console.log(gl.getProgramInfoLog(program))
      gl.deleteProgram(program)
    }
  }

  createUniforms(data) {
    const gl = this.gl
    const uniforms = this.data.uniforms = data
    const values = this.uniforms = {}

    Object.keys(uniforms).forEach(name => {
      const uniform = uniforms[ name ]

      uniform.location = gl.getUniformLocation(this.program, 'u_' + name)

      Object.defineProperty(values, name, {
        set: value => {
          uniforms[ name ].value = value
          this.setUniform(name, value)
        },
        get: () => uniforms[ name ].value
      })
    })
  }

  setUniform(name, value) {
    const gl = this.gl
    const uniform = this.data.uniforms[ name ]

    uniform.value = value

    switch (uniform.type) {
      case 'int': {
        gl.uniform1i(uniform.location, value)
        break
      }
      case 'float': {
        gl.uniform1f(uniform.location, value)
        break
      }
      case 'vec2': {
        gl.uniform2f(uniform.location, ...value)
        break
      }
      case 'vec3': {
        gl.uniform3f(uniform.location, ...value)
        break
      }
      case 'vec4': {
        gl.uniform4f(uniform.location, ...value)
        break
      }
      case 'mat2': {
        gl.uniformMatrix2fv(uniform.location, false, value)
        break
      }
      case 'mat3': {
        gl.uniformMatrix3fv(uniform.location, false, value)
        break
      }
      case 'mat4': {
        gl.uniformMatrix4fv(uniform.location, false, value)
        break
      }
    }

    // ivec2       : uniform2i,
    // ivec3       : uniform3i,
    // ivec4       : uniform4i,
    // sampler2D   : uniform1i,
    // samplerCube : uniform1i,
    // bool        : uniform1i,
    // bvec2       : uniform2i,
    // bvec3       : uniform3i,
    // bvec4       : uniform4i,
  }

  updateUniforms() {
    const gl = this.gl
    const uniforms = this.data.uniforms

    Object.keys(uniforms).forEach(name => {
      const uniform = uniforms[ name ]

      this.uniforms[ name ] = uniform.value
    })
  }

  createBuffers(data) {
    const gl = this.gl
    const buffers = this.data.buffers = data
    const values = this.buffers = {}

    Object.keys(buffers).forEach(name => {
      const buffer = buffers[ name ]

      buffer.buffer = this.createBuffer('a_' + name, buffer.size)

      Object.defineProperty(values, name, {
        set: data => {
          buffers[ name ].data = data
          this.setBuffer(name, data)

          if (name == 'position') { this.count = buffers.position.data.length / 3 }
        },
        get: () => buffers[ name ].data
      })
    })
  }

  createBuffer(name, size) {
    const gl = this.gl
    const program = this.program

    const index = gl.getAttribLocation(program, name)
    const buffer = gl.createBuffer()

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
    gl.enableVertexAttribArray(index)
    gl.vertexAttribPointer(index, size, gl.FLOAT, false, 0, 0)

    return buffer
  }

  setBuffer(name, data) {
    const gl = this.gl
    const buffers = this.data.buffers

    if (name == null && !gl.bindBuffer(gl.ARRAY_BUFFER, null)) return

    gl.bindBuffer(gl.ARRAY_BUFFER, buffers[ name ].buffer)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW)
  }

  updateBuffers() {
    const gl = this.gl
    const buffers = this.buffers

    Object.keys(buffers).forEach(name =>
      buffers[ name ] = buffer.data
    )

    this.setBuffer(null)
  }

  createTexture(src) {
    const gl = this.gl
    const texture = gl.createTexture()

    gl.bindTexture(gl.TEXTURE_2D, texture)
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 0, 0]))

    this.texture = texture

    if (src) {
      this.uniforms.hasTexture = 1
      this.loadTexture(src)
    }
  }

  loadTexture(src) {
    const gl = this.gl
    const texture = this.texture

    const textureImage = new Image()

    textureImage.onload = () => {
      gl.bindTexture(gl.TEXTURE_2D, texture)

      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, textureImage)

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

      // gl.generateMipmap( gl.TEXTURE_2D )
    }

    textureImage.src = src
  }

  update() {
    const gl = this.gl

    const now = performance.now()
    const elapsed = (now - this.time.start) / 5000
    const delta = now - this.time.old
    this.time.old = now

    this.uniforms.time = elapsed

    if (this.count > 0) {
      gl.clear(gl.COLORBUFFERBIT)
      gl.drawArrays(gl.POINTS, 0, this.count)
    }

    this.onUpdate(delta)
    if(isRun){
    requestAnimationFrame(this.update)
    }
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}
const pointSize = 3

/**
 * 初始化 背景
 */
function initBackground() {
  var canvas = document.createElement('canvas')
  canvas.setAttribute('id', 'myloginCanvas')
  document.body.append(canvas)
  const ranNum = getRandomInt(0, 2);
  let wavesArr = [waves,waves2,waves4];
  
  wavesArr[ranNum](canvas);
}

//链接点线
function waves4(canvasEle){
  document.addEventListener('DOMContentLoaded', function () {
    const canvas = canvasEle;
    const ctx = canvas.getContext('2d');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    ctx.lineWidth = 0.3;

    const mousePosition = {
        x: 30 * canvas.width / 100,
        y: 30 * canvas.height / 100
    };

    const dots = {
        nb: 750,
        distance: 50,
        d_radius: 100,
        array: []
    };

    function colorValue(min) {
        return Math.floor(Math.random() * 255 + min);
    }

    function createColorStyle(r, g, b) {
        return 'rgba(' + r + ',' + g + ',' + b + ', 0.8)';
    }

    function mixComponents(comp1, weight1, comp2, weight2) {
        return (comp1 * weight1 + comp2 * weight2) / (weight1 + weight2);
    }

    function averageColorStyles(dot1, dot2) {
        const color1 = dot1.color;
        const color2 = dot2.color;
        const r = mixComponents(color1.r, dot1.radius, color2.r, dot2.radius);
        const g = mixComponents(color1.g, dot1.radius, color2.g, dot2.radius);
        const b = mixComponents(color1.b, dot1.radius, color2.b, dot2.radius);
        return createColorStyle(Math.floor(r), Math.floor(g), Math.floor(b));
    }

    function Color(min) {
        min = min || 0;
        this.r = colorValue(min);
        this.g = colorValue(min);
        this.b = colorValue(min);
        this.style = createColorStyle(this.r, this.g, this.b);
    }

    function Dot() {
        this.x = Math.random() * canvas.width;
        this.y = Math.random() * canvas.height;
        this.vx = -.5 + Math.random();
        this.vy = -.5 + Math.random();
        this.radius = Math.random() * 2;
        this.color = new Color();
    }

    Dot.prototype = {
        draw: function () {
            ctx.beginPath();
            ctx.fillStyle = this.color.style;
            ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
            ctx.fill();
        }
    };

    function createDots() {
        for (let i = 0; i < dots.nb; i++) {
            dots.array.push(new Dot());
        }
    }

    function moveDots() {
        for (let i = 0; i < dots.nb; i++) {
            const dot = dots.array[i];
            if (dot.y < 0 || dot.y > canvas.height) {
                dot.vx = dot.vx;
                dot.vy = -dot.vy;
            } else if (dot.x < 0 || dot.x > canvas.width) {
                dot.vx = -dot.vx;
                dot.vy = dot.vy;
            }
            dot.x += dot.vx;
            dot.y += dot.vy;
        }
    }

    function connectDots() {
        for (let i = 0; i < dots.nb; i++) {
            for (let j = 0; j < dots.nb; j++) {
                const i_dot = dots.array[i];
                const j_dot = dots.array[j];
                if ((i_dot.x - j_dot.x) < dots.distance && (i_dot.y - j_dot.y) < dots.distance && (i_dot.x - j_dot.x) > -dots.distance && (i_dot.y - j_dot.y) > -dots.distance) {
                    if ((i_dot.x - mousePosition.x) < dots.d_radius && (i_dot.y - mousePosition.y) < dots.d_radius && (i_dot.x - mousePosition.x) > -dots.d_radius && (i_dot.y - mousePosition.y) > -dots.d_radius) {
                        ctx.beginPath();
                        ctx.strokeStyle = averageColorStyles(i_dot, j_dot);
                        ctx.moveTo(i_dot.x, i_dot.y);
                        ctx.lineTo(j_dot.x, j_dot.y);
                        ctx.stroke();
                        ctx.closePath();
                    }
                }
            }
        }
    }

    function drawDots() {
        for (let i = 0; i < dots.nb; i++) {
            const dot = dots.array[i];
            dot.draw();
        }
    }

    function animateDots() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        moveDots();
        connectDots();
        drawDots();
        if(isRun){
        requestAnimationFrame(animateDots);
        }
    }

    canvas.addEventListener('mousemove', function (e) {
        mousePosition.x = e.pageX;
        mousePosition.y = e.pageY;
    });

    canvas.addEventListener('mouseleave', function () {
        mousePosition.x = canvas.width / 2;
        mousePosition.y = canvas.height / 2;
    });

    createDots();
    requestAnimationFrame(animateDots);
});

}


//星空
function waves2(canvasEle) {
  var num = 200
  var w = window.innerWidth
  var h = window.innerHeight
  var max = 100
  var _x = 0
  var _y = 0
  var _z = 150
  var dtr = function(d) {
    return d * Math.PI / 180
  }

  var rnd = function() {
    return Math.sin(Math.floor(Math.random() * 360) * Math.PI / 180)
  }
  var dist = function(p1, p2, p3) {
    return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2) + Math.pow(p2.z - p1.z, 2))
  }

  var cam = {
    obj: {
      x: _x,
      y: _y,
      z: _z
    },
    dest: {
      x: 0,
      y: 0,
      z: 1
    },
    dist: {
      x: 0,
      y: 0,
      z: 200
    },
    ang: {
      cplane: 0,
      splane: 0,
      ctheta: 0,
      stheta: 0
    },
    zoom: 1,
    disp: {
      x: w / 2,
      y: h / 2,
      z: 0
    },
    upd: function() {
      cam.dist.x = cam.dest.x - cam.obj.x
      cam.dist.y = cam.dest.y - cam.obj.y
      cam.dist.z = cam.dest.z - cam.obj.z
      cam.ang.cplane = -cam.dist.z / Math.sqrt(cam.dist.x * cam.dist.x + cam.dist.z * cam.dist.z)
      cam.ang.splane = cam.dist.x / Math.sqrt(cam.dist.x * cam.dist.x + cam.dist.z * cam.dist.z)
      cam.ang.ctheta = Math.sqrt(cam.dist.x * cam.dist.x + cam.dist.z * cam.dist.z) / Math.sqrt(cam.dist.x * cam.dist.x + cam.dist.y * cam.dist.y + cam.dist.z * cam.dist.z)
      cam.ang.stheta = -cam.dist.y / Math.sqrt(cam.dist.x * cam.dist.x + cam.dist.y * cam.dist.y + cam.dist.z * cam.dist.z)
    }
  }

  var trans = {
    parts: {
      sz: function(p, sz) {
        return {
          x: p.x * sz.x,
          y: p.y * sz.y,
          z: p.z * sz.z
        }
      },
      rot: {
        x: function(p, rot) {
          return {
            x: p.x,
            y: p.y * Math.cos(dtr(rot.x)) - p.z * Math.sin(dtr(rot.x)),
            z: p.y * Math.sin(dtr(rot.x)) + p.z * Math.cos(dtr(rot.x))
          }
        },
        y: function(p, rot) {
          return {
            x: p.x * Math.cos(dtr(rot.y)) + p.z * Math.sin(dtr(rot.y)),
            y: p.y,
            z: -p.x * Math.sin(dtr(rot.y)) + p.z * Math.cos(dtr(rot.y))
          }
        },
        z: function(p, rot) {
          return {
            x: p.x * Math.cos(dtr(rot.z)) - p.y * Math.sin(dtr(rot.z)),
            y: p.x * Math.sin(dtr(rot.z)) + p.y * Math.cos(dtr(rot.z)),
            z: p.z
          }
        }
      },
      pos: function(p, pos) {
        return {
          x: p.x + pos.x,
          y: p.y + pos.y,
          z: p.z + pos.z
        }
      }
    },
    pov: {
      plane: function(p) {
        return {
          x: p.x * cam.ang.cplane + p.z * cam.ang.splane,
          y: p.y,
          z: p.x * -cam.ang.splane + p.z * cam.ang.cplane
        }
      },
      theta: function(p) {
        return {
          x: p.x,
          y: p.y * cam.ang.ctheta - p.z * cam.ang.stheta,
          z: p.y * cam.ang.stheta + p.z * cam.ang.ctheta
        }
      },
      set: function(p) {
        return {
          x: p.x - cam.obj.x,
          y: p.y - cam.obj.y,
          z: p.z - cam.obj.z
        }
      }
    },
    persp: function(p) {
      return {
        x: p.x * cam.dist.z / p.z * cam.zoom,
        y: p.y * cam.dist.z / p.z * cam.zoom,
        z: p.z * cam.zoom,
        p: cam.dist.z / p.z
      }
    },
    disp: function(p, disp) {
      return {
        x: p.x + disp.x,
        y: -p.y + disp.y,
        z: p.z + disp.z,
        p: p.p
      }
    },
    steps: function(_obj_, sz, rot, pos, disp) {
      var _args = trans.parts.sz(_obj_, sz)
      _args = trans.parts.rot.x(_args, rot)
      _args = trans.parts.rot.y(_args, rot)
      _args = trans.parts.rot.z(_args, rot)
      _args = trans.parts.pos(_args, pos)
      _args = trans.pov.plane(_args)
      _args = trans.pov.theta(_args)
      _args = trans.pov.set(_args)
      _args = trans.persp(_args)
      _args = trans.disp(_args, disp)
      return _args
    }
  };

  (function() {
    'use strict'
    var threeD = function(param) {
      this.transIn = {}
      this.transOut = {}
      this.transIn.vtx = (param.vtx)
      this.transIn.sz = (param.sz)
      this.transIn.rot = (param.rot)
      this.transIn.pos = (param.pos)
    }

    threeD.prototype.vupd = function() {
      this.transOut = trans.steps(

        this.transIn.vtx,
        this.transIn.sz,
        this.transIn.rot,
        this.transIn.pos,
        cam.disp
      )
    }

    var Build = function() {
      this.vel = 0.04
      this.lim = 360
      this.diff = 200
      this.initPos = 100
      this.toX = _x
      this.toY = _y
      this.go()
    }

    Build.prototype.go = function() {
      this.canvas = canvasEle
      this.canvas.width = window.innerWidth
      this.canvas.height = window.innerHeight
      this.$ = canvasEle.getContext('2d')
      this.$.globalCompositeOperation = 'source-over'
      this.varr = []
      this.dist = []
      this.calc = []

      for (var i = 0, len = num; i < len; i++) {
        this.add()
      }

      this.rotObj = {
        x: 0,
        y: 0,
        z: 0
      }
      this.objSz = {
        x: w / 5,
        y: h / 5,
        z: w / 5
      }
    }

    Build.prototype.add = function() {
      this.varr.push(new threeD({
        vtx: {
          x: rnd(),
          y: rnd(),
          z: rnd()
        },
        sz: {
          x: 0,
          y: 0,
          z: 0
        },
        rot: {
          x: 20,
          y: -20,
          z: 0
        },
        pos: {
          x: this.diff * Math.sin(360 * Math.random() * Math.PI / 180),
          y: this.diff * Math.sin(360 * Math.random() * Math.PI / 180),
          z: this.diff * Math.sin(360 * Math.random() * Math.PI / 180)
        }
      }))
      this.calc.push({
        x: 360 * Math.random(),
        y: 360 * Math.random(),
        z: 360 * Math.random()
      })
    }

    Build.prototype.upd = function() {
      cam.obj.x += (this.toX - cam.obj.x) * 0.05
      cam.obj.y += (this.toY - cam.obj.y) * 0.05
    }

    Build.prototype.draw = function() {
      this.$.clearRect(0, 0, this.canvas.width, this.canvas.height)
      cam.upd()
      this.rotObj.x += 0.1
      this.rotObj.y += 0.1
      this.rotObj.z += 0.1

      for (var i = 0; i < this.varr.length; i++) {
        for (var val in this.calc[i]) {
          if (this.calc[i].hasOwnProperty(val)) {
            this.calc[i][val] += this.vel
            if (this.calc[i][val] > this.lim) this.calc[i][val] = 0
          }
        }

        this.varr[i].transIn.pos = {
          x: this.diff * Math.cos(this.calc[i].x * Math.PI / 180),
          y: this.diff * Math.sin(this.calc[i].y * Math.PI / 180),
          z: this.diff * Math.sin(this.calc[i].z * Math.PI / 180)
        }
        this.varr[i].transIn.rot = this.rotObj
        this.varr[i].transIn.sz = this.objSz
        this.varr[i].vupd()
        if (this.varr[i].transOut.p < 0) continue
        var g = this.$.createRadialGradient(this.varr[i].transOut.x, this.varr[i].transOut.y, this.varr[i].transOut.p, this.varr[i].transOut.x, this.varr[i].transOut.y, this.varr[i].transOut.p * 2)
        this.$.globalCompositeOperation = 'lighter'
        g.addColorStop(0, 'hsla(255, 255%, 255%, 1)')
        g.addColorStop(0.5, 'hsla(' + (i + 2) + ',85%, 40%,1)')
        g.addColorStop(1, 'hsla(' + (i) + ',85%, 40%,.5)')
        this.$.fillStyle = g
        this.$.beginPath()
        this.$.arc(this.varr[i].transOut.x, this.varr[i].transOut.y, this.varr[i].transOut.p * 2, 0, Math.PI * 2, false)
        this.$.fill()
        this.$.closePath()
      }
    }
    Build.prototype.anim = function() {
      window.requestAnimationFrame = (function() {
        return window.requestAnimationFrame ||
          function(callback, element) {
            window.setTimeout(callback, 1000 / 60)
          }
      })()
      var anim = function() {
        this.upd()
        this.draw()
        if(isRun){
        window.requestAnimationFrame(anim)
      }
      }.bind(this)
   
      window.requestAnimationFrame(anim)
 
    }

    Build.prototype.run = function() {
      this.anim()

      window.addEventListener('mousemove', function(e) {
        this.toX = (e.clientX - this.canvas.width / 2) * -0.8
        this.toY = (e.clientY - this.canvas.height / 2) * 0.8
      }.bind(this))
      window.addEventListener('touchmove', function(e) {
        e.preventDefault()
        this.toX = (e.touches[0].clientX - this.canvas.width / 2) * -0.8
        this.toY = (e.touches[0].clientY - this.canvas.height / 2) * 0.8
      }.bind(this))
      window.addEventListener('mousedown', function(e) {
        for (var i = 0; i < 100; i++) {
          this.add()
        }
      }.bind(this))
      window.addEventListener('touchstart', function(e) {
        e.preventDefault()
        for (var i = 0; i < 100; i++) {
          this.add()
        }
      }.bind(this))
    }
    var app = new Build()
    app.run()
  })()
  window.addEventListener('resize', function() {
    canvas.width = w = window.innerWidth
    canvas.height = h = window.innerHeight
  }, false)
}


//瓦片
function waves3(c) {
  var w = c.width = window.innerWidth
  var h = c.height = window.innerHeight
  var ctx = c.getContext('2d')
  var opts = {
    len: 20,
    count: 50,
    baseTime: 10,
    addedTime: 10,
    dieChance: 0.05,
    spawnChance: 1,
    sparkChance: 0.1,
    sparkDist: 10,
    sparkSize: 2,
    color: 'hsl(hue,100%,light%)',
    baseLight: 50,
    addedLight: 10,
    shadowToTimePropMult: 6,
    baseLightInputMultiplier: 0.01,
    addedLightInputMultiplier: 0.02,
    cx: w / 2,
    cy: h / 2,
    repaintAlpha: 0.04,
    hueChange: 0.1
  }
  var tick = 0
  var lines = []
  var dieX = w / 2 / opts.len
  var dieY = h / 2 / opts.len
  var baseRad = Math.PI * 2 / 6
  ctx.fillStyle = 'black'
  ctx.fillRect(0, 0, w, h)
  function loop() {
    if(isRun){
    window.requestAnimationFrame(loop); ++tick
    ctx.globalCompositeOperation = 'source-over'
    ctx.shadowBlur = 0
    ctx.fillStyle = 'rgba(0,0,0,alp)'.replace('alp', opts.repaintAlpha)
    ctx.fillRect(0, 0, w, h)
    ctx.globalCompositeOperation = 'lighter'
    if (lines.length < opts.count && Math.random() < opts.spawnChance) lines.push(new Line())
    lines.map(function(line) {
      line.step()
    })
  }
  }
  function Line() {
    this.reset()
  }
  Line.prototype.reset = function() {
    this.x = 0
    this.y = 0
    this.addedX = 0
    this.addedY = 0
    this.rad = 0
    this.lightInputMultiplier = opts.baseLightInputMultiplier + opts.addedLightInputMultiplier * Math.random()
    this.color = opts.color.replace('hue', tick * opts.hueChange)
    this.cumulativeTime = 0
    this.beginPhase()
  }
  Line.prototype.beginPhase = function() {
    this.x += this.addedX
    this.y += this.addedY
    this.time = 0
    this.targetTime = (opts.baseTime + opts.addedTime * Math.random()) | 0
    this.rad += baseRad * (Math.random() < 0.5 ? 1 : -1)
    this.addedX = Math.cos(this.rad)
    this.addedY = Math.sin(this.rad)
    if (Math.random() < opts.dieChance || this.x > dieX || this.x < -dieX || this.y > dieY || this.y < -dieY) this.reset()
  }
  Line.prototype.step = function() {
    ++this.time; ++this.cumulativeTime
    if (this.time >= this.targetTime) this.beginPhase()
    var prop = this.time / this.targetTime
    var wave = Math.sin(prop * Math.PI / 2)
    var x = this.addedX * wave
    var y = this.addedY * wave
    ctx.shadowBlur = prop * opts.shadowToTimePropMult
    ctx.fillStyle = ctx.shadowColor = this.color.replace('light', opts.baseLight + opts.addedLight * Math.sin(this.cumulativeTime * this.lightInputMultiplier))
    ctx.fillRect(opts.cx + (this.x + x) * opts.len, opts.cy + (this.y + y) * opts.len, 2, 2)
    if (Math.random() < opts.sparkChance) ctx.fillRect(opts.cx + (this.x + x) * opts.len + Math.random() * opts.sparkDist * (Math.random() < 0.5 ? 1 : -1) - opts.sparkSize / 2, opts.cy + (this.y + y) * opts.len + Math.random() * opts.sparkDist * (Math.random() < 0.5 ? 1 : -1) - opts.sparkSize / 2, opts.sparkSize, opts.sparkSize)
  }
  loop()
  window.addEventListener('resize',
    function() {
      w = c.width = window.innerWidth
      h = c.height = window.innerHeight
      ctx.fillStyle = 'black'
      ctx.fillRect(0, 0, w, h)
      opts.cx = w / 2
      opts.cy = h / 2
      dieX = w / 2 / opts.len
      dieY = h / 2 / opts.len
    })
}

//波浪
function waves(ele) {
  new ShaderProgram(document.body, ele, {
    texture: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAb1BMVEUAAAD///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8v0wLRAAAAJHRSTlMAC/goGvDhmwcExrVjWzrm29TRqqSKenRXVklANSIUE8mRkGpv+HOfAAABCElEQVQ4y4VT13LDMAwLrUHteO+R9f/fWMfO6dLaPeKVEECRxOULWsEGpS9nULDwia2Y+ALqUNbAWeg775zv+sA4/FFRMxt8U2FZFCVWjR/YrH4/H9sarclSKdPMWKzb8VsEeHB3m0shkhVCyNzeXeAQ9Xl4opEieX2QCGnwGbj6GMyjw9t1K0fK9YZunPXeAGsfJtYjwzxaBnozGGorYz0ypK2HzQSYx1y8DgSRo2ewOiyh2QWOEk1Y9OrQV0a8TiBM1a8eMHWYnRMy7CZ4t1CmyRkhSUvP3gRXyHOCLBxNoC3IJv//ZrJ/kxxUHPUB+6jJZZHrpg6GOjnqaOmzp4NDR48OLxn/H27SRQ08S0ZJAAAAAElFTkSuQmCC',
    uniforms: {
      size: { type: 'float', value: pointSize },
      field: { type: 'vec3', value: [0, 0, 0] },
      speed: { type: 'float', value: 5 }
    },
    vertex: `
      #define M_PI 3.1415926535897932384626433832795
  
      precision highp float;
  
      attribute vec4 a_position;
      attribute vec4 a_color;
  
      uniform float u_time;
      uniform float u_size;
      uniform float u_speed;
      uniform vec3 u_field;
      uniform mat4 u_projection;
  
      varying vec4 v_color;
  
      void main() {
  
        vec3 pos = a_position.xyz;
  
        pos.y += (
          cos(pos.x / u_field.x * M_PI * 8.0 + u_time * u_speed) +
          sin(pos.z / u_field.z * M_PI * 8.0 + u_time * u_speed)
        ) * u_field.y;
  
        gl_Position = u_projection * vec4( pos.xyz, a_position.w );
        gl_PointSize = ( u_size / gl_Position.w ) * 100.0;
  
        v_color = a_color;
  
      }`,
    fragment: `
      precision highp float;
  
      uniform sampler2D u_texture;
  
      varying vec4 v_color;
  
      void main() {
  
        gl_FragColor = v_color * texture2D(u_texture, gl_PointCoord);
  
      }`,
    onResize(w, h, dpi) {
      const position = []; const color = []

      const width = 400 * (w / h)
      const depth = 400
      const height = 3
      const distance = 5

      for (let x = 0; x < width; x += distance) {
        for (let z = 0; z < depth; z += distance) {
          position.push(-width / 2 + x, -30, -depth / 2 + z)
          color.push(0, 1 - (x / width) * 1, 0.5 + x / width * 0.5, z / depth)
        }
      }

      this.uniforms.field = [width, height, depth]

      this.buffers.position = position
      this.buffers.color = color

      this.uniforms.size = (h / 400) * pointSize * dpi
    }
  })
}
var isRun=true;
function removeBackground(){
  isRun = false;
  document.getElementById('myloginCanvas').remove();
  
}

export default {
  initBackground,
  removeBackground
}

