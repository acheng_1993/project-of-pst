import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const basicConfigRouter = 'BasicConfig'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class BasicConfig {
  getBasicConfigGetListPaged(query) {
    return ajaxService.get(basicConfigRouter, 'GetListPaged', query)
  }
  getBasicConfigGetList(query) {
    return ajaxService.get(basicConfigRouter, 'GetList', query)
  }
  createBasicConfig(model) {
    return ajaxService.post(basicConfigRouter, 'insert', model)
  }
  updateBasicConfig(model) {
    return ajaxService.post(basicConfigRouter, 'UpdateById/' + model.id, model)
  }
  deleteBasicConfig(model) {
    return ajaxService.delete(basicConfigRouter, 'delete', model.id)
  }
}

export default BasicConfig
