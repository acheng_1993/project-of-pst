const goalAliIcon =
    [
        {
            value: "alixinglieicon",
            name: "#icon-alixinglieicon"
        },
        {
            value: "aliwangluo",
            name: "#icon-aliwangluo"
        },
        {
            value: "aliyunyingguanli",
            name: "#icon-aliyunyingguanli"
        },
        {
            value: "alijiankong",
            name: "#icon-alijiankong"
        },
        {
            value: "alidashuju",
            name: "#icon-alidashuju"
        },
        {
            value: "alishuju",
            name: "#icon-alishuju"
        },
        {
            value: "aliGOC",
            name: "#icon-aliGOC"
        },
        {
            value: "aliyunweiguanli",
            name: "#icon-aliyunweiguanli"
        },
        {
            value: "aliyanfaxietong",
            name: "#icon-aliyanfaxietong"
        },
        {
            value: "alibianji",
            name: "#icon-alibianji"
        },
        {
            value: "alijian",
            name: "#icon-alijian"
        },
        {
            value: "alijiankong1",
            name: "#icon-alijiankong1"
        },
        {
            value: "aligengduo",
            name: "#icon-aligengduo"
        },
        {
            value: "aliguanbi",
            name: "#icon-aliguanbi"
        },
        {
            value: "alibangzhu",
            name: "#icon-alibangzhu"
        },
        {
            value: "alikaiguan",
            name: "#icon-alikaiguan"
        },
        {
            value: "alishaixuan",
            name: "#icon-alishaixuan"
        },
        {
            value: "alishanchu",
            name: "#icon-alishanchu"
        },
        {
            value: "alimingpian",
            name: "#icon-alimingpian"
        },
        {
            value: "alishouqicaidan",
            name: "#icon-alishouqicaidan"
        },
        {
            value: "alitixing",
            name: "#icon-alitixing"
        },
        {
            value: "alitianjia",
            name: "#icon-alitianjia"
        },
        {
            value: "alisousuo",
            name: "#icon-alisousuo"
        },
        {
            value: "aliyingyongzhongxin",
            name: "#icon-aliyingyongzhongxin"
        },
        {
            value: "aliyoujiantou",
            name: "#icon-aliyoujiantou"
        },
        {
            value: "aliyoujian",
            name: "#icon-aliyoujian"
        },
        {
            value: "alizhankaicaidan",
            name: "#icon-alizhankaicaidan"
        },
        {
            value: "aliyonghu-yuan",
            name: "#icon-aliyonghu-yuan"
        },
        {
            value: "aliyonghu",
            name: "#icon-aliyonghu"
        },
        {
            value: "alizuojiantou",
            name: "#icon-alizuojiantou"
        },
        {
            value: "alizhuyi",
            name: "#icon-alizhuyi"
        },
        {
            value: "alixuanzhong",
            name: "#icon-alixuanzhong"
        },
        {
            value: "alishoucang",
            name: "#icon-alishoucang"
        },
        {
            value: "alishoucangxuanzhong",
            name: "#icon-alishoucangxuanzhong"
        },
        {
            value: "alicaidan",
            name: "#icon-alicaidan"
        },
        {
            value: "aliliebiao",
            name: "#icon-aliliebiao"
        },
        {
            value: "aliwenduzengjia",
            name: "#icon-aliwenduzengjia"
        },
        {
            value: "aliwangluoxitong",
            name: "#icon-aliwangluoxitong"
        },
        {
            value: "aliwangluo1",
            name: "#icon-aliwangluo1"
        },
        {
            value: "aliwendu",
            name: "#icon-aliwendu"
        },
        {
            value: "alidianyuan",
            name: "#icon-alidianyuan"
        },
        {
            value: "aliwendujiangdi",
            name: "#icon-aliwendujiangdi"
        },
        {
            value: "alidingwei",
            name: "#icon-alidingwei"
        },
        {
            value: "aliyingpan",
            name: "#icon-aliyingpan"
        },
        {
            value: "alineicun",
            name: "#icon-alineicun"
        },
        {
            value: "aliwangluojiekou",
            name: "#icon-aliwangluojiekou"
        },
        {
            value: "aliwenjian",
            name: "#icon-aliwenjian"
        },
        {
            value: "alixitongyunzhuanqingkuang",
            name: "#icon-alixitongyunzhuanqingkuang"
        },
        {
            value: "alizhuanxianjieru",
            name: "#icon-alizhuanxianjieru"
        },
        {
            value: "aliIP",
            name: "#icon-aliIP"
        },
        {
            value: "alimokuai",
            name: "#icon-alimokuai"
        },
        {
            value: "aliyun",
            name: "#icon-aliyun"
        },
        {
            value: "aliVPN",
            name: "#icon-aliVPN"
        },
        {
            value: "alishujuku",
            name: "#icon-alishujuku"
        },
        {
            value: "alianquan",
            name: "#icon-alianquan"
        },
        {
            value: "aliUSB",
            name: "#icon-aliUSB"
        },
        {
            value: "alishijian",
            name: "#icon-alishijian"
        },
        {
            value: "alishouye",
            name: "#icon-alishouye"
        },
        {
            value: "alishuju01",
            name: "#icon-alishuju01"
        },
        {
            value: "alibaocun",
            name: "#icon-alibaocun"
        },
        {
            value: "alijiegousheji",
            name: "#icon-alijiegousheji"
        },
        {
            value: "alilishi",
            name: "#icon-alilishi"
        },
        {
            value: "alishang",
            name: "#icon-alishang"
        },
        {
            value: "alixia",
            name: "#icon-alixia"
        },
        {
            value: "alizuo",
            name: "#icon-alizuo"
        },
        {
            value: "alibiaoqian",
            name: "#icon-alibiaoqian"
        },
        {
            value: "alidaochu",
            name: "#icon-alidaochu"
        },
        {
            value: "alibiaoge",
            name: "#icon-alibiaoge"
        },
        {
            value: "alichuangjianshili",
            name: "#icon-alichuangjianshili"
        },
        {
            value: "alichakan",
            name: "#icon-alichakan"
        },
        {
            value: "alirenwu",
            name: "#icon-alirenwu"
        },
        {
            value: "alipeizhiguanli",
            name: "#icon-alipeizhiguanli"
        },
        {
            value: "aliquanxian",
            name: "#icon-aliquanxian"
        },
        {
            value: "alilishijilu",
            name: "#icon-alilishijilu"
        },
        {
            value: "alilianjie",
            name: "#icon-alilianjie"
        },
        {
            value: "alirili",
            name: "#icon-alirili"
        },
        {
            value: "alishenqingquanxian",
            name: "#icon-alishenqingquanxian"
        },
        {
            value: "alishuaxin",
            name: "#icon-alishuaxin"
        },
        {
            value: "alishujudaochu-01",
            name: "#icon-alishujudaochu-01"
        },
        {
            value: "aliweixiao",
            name: "#icon-aliweixiao"
        },
        {
            value: "alixitongzhuangtai",
            name: "#icon-alixitongzhuangtai"
        },
        {
            value: "alishuzi",
            name: "#icon-alishuzi"
        },
        {
            value: "alishujubiangeng",
            name: "#icon-alishujubiangeng"
        },
        {
            value: "alixiaofeimingxidan",
            name: "#icon-alixiaofeimingxidan"
        },
        {
            value: "alihistore-01",
            name: "#icon-alihistore-01"
        },
        {
            value: "alihuanj-01",
            name: "#icon-alihuanj-01"
        },
        {
            value: "aliloading",
            name: "#icon-aliloading"
        },
        {
            value: "aliapp",
            name: "#icon-aliapp"
        },
        {
            value: "alimysql-01",
            name: "#icon-alimysql-01"
        },
        {
            value: "alioracle-01",
            name: "#icon-alioracle-01"
        },
        {
            value: "aliSQLshenhe",
            name: "#icon-aliSQLshenhe"
        },
        {
            value: "alitext",
            name: "#icon-alitext"
        },
        {
            value: "alioceanbase-01",
            name: "#icon-alioceanbase-01"
        },
        {
            value: "alipostgresql-01",
            name: "#icon-alipostgresql-01"
        },
        {
            value: "aliwangluopinpaishu",
            name: "#icon-aliwangluopinpaishu"
        },
        {
            value: "aliaislogo",
            name: "#icon-aliaislogo"
        },
        {
            value: "aliicon_add",
            name: "#icon-aliicon_add"
        },
        {
            value: "aliicon_addmessage",
            name: "#icon-aliicon_addmessage"
        },
        {
            value: "aliicon_addresslist",
            name: "#icon-aliicon_addresslist"
        },
        {
            value: "aliicon_affiliations_li",
            name: "#icon-aliicon_affiliations_li"
        },
        {
            value: "aliicon_addperson",
            name: "#icon-aliicon_addperson"
        },
        {
            value: "aliicon_boss",
            name: "#icon-aliicon_boss"
        },
        {
            value: "aliicon_alipay_line",
            name: "#icon-aliicon_alipay_line"
        },
        {
            value: "aliicon_addressbook",
            name: "#icon-aliicon_addressbook"
        },
        {
            value: "aliicon_at",
            name: "#icon-aliicon_at"
        },
        {
            value: "aliicon_airplay",
            name: "#icon-aliicon_airplay"
        },
        {
            value: "aliicon_calendar",
            name: "#icon-aliicon_calendar"
        },
        {
            value: "aliicon_attestation",
            name: "#icon-aliicon_attestation"
        },
        {
            value: "aliicon_camera",
            name: "#icon-aliicon_camera"
        },
        {
            value: "aliicon_certificate_fil",
            name: "#icon-aliicon_certificate_fil"
        },
        {
            value: "aliicon_coinpurse_line",
            name: "#icon-aliicon_coinpurse_line"
        },
        {
            value: "aliicon_collect",
            name: "#icon-aliicon_collect"
        },
        {
            value: "aliicon_compile",
            name: "#icon-aliicon_compile"
        },
        {
            value: "aliicon_details",
            name: "#icon-aliicon_details"
        },
        {
            value: "aliicon_circle_line",
            name: "#icon-aliicon_circle_line"
        },
        {
            value: "aliicon_cloud_history",
            name: "#icon-aliicon_cloud_history"
        },
        {
            value: "aliicon_community_line",
            name: "#icon-aliicon_community_line"
        },
        {
            value: "aliicon_discovery",
            name: "#icon-aliicon_discovery"
        },
        {
            value: "aliicon_delete",
            name: "#icon-aliicon_delete"
        },
        {
            value: "aliicon_dispose",
            name: "#icon-aliicon_dispose"
        },
        {
            value: "aliicon_doc",
            name: "#icon-aliicon_doc"
        },
        {
            value: "aliicon_cspace",
            name: "#icon-aliicon_cspace"
        },
        {
            value: "aliicon_exchange",
            name: "#icon-aliicon_exchange"
        },
        {
            value: "aliicon_ding",
            name: "#icon-aliicon_ding"
        },
        {
            value: "aliicon_down",
            name: "#icon-aliicon_down"
        },
        {
            value: "aliicon_dingtalk_line",
            name: "#icon-aliicon_dingtalk_line"
        },
        {
            value: "aliicon_gift",
            name: "#icon-aliicon_gift"
        },
        {
            value: "aliicon_glass",
            name: "#icon-aliicon_glass"
        },
        {
            value: "aliicon_file",
            name: "#icon-aliicon_file"
        },
        {
            value: "aliicon_GPS",
            name: "#icon-aliicon_GPS"
        },
        {
            value: "aliicon_hardware_fill",
            name: "#icon-aliicon_hardware_fill"
        },
        {
            value: "aliicon_HRM",
            name: "#icon-aliicon_HRM"
        },
        {
            value: "aliicon_im_more",
            name: "#icon-aliicon_im_more"
        },
        {
            value: "aliicon_Eapp_line",
            name: "#icon-aliicon_Eapp_line"
        },
        {
            value: "aliicon_group",
            name: "#icon-aliicon_group"
        },
        {
            value: "aliicon_horn",
            name: "#icon-aliicon_horn"
        },
        {
            value: "aliicon_im_face",
            name: "#icon-aliicon_im_face"
        },
        {
            value: "aliicon_homepage",
            name: "#icon-aliicon_homepage"
        },
        {
            value: "aliicon_invite",
            name: "#icon-aliicon_invite"
        },
        {
            value: "aliicon_launch_page",
            name: "#icon-aliicon_launch_page"
        },
        {
            value: "aliicon_likegood",
            name: "#icon-aliicon_likegood"
        },
        {
            value: "aliicon_index_line",
            name: "#icon-aliicon_index_line"
        },
        {
            value: "aliicon_live",
            name: "#icon-aliicon_live"
        },
        {
            value: "aliicon_link",
            name: "#icon-aliicon_link"
        },
        {
            value: "aliicon_im_voice",
            name: "#icon-aliicon_im_voice"
        },
        {
            value: "aliicon_mobilephone",
            name: "#icon-aliicon_mobilephone"
        },
        {
            value: "aliicon_dmail",
            name: "#icon-aliicon_dmail"
        },
        {
            value: "aliicon_message",
            name: "#icon-aliicon_message"
        },
        {
            value: "aliicon_new_recruit",
            name: "#icon-aliicon_new_recruit"
        },
        {
            value: "aliicon_little_taget",
            name: "#icon-aliicon_little_taget"
        },
        {
            value: "aliicon_more",
            name: "#icon-aliicon_more"
        },
        {
            value: "aliicon_left",
            name: "#icon-aliicon_left"
        },
        {
            value: "aliicon_next_arrow",
            name: "#icon-aliicon_next_arrow"
        },
        {
            value: "aliicon_notice",
            name: "#icon-aliicon_notice"
        },
        {
            value: "aliicon_nomemo",
            name: "#icon-aliicon_nomemo"
        },
        {
            value: "aliicon_newgroup",
            name: "#icon-aliicon_newgroup"
        },
        {
            value: "aliicon_namecard",
            name: "#icon-aliicon_namecard"
        },
        {
            value: "aliicon_phone",
            name: "#icon-aliicon_phone"
        },
        {
            value: "aliicon_qq",
            name: "#icon-aliicon_qq"
        },
        {
            value: "aliicon_photo",
            name: "#icon-aliicon_photo"
        },
        {
            value: "aliicon_medal",
            name: "#icon-aliicon_medal"
        },
        {
            value: "aliicon_redpacket",
            name: "#icon-aliicon_redpacket"
        },
        {
            value: "aliicon_patriarch",
            name: "#icon-aliicon_patriarch"
        },
        {
            value: "aliicon_roundclose",
            name: "#icon-aliicon_roundclose"
        },
        {
            value: "aliicon_im_keyboard",
            name: "#icon-aliicon_im_keyboard"
        },
        {
            value: "aliicon_roundreduce",
            name: "#icon-aliicon_roundreduce"
        },
        {
            value: "aliicon_railway",
            name: "#icon-aliicon_railway"
        },
        {
            value: "aliicon_QRcode",
            name: "#icon-aliicon_QRcode"
        },
        {
            value: "aliicon_savememo",
            name: "#icon-aliicon_savememo"
        },
        {
            value: "aliicon_roundadd",
            name: "#icon-aliicon_roundadd"
        },
        {
            value: "aliicon_refresh",
            name: "#icon-aliicon_refresh"
        },
        {
            value: "aliicon_search",
            name: "#icon-aliicon_search"
        },
        {
            value: "aliicon_scan",
            name: "#icon-aliicon_scan"
        },
        {
            value: "aliicon_send",
            name: "#icon-aliicon_send"
        },
        {
            value: "aliicon_principal",
            name: "#icon-aliicon_principal"
        },
        {
            value: "aliicon_service",
            name: "#icon-aliicon_service"
        },
        {
            value: "aliicon_scan_namecard",
            name: "#icon-aliicon_scan_namecard"
        },
        {
            value: "aliicon_secret",
            name: "#icon-aliicon_secret"
        },
        {
            value: "aliicon_share",
            name: "#icon-aliicon_share"
        },
        {
            value: "aliicon_signin_line",
            name: "#icon-aliicon_signin_line"
        },
        {
            value: "aliicon_sms",
            name: "#icon-aliicon_sms"
        },
        {
            value: "aliicon_sketch",
            name: "#icon-aliicon_sketch"
        },
        {
            value: "aliicon_setting",
            name: "#icon-aliicon_setting"
        },
        {
            value: "aliicon_signal",
            name: "#icon-aliicon_signal"
        },
        {
            value: "aliicon_skin",
            name: "#icon-aliicon_skin"
        },
        {
            value: "aliicon_star",
            name: "#icon-aliicon_star"
        },
        {
            value: "aliicon_subordinate",
            name: "#icon-aliicon_subordinate"
        },
        {
            value: "aliicon_statistics",
            name: "#icon-aliicon_statistics"
        },
        {
            value: "aliicon_threeline_fill",
            name: "#icon-aliicon_threeline_fill"
        },
        {
            value: "aliicon_study",
            name: "#icon-aliicon_study"
        },
        {
            value: "aliicon_voice",
            name: "#icon-aliicon_voice"
        },
        {
            value: "aliicon_square",
            name: "#icon-aliicon_square"
        },
        {
            value: "aliicon_wechat",
            name: "#icon-aliicon_wechat"
        },
        {
            value: "aliicon_sport",
            name: "#icon-aliicon_sport"
        },
        {
            value: "aliicon_work",
            name: "#icon-aliicon_work"
        },
        {
            value: "aliicon_warn",
            name: "#icon-aliicon_warn"
        },
        {
            value: "aliicon_workmore",
            name: "#icon-aliicon_workmore"
        },
        {
            value: "aliicon_safety",
            name: "#icon-aliicon_safety"
        },
        {
            value: "aliicon_workset",
            name: "#icon-aliicon_workset"
        },
        {
            value: "aliicon_voipphone",
            name: "#icon-aliicon_voipphone"
        },
        {
            value: "aliicon_shield",
            name: "#icon-aliicon_shield"
        },
        {
            value: "aliicon_shakehands",
            name: "#icon-aliicon_shakehands"
        },
        {
            value: "aliicon_video",
            name: "#icon-aliicon_video"
        },
        {
            value: "aliicon_task_done",
            name: "#icon-aliicon_task_done"
        },
        {
            value: "aliicon_meeting",
            name: "#icon-aliicon_meeting"
        },
        {
            value: "aliicon_synergy",
            name: "#icon-aliicon_synergy"
        },
        {
            value: "aliicon_workfile_line",
            name: "#icon-aliicon_workfile_line"
        },
        {
            value: "aliicon_approval_fill",
            name: "#icon-aliicon_approval_fill"
        }
    ]

export default goalAliIcon
