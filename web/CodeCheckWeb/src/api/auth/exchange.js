import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'
const Base64 = require('js-base64').Base64
const exchangeRouter = 'Exchange'
const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)
class Exchange {
  /**
     * MQ交换器API
     */
  getExchangeListPaged(query) {
    return ajaxService.get(exchangeRouter, 'GetListPaged', query)
  }
  getExchangeList(query) {
    return ajaxService.get(exchangeRouter, 'GetList', query)
  }
  createExchange(model) {
    return ajaxService.post(exchangeRouter, 'insert', model)
  }

  updateExchange(model) {
    return ajaxService.post(exchangeRouter, 'UpdateById/' + model.id, model)
  }

  deleteExchange(id) {
    return ajaxService.delete(exchangeRouter, 'delete', id)
  }

  checkExchangeIsExists(params) {
    return ajaxService.get(exchangeRouter, 'IsQueueOfExchangeExist', params)
  }
}
export default Exchange
