const Eunum = {
  /**
   * 工单状态
   */
  WorkOrderStatusEnum: {
    /**
     * 创建
     */
    Create: 'Create',
    /**
     * 生产中
     */
    InProduction: 'InProduction',
    /**
     * 挂起
     */
    HangUp: 'HangUp',
    /**
     * 已完成
     */
    Done: 'Done',
    /**
     * 停止
     */
    Stop: 'Stop',
    /**
     * 清线
     */
    LineCleaning: 'LineCleaning',
    /**
     * 关闭
     */
    Closed: 'Closed'
  }
}

export default Eunum
