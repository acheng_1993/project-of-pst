import axios from 'axios'
import { MessageBox, Message, Loading } from 'element-ui'
import store from '@/store'
import {
  getToken,
  getAuthToken,
  removeToken,
  getAccountInfo
} from '@/utils/auth'

var globalLoading
var lastTipTime = new Date()
let lastMsg = ''
axios.defaults.withCredentials = false
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api 的 base_url
  withCredentials: false, // 跨域请求时发送 cookies

  timeout: 24000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    const token = getAuthToken()
    const accountInfo = getAccountInfo()
    // 带上AccountInfo
    if (accountInfo) {
      config.headers['AccountInfo'] = encodeURIComponent(accountInfo)
    }
    if (token) {
      config.headers['AuthToken'] = `Bearer ${token}`
    }
    config.headers['Content-Type'] = 'application/json;charset=UTF-8' // -patch+json

    // config.headers['Access-Control-Allow-Origin'] = '*'
    // config.headers['Access-Control-Allow-Methods'] = 'OPTIONS, GET, PUT, POST, DELETE'
    // config.headers['Access-Control-Allow-Headers'] = 'Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With'
    // config.headers['Access-Control-Allow-Credentials'] = true
    // 访问RMQ httpapi
    // if (config.url.indexOf('15672') > -1) {
    //   config.headers['Authorization'] = config.query.auth// -patch+json
    //   config.headers['Host'] = '192.168.6.45:15672'// -patch+json
    // }

    // config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    return config
  },
  error => {
    // Do something with request error
    console.log(error.message) // for debug
    Promise.reject(error)
  }
)
// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */
  /**
   * 下面的注释为通过在response里，自定义code来标示请求状态
   * 当code返回如下情况则说明权限有问题，登出并返回到登录页
   * 如想通过 XMLHttpRequest 来状态码标识 逻辑可写在下面error中
   * 以下代码均为样例，请结合自生需求加以修改，若不需要，则可删除
   */
  response => {
    const res = response.data
    if (res.isSuccess == false) {
      let message = '数据接口异常，请联系管理员'
      if (res.errorMessage && res.errorMessage != '') {
        message = res.errorMessage
      }
      const now = new Date()
      if (now.getTime() - lastTipTime.getTime() < 2000 && lastMsg == message) {
        return
      } else {
        lastTipTime = now
        lastMsg = message
      }
      if (res.httpStatusCode == 401) {
        MessageBox.confirm('登录已过期，请重新登录', '过期登录', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload() // 为了重新实例化vue-router对象 避免bug
          })
        })
      }
      Message({
        message: message,
        type: 'error',
        duration: 3 * 1000
      })

      return Promise.reject(message)
    }
    return res
  },
  error => {
    if (!error.response) {
      Message({
        message: error.message,
        type: 'error'
      })
    } else {
      if (error.response.status == '401') {
        MessageBox.confirm('授权已过期，请重新登录', '过期登录', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          removeToken()
          store.dispatch('user/resetToken').then(() => {
            location.reload() // 为了重新实例化vue-router对象 避免bug
          })
        })
      }
    }

    console.log(error)
    console.log('err' + error.message) // for debug
    return Promise.reject(error)
  }
)

export default service
