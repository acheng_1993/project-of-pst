import Cookies from 'js-cookie'
const AuthTokenKey = 'pst_bar_Auth-Token'

const TokenKey = 'pst-barAdmin-Token'
const AccountInfoKey = 'WMS_Account-Info'
export function getToken() {
  return Cookies.get(TokenKey)
}
export function getAuthToken() {
  return Cookies.get(AuthTokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function setAuthToken(token) {
  return Cookies.set(AuthTokenKey, token)
}

export function removeToken() {
  Cookies.remove(TokenKey)
  return Cookies.remove(AuthTokenKey)
}

export function getAccountInfo() {
  return Cookies.get(AccountInfoKey)
}

export function setAccountInfo(info) {
  return Cookies.set(AccountInfoKey, info)
}

export function removeAccountInfo() {
  return Cookies.remove(AccountInfoKey)
}
