
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_1按业务模块生成(泛型主键),不向前兼容 添加时间:2024/2/20 14:22:04
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System;
using CodeCheck.Service.Basic.Abstractions;
using CodeCheck.Service.Utility;
using CodeCheck.Service.Core;
using CodeCheck.Service.AppLayer;
using CodeCheck.Service.AppLayer.ViewObject;
using CodeCheck.Service.AppLayer.DTOS;
using System.IO;
using ToolKitCore.MyExcelExtension;

namespace CodeCheck.Service.AppLayer
{
    public class CodeRecordApp:ISelfScopedAutoInject
    {
        private readonly ICommandCodeRecordService commandCodeRecordService;
		private readonly IQueryCodeRecordService queryCodeRecordService;


         public CodeRecordApp(ICommandCodeRecordService commandCodeRecordService,IQueryCodeRecordService queryCodeRecordService)
        {
            this.commandCodeRecordService = commandCodeRecordService;
			this.queryCodeRecordService = queryCodeRecordService;
        }

        /// <summary>
        /// 根据主键获取单个实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<CodeRecordView> GetAsync(long id)
        {
		   
           return   await queryCodeRecordService.GetAsync<CodeRecordView>(id).ConfigureAwait(false);
		}


        /// <summary>
        /// 查询数据(分页) 返回指定实体T
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="size">每页数量</param>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<QueryPagedResponseModel<CodeRecordView>> GetListPagedAsync(int page, int size, CodeRecordConditionDTO conditionDTO=null, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseCodeRecordCondition>();
		     return await queryCodeRecordService.GetListPagedAsync<CodeRecordView>(page, size, condition, field, orderBy).ConfigureAwait(false);
        }

        /// <summary>
        /// 查询数据(不分页) 返回指定实体T
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <param name="field">返回字段</param>
        /// <param name="orderBy">排序字段</param>
        /// <returns></returns>
        public async Task<List<CodeRecordView>> GetListAsync(CodeRecordConditionDTO  conditionDTO, string field = null, string orderBy = null)
        {
		     var condition=conditionDTO.MapTo<BaseCodeRecordCondition>();
             return await queryCodeRecordService.GetListAsync<CodeRecordView>(condition, field, orderBy).ConfigureAwait(false);
	    }

        /// <summary>
        /// 根据id集合获取多条数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<CodeRecordView>> GetListByIdsAsync(List<long> ids)
        {
             return await queryCodeRecordService.GetListByIdsAsync<CodeRecordView>(ids).ConfigureAwait(false);
	    }

	    /// <summary>
        /// 插入单个实体
        /// </summary>
        /// <param name="insertDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<long>> InsertAsync(CodeRecordInsertDTO insertDto)
        { 
		    var entity=insertDto.MapTo<CodeRecordEntity>();
            var result = await commandCodeRecordService.InsertAsync(entity).ConfigureAwait(false);
            return result;
        }

		 

		   /// <summary>
        /// 批量插入实体
        /// </summary>
        /// <param name="insertDtoList">对应实体数据传输对象集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> InsertBatchAsync(List<CodeRecordInsertDTO> insertDtoList)
        {
		    var entities=insertDtoList.MapToList<CodeRecordEntity>();
            var result = await commandCodeRecordService.InsertBatchAsync(entities).ConfigureAwait(false);
            return result;
        }
       
		  
        /// <summary>
        /// 根据主键更新实体
        /// </summary>
		/// <param name="id">主键</param>
        /// <param name="updateDto">对应实体数据传输对象</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> UpdateAsync(long id,CodeRecordUpdateDTO updateDto)
        {
		    var entity=updateDto.MapTo<CodeRecordEntity>();
			entity.Id = id;
            var result = await commandCodeRecordService.UpdateAsync(entity).ConfigureAwait(false);  
            return result;
        }
	  
 
       /// <summary>
        /// 根据根据主键删除
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteAsync(long id)
        {
            var result = await commandCodeRecordService.DeleteAsync(id).ConfigureAwait(false);
            return result;
	   }


	     /// <summary>
        /// 批量删除 根据主键
        /// </summary>
        /// <param name="idList">主键集合</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<bool>> DeleteBatchAsync(IList<long> idList)
        {
            var result = await commandCodeRecordService.DeleteBatchAsync(idList).ConfigureAwait(false);
            return result;

        }

         /// <summary>
        /// 导出到excel
        /// </summary>
        /// <param name="conditionDTO">查询条件类</param>
        /// <returns></returns>
        public async Task<HttpResponseResultModel<string>> ExportToExcelAsync(CodeRecordConditionDTO  conditionDTO)
        {
            HttpResponseResultModel<string> httpResponseResultModel = new HttpResponseResultModel<string>();
          	var condition=conditionDTO.MapTo<BaseCodeRecordCondition>();
            var views= await queryCodeRecordService.GetListAsync<CodeRecordView>(condition).ConfigureAwait(false);

            if (views.IsListNullOrEmpty())
            {
                httpResponseResultModel.IsSuccess = false;
                httpResponseResultModel.ErrorMessage = "没有数据";
                return httpResponseResultModel;
            }
            var exportModel = new ExportToExcelModel<CodeRecordView>()
            {
                Data = views,
                FileName = $"条码记录_{DateTime.Now.ToString("yyyyMMddHHmmss")}.csv",
                Path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", $"exportExcel/"),
                SheetName = "Sheet1",
            };
            await ExportImportExcelHelper.ExportToCSVAsync(exportModel);
            httpResponseResultModel.IsSuccess = true;
            httpResponseResultModel.BackResult = $"exportExcel/" + exportModel.FileName;
            return httpResponseResultModel;
        }

    }
}
