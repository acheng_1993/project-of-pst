import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'RoleMenuRelation'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class RoleMenuRelation {
  getRoleMenuRelationlist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  saveRoleMenu(query) {
    return ajaxService.post(UserRouter, 'SaveRoleMenu', query)
  }
  saveRoleMenuByFunctionids(query) {
    return ajaxService.post(UserRouter, 'SaveRoleMenuByFunctionids', query)
  }
}

export default RoleMenuRelation
