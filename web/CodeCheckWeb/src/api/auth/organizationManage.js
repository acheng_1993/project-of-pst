import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const organizationManageRouter = 'Organization'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class OrganizationManage {
  getOrganizationManageTreeOnlyContainsOrgan(query) {
    return ajaxService.get(organizationManageRouter, 'GetTreeOnlyContainsOrgan', query)
  }
  getOrganTreeAsync(query) {
    return ajaxService.get(organizationManageRouter, 'GetOrganTreeAsync', query)
  }
  getOrganizationManageListPaged(query) {
    return ajaxService.get(organizationManageRouter, 'GetListPaged', query)
  }
  getOrganizationManageList(query) {
    return ajaxService.get(organizationManageRouter, 'GetList', query)
  }
  createOrganizationManage(model) {
    return ajaxService.post(organizationManageRouter, 'insert', model)
  }
  updateOrganizationManage(model) {
    return ajaxService.post(organizationManageRouter, 'UpdateById/' + model.id, model)
  }
  deleteOrganizationManage(model) {
    return ajaxService.delete(organizationManageRouter, 'delete', model)
  }
}

export default OrganizationManage
