import Vue from 'vue'
import AjaxService from '@/utils/ajaxService'
const Base64 = require('js-base64').Base64
const queueRouter = 'Queue'
const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

/**
 * 队列信息配置
 */
class Queue {
  getQueuecConfigListPaged(query) {
    return ajaxService.get(queueRouter, 'GetListPaged', query)
  }
  getQueuecConfigList(query) {
    return ajaxService.get(queueRouter, 'GetList', query)
  }

  createQueueConfig(model) {
    return ajaxService.post(queueRouter, 'AddNewQueue', model)
  }

  updateQueueConfig(model) {
    return ajaxService.post(queueRouter, 'UpdateById/' + model.id, model)
  }

  deleteQueueConfig(id) {
    return ajaxService.delete(queueRouter, 'delete', id)
  }

  declareQueue(model) {
    return ajaxService.post(queueRouter, 'DeclareQueueOnServer', model)
  }

  GetQueueMessageCount(queueName) {
    return ajaxService.get(queueRouter, 'GetQueueMessageCount', queueName)
  }
}
export default Queue
