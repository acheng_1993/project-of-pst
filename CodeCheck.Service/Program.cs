using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CodeCheck.Service.AppLayer;
using CodeCheck.Service.Core;
using CodeCheck.Service.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CodeCheck.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();
            if (IsExistProcess())
            {
                Console.WriteLine("系统提示:对不起,该系统不允许同时运行多个实例!");
                return;
            }
            InitConifg();
            //多线程异常
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            var services = new ServiceCollection();
            //添加服务注册
            RegisterService.RegisterComponents(services);
            var serviceProvider = services.BuildServiceProvider();
            AppsettingsConfig.ServiceProvider = serviceProvider;
            #region 启动一个REST SERVER
            try
            {
               
                CreateHostBuilder(serviceProvider).Build().RunAsync();
                CodeDcService.StartRecordCodeAsync();
                CodeDcService.InitAlertDevice();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"打开REST SERVER失败:{ex.ToString()}");
            }
            #endregion
            Console.WriteLine("系统提示:系统运行中。。。");
            Console.ReadKey();
        }
        static bool canCreateNew; //用来承接是否已经有一个实例在运行的布尔变量
        //限制单例运行
        static Mutex m = new Mutex(true, "CodeCheck.Service", out canCreateNew);

        /// <summary>
        /// 判断当前实例是否已经运行
        /// </summary>
        /// <returns></returns>
        static bool IsExistProcess()
        {
            if (canCreateNew)
            {

                return false;
            }
            else
            {
                //
                return true;
            }
        }

        #region 多线程异常
        /// <summary>
        /// 多线程异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {

            string str = "";
            //父方法
            System.Diagnostics.StackTrace ss = new System.Diagnostics.StackTrace(true);
            System.Reflection.MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得父方法命名空间    
            str += mb.DeclaringType.Namespace + ".";

            //取得父方法类名    
            str += mb.DeclaringType.Name + ".";

            //取得父方法类全名    
            str += mb.DeclaringType.FullName + ".";

            //取得父方法名    
            str += mb.Name;


            Exception ex = e.ExceptionObject as Exception;

            string exMessage = Newtonsoft.Json.JsonConvert.SerializeObject(ex, Newtonsoft.Json.Formatting.Indented);

            //记录异常日志
            WriteLogHelper.WriteLogsAsync("子线程异常[" + str + "]\r\n" + ex.Message + "\r\n INFO:" + exMessage + "\r\n\r\n", "UnhandledException");

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(IServiceProvider serviceProvider)
        {
            var response = Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    //  webBuilder.UseUrls($"http://*:{AppsettingsConfig.l}");
                });
            return response;
        }

        private static void InitConifg()
        {
            var configuration = new ConfigurationBuilder()
                     .SetBasePath(Directory.GetCurrentDirectory())
                     .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                     .Build();
            AppsettingsConfig config = new AppsettingsConfig();
            configuration.Bind("WebConfig", config);
        }
    }
}
