﻿using CodeCheck.Service.AppLayer.DTOS;
using CodeCheck.Service.Core;
using CodeCheck.Service.IDbService;
using CodeCheck.Service.Utility;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CodeCheck.Service.AppLayer
{
    /// <summary>
    /// 条码收集服务
    /// </summary>
    public class CodeDcService
    {
        /// <summary>
        /// 开始记录条码
        /// </summary>
        /// <returns></returns>
        public async static Task StartRecordCodeAsync() {

            Task.Run(async () =>
            {
                await Task.Delay(1000);
                RecordCodeAsync();
            });



        }


        private async static Task RecordCodeAsync() {
         
            while (true)
            {
                var result = new List<string>();
                try
                {
                    var now = DateTime.Now;
                    //按年月日扫描文件包含yymmdd_Result关键字文件          
                    //读取本天内的条码，不能存在重复
                    //当日文件是否存在重复
                    //当日条码与之前的条码是否存在重复
                    //可查询 条码获取时间以及校验结果
                    string path = Path.Combine( $"{AppsettingsConfig.ReadFilePath}",$"{now.Year}", now.ToString("MM"));
                
                    string lineKeywords = "読み取り捺印データ";
                    var fileKeywords = $"*{now.ToString("yyMMdd")}_Result.log";
                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    var sjis = System.Text.Encoding.GetEncoding("shift_jis");
                    // 遍历指定文件夹下的所有.txt文件
                    foreach (string filePath in Directory.EnumerateFiles(path, fileKeywords, SearchOption.AllDirectories))
                    {
                        // 读取文件内容
                        using (StreamReader reader = new StreamReader(filePath, sjis))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                // 检查当前行是否包含关键字
                                if (line.Contains(lineKeywords, StringComparison.OrdinalIgnoreCase))
                                {
                                    var lineArr = line.Split(',');
                                    if (lineArr.Length > 1)
                                    {
                                        var code = line.Split(',')[1];
                                        if (code.Length > 10)
                                            result.Add(code.Substring(0, 10));
                                        else {
                                            result.Add(code);
                                        }
                                    }
                                }
                            }
                        }

                    }

                    // 查询非当天的条码是否出现在了文件中
                    // 通知PLC 以及串口报警灯报警
                    var sugarService =  AppsettingsConfig.ServiceProvider.GetService<ISugar_CodeRecordService>();
                    var result2 = result.Distinct();
                    bool isRepeat = false;// 是否重复
                    var currentDayExpre = PackageExpression(new CodeRecordConditionDTO()
                    {
                        CodeList = result,
                        CodeTimeStart = now.Date,
                        CodeTimeEnd = now,
                    });
                    List<Sugar_CodeRecordEntity> codeList = new List<Sugar_CodeRecordEntity>();
                    if (result.Count > 0)
                    {
                        string repeatCode = "";
                        var currentDayCodes = await sugarService.ListGetAsync(currentDayExpre);
                        if (result.Count != result2.Count())
                        {
                            var lastCode = result[result.Count - 1];
                            var lastCount = result.Count(x => x == lastCode);
                            if (lastCount > 1)
                            {
                                isRepeat = true;
                                repeatCode = lastCode;
                            }
                        }
                        bool isNew = false;
                        if (isRepeat == false)
                        {
                            // 看看其他内容
                            var expre = PackageExpression(new CodeRecordConditionDTO()
                            {
                                CodeList = result,
                                CodeTimeStart = now.AddYears(-10),
                                CodeTimeEnd = now.Date.AddDays(-1),
                            });
                            var repeatFirst = await sugarService.GetFirstAsync(expre);
                            if (repeatFirst != null&&repeatFirst.Code== result[result.Count-1] && result.Count > currentDayCodes.Count)
                            {
                                isRepeat = true;
                                repeatCode = repeatFirst.Code;
                            }
                        }
                    
                            for (var i = 0; i < result.Count; i++)
                            {
                            if (result.Count > currentDayCodes.Count+ codeList.Count)
                            {
                                int status = repeatCode == result[i] ? 2 : 1;
                                if (currentDayCodes.Exists(x=>x.Code==result[i]) == false|| status==2) {
                                    codeList.Add(new Sugar_CodeRecordEntity()
                                    {
                                        Code = result[i],
                                        Status = status,
                                        CodeTime = DateTime.Now,
                                    });
                                    isNew = true;
                                }
                            }
                        }
                        if (isNew)
                        {
                            if (isRepeat)
                            {
                                // 通知 相关设备与PLC
                                RedAlert();

                            }
                            else
                            {
                                GreenAlert();
                            }
                            try
                            {
                                if (string.IsNullOrEmpty(AppsettingsConfig.WarnTagName) == false)
                                {
                                    var requestToHttpHelper = AppsettingsConfig.ServiceProvider.GetService<Utility.RequestToHttpHelper>();
                                    var req = new Utility.HttpRequestModel()
                                    {
                                        Data = new { tag = new { key = AppsettingsConfig.WarnTagName, value = isRepeat ? "2" : "1" } },
                                        Host = AppsettingsConfig.DcsApi,
                                        Path = "/Tag/WriteTag",
                                    };
                                    // 更新服务对应的任务状态
                                    var httpRsp = await requestToHttpHelper.PostAsync<HttpResponseResultModel<bool>>(req);
                                    if (httpRsp.IsSuccess == false || httpRsp.BackResult.IsSuccess == false)
                                    {
                                        WriteLogHelper.WriteLogsAsync($"{AppsettingsConfig.DcsApi},{req.Path},通知PLC失败", "通知PLC失败");
                                    }
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                   
                    if(codeList.IsListNullOrEmpty()==false)
                    sugarService.InsertRange(codeList);

                }
                catch (Exception ex) {
                    WriteLogHelper.WriteLogsAsync("读取条码异常",ex.ToString());
                }
                await Task.Delay(AppsettingsConfig.ReadInterval);
            }
        }


        static Expression<Func<Sugar_CodeRecordEntity, bool>> PackageExpression(CodeRecordConditionDTO conditionDTO)
        {
            // 拼接条件
            Expression<Func<Sugar_CodeRecordEntity, bool>> expression = t => t.IsDeleted == false;
        
            if (conditionDTO.Status.HasValue&& conditionDTO.Status.Value>0)
            {
                expression = expression.And(x => x.Status == conditionDTO.Status.Value);
            }
            if (conditionDTO.CodeTimeStart.HasValue )
            {
                expression = expression.And(x => x.CodeTime >= conditionDTO.CodeTimeStart.Value);
            }
            if (conditionDTO.CodeTimeEnd.HasValue)
            {
                expression = expression.And(x => x.CodeTime < conditionDTO.CodeTimeEnd.Value);
            }
            if (conditionDTO.CodeList.IsListNullOrEmpty()==false)
            {
                expression = expression.And(x => conditionDTO.CodeList.Contains( x.Code) );
            }
            return expression;
        }
        static SerialPortHelper serialPortHelper;
        /// <summary>
        /// 初始化报警
        /// </summary>
        public static void InitAlertDevice() {
            Task.Run(() =>
            {
                serialPortHelper = new SerialPortHelper(AppsettingsConfig.SerialPortConfig.ComPort,
                   AppsettingsConfig.SerialPortConfig.Baudrate, (SerialParity)AppsettingsConfig.SerialPortConfig.Parity,
                    AppsettingsConfig.SerialPortConfig.DataBits, (StopBit)AppsettingsConfig.SerialPortConfig.StopBit);
                serialPortHelper.Connect();
            });

        }

        /// <summary>
        /// 绿灯
        /// </summary>
        /// <returns></returns>
        public static bool GreenAlert()
        {
            bool isTrue = false;
            isTrue = serialPortHelper.SendHex("01 05 00 03 FF 00 7C 3A");

            Task.Run(async () => {
                await Task.Delay(1500);
                serialPortHelper.SendHex("01 05 00 03 00 00 3D CA");// 关灯
            });
            return isTrue;
        }

        /// <summary>
        /// 红色报警
        /// </summary>
        /// <returns></returns>
        public static bool RedAlert() { 
         bool isTrue = false;
            isTrue = serialPortHelper.SendHex("01 05 00 0A FF 00 AC 38");

            return isTrue;
        }

        /// <summary>
        /// 关闭报警
        /// </summary>
        /// <returns></returns>
        public static bool CloseAll()
        {
            bool isTrue = false;

            isTrue = serialPortHelper.SendHex("01 05 00 00 00 00 CD CA");// 全关

            Task.Run(async () => {
                await Task.Delay(1000);
                isTrue = serialPortHelper.SendHex("01 05 00 04 00 00 8C 0B");// 关蜂鸣器
            });
       

            return isTrue;
        }
    }
}
