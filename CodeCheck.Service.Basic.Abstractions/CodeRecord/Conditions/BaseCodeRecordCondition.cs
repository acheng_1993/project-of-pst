

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_1按业务模块生成(泛型主键),不向前兼容 添加时间:2024/2/20 14:22:04
using System;
using System.Collections.Generic;
namespace CodeCheck.Service.Basic.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseCodeRecordCondition
    {
            /// <summary>
        ///  条码
        /// </summary>
        public string Code {get;set;}
        
           /// <summary>
        ///  条码时间起始时间
        /// </summary>
        public DateTime? CodeTimeStart {get;set;}
        
           /// <summary>
        ///  条码时间结束时间
        /// </summary>
        public DateTime? CodeTimeEnd {get;set;}
        
           /// <summary>
        ///  状态1：OK，2：重复
        /// </summary>
        public int? Status {get;set;}
        
           /// <summary>
        ///  预留1
        /// </summary>
        public string Attribute1 {get;set;}
        
           /// <summary>
        ///  预留2
        /// </summary>
        public string Attribute2 {get;set;}
        
           /// <summary>
        ///  预留3
        /// </summary>
        public string Attribute3 {get;set;}
        
           /// <summary>
        ///  创建时间起始时间
        /// </summary>
        public DateTime? CreateTimeStart {get;set;}
        
           /// <summary>
        ///  创建时间结束时间
        /// </summary>
        public DateTime? CreateTimeEnd {get;set;}
        
      

    }
}
