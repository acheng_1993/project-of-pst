﻿using Microsoft.Extensions.DependencyInjection;
using Scrutor;
using System.Reflection;
using System.Collections.Generic;
using CodeCheck.Service.Core;
using System.Linq;
using CodeCheck.Service.AppLayer;
using CodeCheck.Service.Utility;
using CodeCheck.Service.MediatR;
using Microsoft.AspNetCore.Http;
using CodeCheck.Service.Repository;
using CodeCheck.Service.Basic;
using Polly;
using System.Net.Http;
using System;
using CodeCheck.Service.DbService;

namespace CodeCheck.Service
{
    public static class RegisterService
    {
        public static void RegisterComponents(IServiceCollection services)
        {
            RegisterHttpClientWithPolly(services);
            //services.AddHttpClient();
            services.AddScoped<RequestToHttpHelper>();
            services.AddSingleton<TPLLogger>();
           // services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();//单例
            RegisterBasic.RegisterComponents(services);
            RegisterApp.RegisterComponents(services);
            RegisterRepository.RegisterComponents(services);
            RegisterMediatR.RegisterComponents(services);
            DbRegisterService.Register(services);
            //自动注入IAutoInject
            services.Scan(x =>
            {
                var entryAssembly = Assembly.GetEntryAssembly();
                var referencedAssemblies = entryAssembly.GetReferencedAssemblies().Select(Assembly.Load);
                var assemblies = new List<Assembly> { entryAssembly }.Concat(referencedAssemblies);

                x.FromAssemblies(assemblies)
                    .AddClasses(classes => classes.AssignableTo(typeof(IAutoInject)))
                        .AsImplementedInterfaces()
                        .WithScopedLifetime()
                        //接口注册Scoped
                    .AddClasses(classes => classes.AssignableTo(typeof(IScopedAutoInject)))
                        .AsImplementedInterfaces()
                        .WithScopedLifetime()
                    //接口注册Singleton
                    .AddClasses(classes => classes.AssignableTo(typeof(ISingletonAutoInject)))
                          .AsImplementedInterfaces()
                          .WithSingletonLifetime()
                   //接口注册Transient
                    .AddClasses(classes => classes.AssignableTo(typeof(ITransientAutoInject)))
                          .AsImplementedInterfaces()
                          .WithTransientLifetime()
                    //具体类注册Scoped
                    .AddClasses(classes => classes.AssignableTo(typeof(ISelfScopedAutoInject)))
                          .AsSelf()
                          .WithScopedLifetime()
                    //具体类注册Singleton
                    .AddClasses(classes => classes.AssignableTo(typeof(ISelfSingletonAutoInject)))
                          .AsSelf()
                          .WithSingletonLifetime()
                    //具体类注册Transient
                    .AddClasses(classes => classes.AssignableTo(typeof(ISelfTransientAutoInject)))
                          .AsSelf()
                          .WithTransientLifetime();
            });
        }
        /// <summary>
        ///  http 请求注入polly，失败了，自动重试
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterHttpClientWithPolly(IServiceCollection services)
        {
            // 相关文档地址
            //https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/http-requests?view=aspnetcore-5.0

            int retryTimes = GlobalConstant.HttpRequestRetryTimes;//重试2次
            int index = 1;
            int retryIntervalMilliseconds = GlobalConstant.PollyRetryIntervalMilliseconds;//重试间隔
            services.AddHttpClient(GlobalConstant.HttpClientName)
               .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(retryTimes, retryAttempt => TimeSpan.FromMilliseconds(retryIntervalMilliseconds), (response, timeSpan) =>
               {
                   WriteLogHelper.WriteLogsAsync($"{timeSpan.ToString()}重试第{index}次：" + response.Exception?.ToString(), "Error");
                   index++;
                   if (index > 2)
                   {
                       index = 1;
                   }
               }));
        }
    }
}