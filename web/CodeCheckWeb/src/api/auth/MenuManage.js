import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'Menu'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class MenuManage {
  getMenuTree(query) {
    return ajaxService.get(UserRouter, 'GetMenuTree', query)
  }
  updateMenu(query) {
    return ajaxService.post(UserRouter, 'UpdateById/' + query.id, query)
  }
  deleteMenu(query) {
    // ajaxService.delete  方法有问题
    return ajaxService.delete(UserRouter, 'delete', query)
  }
  insertMenu(query) {
    return ajaxService.post(UserRouter, 'insert', query)
  }
  getMenulist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  getMenuTreeInvolvingDefaultRoleMenu(query) {
    return ajaxService.get(
      UserRouter,
      'GetMenuTreeInvolvingDefaultRoleMenu',
      query
    )
  }
  getMenuTreeInvolvingDefaultUserMenu(query) {
    return ajaxService.get(
      UserRouter,
      'GetMenuTreeInvolvingDefaultUserMenu',
      query
    )
  }
  getMenuTreeInvolvingDefaultUserScreenMenu(query) {
    return ajaxService.get(
      UserRouter,
      'GetMenuTreeInvolvingDefaultUserScreenMenu',
      query
    )
  }
  saveMenu(query) {
    return ajaxService.post(UserRouter, 'SaveMenu', query)
  }
}

export default MenuManage
