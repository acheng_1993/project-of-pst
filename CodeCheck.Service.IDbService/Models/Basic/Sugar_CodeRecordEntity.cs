
//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_2模块组件(泛型主键) 添加时间:2024/2/20 14:27:58
using SqlSugar;
using CodeCheck.Service.Core;
namespace CodeCheck.Service.IDbService
{
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("CodeRecord")]
    public class Sugar_CodeRecordEntity: BaseField
    {

       public  Sugar_CodeRecordEntity()
       {
                    Id = GeneratePrimaryKeyIdHelper.GetPrimaryKeyId();
                         }
         [SugarColumn(ColumnName = "Id", IsPrimaryKey = true)]
       public long Id{get;set;}
            /// <summary>
        ///  条码
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string Code {get;set;}
        
          /// <summary>
        ///  条码时间
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public DateTime? CodeTime {get;set;}
        
          /// <summary>
        ///  状态1：OK，2：重复
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public int? Status {get;set;}
        
          /// <summary>
        ///  预留1
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string Attribute1 {get;set;}
        
          /// <summary>
        ///  预留2
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string Attribute2 {get;set;}
        
          /// <summary>
        ///  预留3
        /// </summary>
                   [SugarColumn(IsNullable = true)]
        public string Attribute3 {get;set;}
        
     

    }

   
}
