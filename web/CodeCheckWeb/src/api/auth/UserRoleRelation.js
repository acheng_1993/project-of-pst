import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const UserRouter = 'UserRoleRelation'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class UserRoleRelation {
  saveUserRole(query) {
    return ajaxService.post(UserRouter, 'SaveUserRole', query)
  }
  getUserRoleRelationPagelist(query) {
    return ajaxService.get(UserRouter, 'GetListPaged', query)
  }
  getUserRoleRelationlist(query) {
    return ajaxService.get(UserRouter, 'GetList', query)
  }
  saveUserRoleByRoleIds(query) {
    return ajaxService.post(UserRouter, 'SaveUserRoleByRoleIds', query)
  }
  getUserListByids(query) {
    return ajaxService.post(UserRouter, 'GetUserListByids', query)
  }
}

export default UserRoleRelation
