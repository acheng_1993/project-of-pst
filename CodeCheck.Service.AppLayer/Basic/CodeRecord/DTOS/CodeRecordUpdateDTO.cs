

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_1按业务模块生成(泛型主键),不向前兼容 添加时间:2024/2/20 14:22:04
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CodeCheck.Service.AppLayer.DTOS
{
    /// <summary>
    /// 
    /// </summary>
    public class CodeRecordUpdateDTO
    {
            /// <summary>
        ///  主键
        /// </summary>
        [Description("主键")]
                 public long Id {get;set;}
        
          /// <summary>
        ///  条码
        /// </summary>
        [Description("条码")]
                 public string Code {get;set;}
        
          /// <summary>
        ///  条码时间
        /// </summary>
        [Description("条码时间")]
                 public DateTime? CodeTime {get;set;}
        
          /// <summary>
        ///  状态1：OK，2：重复
        /// </summary>
        [Description("状态1：OK，2：重复")]
                 public int? Status {get;set;}
        
          /// <summary>
        ///  预留1
        /// </summary>
        [Description("预留1")]
                 public string Attribute1 {get;set;}
        
          /// <summary>
        ///  预留2
        /// </summary>
        [Description("预留2")]
                 public string Attribute2 {get;set;}
        
          /// <summary>
        ///  预留3
        /// </summary>
        [Description("预留3")]
                 public string Attribute3 {get;set;}
        
     

    }
}
