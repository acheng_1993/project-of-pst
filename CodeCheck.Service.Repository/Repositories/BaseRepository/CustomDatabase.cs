﻿using NPoco;
using System;
using System.Data.Common;
using System.Data.SqlClient;
using CodeCheck.Service.Utility;
using CodeCheck.Service.Core;

namespace CodeCheck.Service.Repository.Repositories
{
    internal class CustomDatabase : Database
    {
        public CustomDatabase(string connection, DatabaseType databaseType, DbProviderFactory dbProviderFactory) : base(connection, databaseType, dbProviderFactory)
        {


        }
        protected override bool OnUpdating(UpdateContext updateContext)
        {
            var entity = updateContext.Poco as BaseField;
            if (entity != null)
            {
                var userInfo = CustomHeaderHelper.GetCustomHeader();
                entity.ModifyTime = DateTime.Now;
                entity.ModifierId = userInfo.Id;
                entity.ModifierName = userInfo.RealName;
                entity.CreateTime = null;
                entity.CreaterId = null;
                entity.CreaterName = null;
            }
            return base.OnUpdating(updateContext);
        }

        protected override bool OnInserting(InsertContext insertContext)
        {
            var entity = insertContext.Poco as BaseField;
            if (entity != null)
            {
                var userInfo = CustomHeaderHelper.GetCustomHeader();
                entity.CreateTime = DateTime.Now;
                entity.CreaterId = userInfo.Id;
                entity.ModifierName = userInfo.RealName;
                entity.ModifyTime = DateTime.Now;
                entity.ModifierId = userInfo.Id;
                entity.CreaterName = userInfo.RealName;
            }
            return base.OnInserting(insertContext);
        }


    }
}
