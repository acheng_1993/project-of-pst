﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SqlSugar;
using CodeCheck.Service.Core;
using CodeCheck.Service.Utility;
using CodeCheck.Service.IDbService;

namespace CodeCheck.Service.DbService
{
    /// <summary>
    /// 抽象基类实现
    /// </summary>
    public abstract class AbstractBaseService<T> : DbContext, IBaseService<T> where T : class, new()
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public AbstractBaseService()
        {
        }

        #region Insert
        /// <summary>
        /// 插入(NUll不插入值)，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool InsertSplitTable(T model)
        {
            return Db.Insertable<T>(model).SplitTable().ExecuteCommand() > 0;
        }


        /// <summary>
        /// 插入(NUll不插入值)，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool InsertRangeSplitTable(IEnumerable<T> models)
        {
            return Db.Insertable<T>(models).SplitTable().ExecuteCommand() > 0;
        }
        public virtual bool DeleteSplitTable(T model)
        {
            //var tableName = Db.SplitHelper<SplitTestTable>().GetTableName(data.CreateTime);//根据时间获取表名
            return Db.Deleteable<T>(model).SplitTable().ExecuteCommand() > 0;//,SplitTable不能少
        }

        public virtual bool DeleteRangeSplitTable(IEnumerable<T> models)
        {
            //var tableName = Db.SplitHelper<SplitTestTable>().GetTableName(data.CreateTime);//根据时间获取表名
            return Db.Deleteable<T>(models).SplitTable().ExecuteCommand()>0;//,SplitTable不能少
        }


        /// <summary>
        /// 插入(NUll不插入值)，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Insert(T model)
        {
            return GetDbSet<T>().InsertWithOutNull(model);
        }

        /// <summary>
        /// 插入(NUll不插入值)，返回是否成功
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Insert(string tableName, T model)
        {
            return GetDbSet<T>().InsertWithOutNull(tableName, model);
        }

        /// <summary>
        /// 同步插入，返回雪花Id
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual long InsertSnowflakeId(T t)
        {
            return GetDbSet<T>().InsertReturnSnowflakeId(t);
        }

        /// <summary>
        /// 插入，返回自增id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual List<long> InsertSnowflakeId(List<T> model)
        {
            return GetDbSet<T>().InsertReturnSnowflakeId(model);
        }
        
        /// <summary>
        /// 异步插入，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<bool> InsertAsync(T model)
        {
            return await GetDbSet<T>().InsertAsync(model);
        }

        /// <summary>
        /// 异步插入，返回雪花Id
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual async Task<long> InsertSnowflakeIdAsync(T t)
        {
            return await GetDbSet<T>().InsertReturnSnowflakeIdAsync(t);
        }
        
        /// <summary>
        /// 异步插入，返回自增id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<List<long>> InsertSnowflakeIdAsync(List<T> model)
        {
            return await GetDbSet<T>().InsertReturnSnowflakeIdAsync(model);
        }

        /// <summary>
        /// 异步批量插入，返回是否成功
        /// </summary>
        /// <param name="dbModels">主键集合</param>
        /// <returns></returns>
        public virtual bool InsertRange(T[] dbModels)
        {
            return GetDbSet<T>().InsertRangeWithOutNull(dbModels);
        }

        /// <summary>
        /// 异步批量插入，返回是否成功
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dbModels">主键集合</param>
        /// <returns></returns>
        public virtual bool InsertRange(string tableName, T[] dbModels)
        {
            return GetDbSet<T>().InsertRangeWithOutNull(tableName, dbModels);
        }

        /// <summary>
        /// 异步批量插入，返回是否成功
        /// </summary>
        /// <param name="dbModels">Db模型集合</param>
        /// <returns></returns>
        public virtual async Task<bool> InsertRangeAsync(T[] dbModels)
        {
            return await GetDbSet<T>().InsertAsync(dbModels);
        }

        /// <summary>
        /// 异步批量插入，返回是否成功
        /// </summary>
        /// <param name="lisT">Db模型集合</param>
        /// <returns></returns>
        public virtual bool InsertRange(List<T> lisT)
        {
            return GetDbSet<T>().InsertRangeWithOutNull(lisT);
        }

        /// <summary>
        /// 异步批量插入，返回是否成功
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="lisT">Db模型集合</param>
        /// <returns></returns>
        public virtual bool InsertRange(string tableName, List<T> lisT)
        {
            return GetDbSet<T>().InsertRangeWithOutNull(tableName, lisT);
        }

        /// <summary>
        /// 异步批量插入，返回是否成功
        /// </summary>
        /// <param name="lisT">Db模型集合</param>
        /// <returns></returns>
        public virtual async Task<bool> InsertRangeAsync(List<T> lisT)
        {
            return await GetDbSet<T>().InsertAsync(lisT);
        }

        /// <summary>
        /// 插入，返回自增id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int InsertReturnIdentity(T model)
        {
            return GetDbSet<T>().InsertWithOutNullReturnIdentity(model);
        }

        /// <summary>
        /// 插入，返回自增id
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int InsertReturnIdentity(string tableName, T model)
        {
            return GetDbSet<T>().InsertWithOutNullReturnIdentity(tableName, model);
        }

        /// <summary>
        /// 异步插入，返回自增id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<int> InsertReturnIdentityAsync(T model)
        {
            return await GetDbSet<T>().InsertReturnIdentityAsync(model);
        }

        /// <summary>
        /// 插入，返回model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual T InsertReturnEntity(T model)
        {
            return GetDbSet<T>().InsertReturnEntity(model);
        }

        /// <summary>
        /// 插入，返回model
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual T InsertReturnEntity(string tableName, T model)
        {
            return GetDbSet<T>().InsertReturnEntity(tableName, model);
        }

        /// <summary>
        /// 异步插入，返回model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<T> InsertReturnEntityAsync(T model)
        {
            return await GetDbSet<T>().InsertReturnEntityAsync(model);
        }

        #endregion

        #region Delete

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Delete(T model)
        {
            return GetDbSet<T>().Delete(model);
        }

        /// <summary>
        /// 异步删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(T model)
        {
            return await GetDbSet<T>().DeleteAsync(model);
        }

        /// <summary>
        /// 异步批量删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteRangeAsync(List<T> model)
        {
            return await GetDbSet<T>().DeleteAsync(model);
        }

        /// <summary>
        /// 根据where条件删除
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual bool Delete(Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().Delete(whereExpression);
        }


        /// <summary>
        /// 根据where条件删除
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().DeleteAsync(whereExpression);
        }

        /// <summary>
        /// 根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual bool DeleteById(dynamic id)
        {
            return GetDbSet<T>().DeleteById(id);
        }

        /// <summary>
        /// 异步根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteByIdAsync(dynamic id)
        {
            return await GetDbSet<T>().DeleteByIdAsync(id);
        }

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual bool DeleteByIds(dynamic[] listId)
        {
            return GetDbSet<T>().DeleteByIds(listId);
        }

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual bool DeleteByIds(string tableName, dynamic[] listId)
        {
            return GetDbSet<T>().DeleteByIds(tableName, listId.ToList());
        }

        /// <summary>
        /// 异步根据主键批量删除
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteByIdsAsync(dynamic[] listId)
        {
            return await GetDbSet<T>().DeleteByIdsAsync(listId);
        }

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual bool DeleteByIds(List<dynamic> listId)
        {
            return GetDbSet<T>().DeleteByIds(listId);
        }

        /// <summary>
        /// 根据主键批量删除
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual bool DeleteByIds(string tableName, List<dynamic> listId)
        {
            return GetDbSet<T>().DeleteByIds(tableName, listId);
        }

        /// <summary>
        /// 异步根据主键批量删除
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteByIdsAsync(List<dynamic> listId)
        {
            return await GetDbSet<T>().DeleteByIdsAsync(listId);
        }

        #endregion

        #region Get

        /// <summary>
        /// 查询全部数据，不建议使用
        /// </summary>
        /// <returns></returns>
        public virtual List<T> ListGet()
        {
            return GetDbSet<T>().GetList();
        }

        /// <summary>
        /// 根据where条件查询全部数据
        /// </summary>
        /// <returns></returns>
        public virtual List<T> ListGet(Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().GetList(whereExpression);
        }

        /// <summary>
        /// 根据where条件查询全部数据排序
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="orderByType"></param>
        /// <returns></returns>
        public virtual List<T> ListGet(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderByExpression, OrderByType orderByType)
        {
            return GetDbSet<T>().ListGet(whereExpression, orderByExpression, orderByType);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="whereExpression">where条件</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <returns></returns>
        public virtual List<T> ListPageGet(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize,
            ref int totalCount)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(whereExpression, pageModel);

            totalCount = pageModel.TotalCount;
            return result;
        }

        /// <summary>
        /// 升序分页查询
        /// </summary>
        /// <param name="whereExpression">where条件</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序</param>
        /// <returns></returns>
        public virtual List<T> ListPageGetOrderByAsc(Expression<Func<T, bool>> whereExpression, int intPageIndex,
            int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(whereExpression, pageModel,
                orderByExpression);

            totalCount = pageModel.TotalCount;
            return result;
        }

        /// <summary>
        /// 降序分页查询
        /// </summary>
        /// <param name="whereExpression">where条件</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序</param>
        /// <returns></returns>
        public virtual List<T> ListPageGetOrderByDesc(Expression<Func<T, bool>> whereExpression, int intPageIndex,
            int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(whereExpression, pageModel,
                orderByExpression, OrderByType.Desc);

            totalCount = pageModel.TotalCount;
            return result;
        }


        /// <summary>
        /// 多条件分页查询
        /// </summary>
        /// <param name="conditionalList">where条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <returns></returns>
        public virtual List<T> ListPageGet(List<IConditionalModel> conditionalList, int intPageIndex, int intPageSize,
            ref int totalCount)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(conditionalList, pageModel);

            totalCount = pageModel.TotalCount;
            return result;
        }

        /// <summary>
        /// 多条件升序分页查询
        /// </summary>
        /// <param name="conditionalList">where条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序</param>
        /// <returns></returns>
        public virtual List<T> ListPageGetOrderByAsc(List<IConditionalModel> conditionalList, int intPageIndex,
            int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(conditionalList, pageModel,
                orderByExpression);

            totalCount = pageModel.TotalCount;
            return result;
        }

        /// <summary>
        /// 多条件降序分页查询
        /// </summary>
        /// <param name="conditionalList">where条件集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByExpression">排序</param>
        /// <returns></returns>
        public virtual List<T> ListPageGetOrderByDesc(List<IConditionalModel> conditionalList, int intPageIndex,
            int intPageSize, ref int totalCount,
            Expression<Func<T, object>> orderByExpression)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(conditionalList, pageModel,
                orderByExpression, OrderByType.Desc);

            totalCount = pageModel.TotalCount;
            return result;
        }

        /// <summary>
        /// 异步查询全部数据，不建议使用
        /// </summary>
        /// <returns></returns>
        public virtual async Task<List<T>> ListGetAsync()
        {
            return await GetDbSet<T>().ListGetAsync();
        }

        /// <summary>
        /// 异步根据where条件查询全部数据
        /// </summary>
        /// <returns></returns>
        public virtual async Task<List<T>> ListGetAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().ListGetAsync(whereExpression);
        }

        /// <summary>
        /// 异步根据where条件查询全部数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> ListGetAsync(string strWhere)
        {
            return await GetDbSet<T>().ListGetAsync(strWhere);
        }

        /// <summary>
        /// 根据主键查询
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public virtual T GetById(dynamic objId)
        {
            return GetDbSet<T>().GetById(objId);
        }

        /// <summary>
        /// 根据主键查询，可从缓存中查，不建议使用
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="blnUseCache"></param>
        /// <returns></returns>
        public virtual T GetById(dynamic objId, bool blnUseCache)
        {
            return GetDbSet<T>().GetById(objId, blnUseCache);
        }

        /// <summary>
        /// 根据where条件查询单条，超过一条会报错!!!
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual T GetSingle(Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().GetSingle(whereExpression);
        }

        /// <summary>
        /// 根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="orderExpression">orderBy表达式</param>
        /// <param name="orderByType">排序类型</param>
        /// <returns></returns>
        public virtual T GetFirst(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderExpression, OrderByType orderByType)
        {
            return GetDbSet<T>().GetFirst(whereExpression, orderExpression, orderByType);
        }

        /// <summary>
        /// 根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="orderExpression">orderBy表达式</param>
        /// <param name="orderByType">排序类型</param>
        /// <returns></returns>
        public virtual async Task<T> GetFirstAsync(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderExpression, OrderByType orderByType)
        {
            return await Db.Queryable<T>().OrderBy(orderExpression, orderByType).Where(whereExpression).FirstAsync();
        }

        /// <summary>
        /// 根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual async Task<T> GetFirstAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().GetFirstAsync(whereExpression);
        }

        /// <summary>
        /// 根据where条件查询单条，多个返回第一条
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="orderExpression">orderBy表达式</param>
        /// <param name="orderByType">排序类型</param>
        /// <returns></returns>
        public virtual T GetFirst(string tableName, Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderExpression, OrderByType orderByType)
        {
            return Db.Queryable<T>().AS(tableName).OrderBy(orderExpression, orderByType).Where(whereExpression).First();
        }

        /// <summary>
        /// 根据where条件查询单条，超过一条会报错!!!
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual async Task<T> GetSingleAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().GetSingleAsync(whereExpression);
        }

        /// <summary>
        /// 异步根据主键查询
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public virtual async Task<T> GetByIdAsync(dynamic objId)
        {
            return await GetDbSet<T>().GetByIdAsync(objId);
        }

        /// <summary>
        /// 根据主键查询，可从缓存中查，不建议使用
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="blnUseCache"></param>
        /// <returns></returns>
        public virtual async Task<T> GetByIdAsync(dynamic objId, bool blnUseCache)
        {
            return await GetDbSet<T>().GetByIdAsync(objId, blnUseCache);
        }

        /// <summary>
        /// 根据主键集合查询
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual List<T> ListGetByIds(IEnumerable<dynamic> listId)
        {
            return GetDbSet<T>().GetByIds(listId);
        }

        /// <summary>
        /// 根据主键集合查询
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual List<T> ListGetByIds(string tableName, IEnumerable<dynamic> listId)
        {
            return GetDbSet<T>().GetByIds(tableName, listId);
        }

        /// <summary>
        /// 异步根据主键集合查询
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> ListGetByIdsAsync(IEnumerable<dynamic> listId)
        {
            return await GetDbSet<T>().GetByIdsAsync(listId);
        }

        #endregion

        #region Update

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Update(T model)
        {
            return GetDbSet<T>().UpdateWithOutNull(model);
        }

        public virtual bool UpdateSplitTable(T model)
        {
            return Db.Updateable(model).IgnoreColumns(ignoreAllNullColumns: true).SplitTable().ExecuteCommand() > 0;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="updateObjs">Db模型集合</param>
        /// <returns></returns>
        public virtual bool UpdateRangeSplitTable(IEnumerable<T> updateObjs)
        {
            return Db.Updateable(updateObjs.ToList()).IgnoreColumns(ignoreAllNullColumns: true).SplitTable().ExecuteCommand()>0;
        }


        /// <summary>
        /// 异步根据实体更新（主键要有值，主键是更新条件）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(T model)
        {
            return await GetDbSet<T>().UpdateAsync(model);
        }

        /// <summary>
        /// 根据表达式中的列更新
        /// </summary>
        /// <param name="columns">指定列表达式</param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual bool Update(Expression<Func<T, T>> columns, Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().Update(columns, whereExpression);
        }

        /// <summary>
        /// 根据表达式中的列更新
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns">指定列表达式</param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual bool Update(string tableName, Expression<Func<T, T>> columns,
            Expression<Func<T, bool>> whereExpression)
        {
            return Db.Updateable<T>().AS(tableName).SetColumns(columns).Where(whereExpression)
                .ExecuteCommand() > 0;
        }

        /// <summary>
        /// 异步根据表达式中的列更新
        /// </summary>
        /// <param name="columns">指定列表达式</param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(Expression<Func<T, T>> columns,
            Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().UpdateAsync(columns, whereExpression);
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件）
        /// </summary>
        /// <param name="updateObjs">Db模型集合</param>
        /// <returns></returns>
        public virtual bool UpdateRange(IEnumerable<T> updateObjs)
        {
            return GetDbSet<T>().UpdateRangeWithOutNull(updateObjs.ToList());
        }

        /// <summary>
        /// 异步根据实体更新（主键要有值，主键是更新条件）
        /// </summary>
        /// <param name="updateObjs">Db模型集合</param>
        /// <returns></returns>
        public virtual Task<int> UpdateRangeAsync(IEnumerable<T> updateObjs)
        {
            return GetDbSet<T>().UpdateRangeAsync(updateObjs);
        }

        #endregion

        #region 分页查询

        /// <summary>
        /// 列表查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="orderByType">排序枚举</param>
        /// <returns></returns>
        public virtual List<T> ListGet_SplitTable(Expression<Func<T, bool>> whereExpression, DateTime startTime, DateTime endTime
          )
        {
         
            List<T> result = Db.Queryable<T>().Where(whereExpression)
                .SplitTable(startTime, endTime).ToList();
            return result;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        public virtual List<T> ListPageGet_SplitTable(Expression<Func<T, bool>> whereExpression,DateTime startTime, DateTime endTime, int intPageIndex, int intPageSize,
            ref int totalCount)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = Db.Queryable<T>().Where(whereExpression).SplitTable(startTime, endTime).ToPageList(intPageIndex, intPageSize);
                
            totalCount = pageModel.TotalCount;
            return result;
        }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        public virtual List<T> ListPageGet(Expression<Func<T, bool>> whereExpression, int intPageIndex, int intPageSize,
            ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>> orderByExpression)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(whereExpression, pageModel,
                orderByExpression, orderByType);

            totalCount = pageModel.TotalCount;
            return result;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="whereExpression">where表达式</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        public virtual List<T> ListPageGet(string tableName, Expression<Func<T, bool>> whereExpression,
            int intPageIndex, int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>> orderByExpression)
        {
            return Db.Queryable<T>().AS(tableName).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .ToPageList(intPageIndex, intPageSize, ref totalCount);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="conditionalList">where表达式集合</param>
        /// <param name="intPageIndex">页码</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="totalCount">总数</param>
        /// <param name="orderByType">排序枚举</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <returns></returns>
        public virtual List<T> ListPageGet(List<IConditionalModel> conditionalList, int intPageIndex, int intPageSize,
            ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>> orderByExpression)
        {
            PageModel pageModel = new PageModel
            {
                PageIndex = intPageIndex,
                PageSize = intPageSize,
                TotalCount = totalCount
            };

            List<T> result = GetDbSet<T>().GetPageList(conditionalList, pageModel,
                orderByExpression, orderByType);

            totalCount = pageModel.TotalCount;
            return result;
        }

        #endregion

        #region 是否存在

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual bool IsAny(Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().IsAny(whereExpression);
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual bool IsAny(string tableName, Expression<Func<T, bool>> whereExpression)
        {
            return Db.Queryable<T>().AS(tableName).Where(whereExpression).Any();
        }

        /// <summary>
        /// 异步是否存在
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual async Task<bool> IsAnyAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().IsAnyAsync(whereExpression);
        }

        #endregion


        /// <summary>
        /// 合计
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual int Count(Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().Count(whereExpression);
        }

        /// <summary>
        /// 异步合计
        /// </summary>
        /// <param name="whereExpression">where表达式</param>
        /// <returns></returns>
        public virtual async Task<int> CountAsync(Expression<Func<T, bool>> whereExpression)
        {
            return await GetDbSet<T>().CountAsync(whereExpression);
        }

        /// <summary>
        /// 插入，返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Saveable(T model)
        {
            return GetDbSet<T>().Saveable(model);
        }

        /// <summary>
        /// 插入，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int SaveableReturnCommand(T model)
        {
            return GetDbSet<T>().SaveableReturnCommand(model);
        }

        /// <summary>
        /// 插入，返回类
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual T SaveableReturnEntity(T model)
        {
            return GetDbSet<T>().SaveableReturnEntity(model);
        }

        /// <summary>
        /// 根据主键判段是否存在，如果存在则更新，不存在则插入
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public virtual int Saveable(IEnumerable<T> models)
        {
            return GetDbSet<T>().Saveable(models);
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件），返回是否成功
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateColumns"></param>
        /// <returns></returns>
        public virtual bool Saveable(T model, Expression<Func<T, object>> updateColumns)
        {
            return GetDbSet<T>().Saveable(model, updateColumns);
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件）,返回受影响的行数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateColumns"></param>
        /// <returns></returns>
        public virtual int SaveableReturnCommand(T model, Expression<Func<T, object>> updateColumns)
        {
            return GetDbSet<T>().SaveableReturnCommand(model, updateColumns);
        }

        /// <summary>
        /// 根据实体更新（主键要有值，主键是更新条件）,返回受影响的行数
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateColumns"></param>
        /// <returns></returns>
        public virtual int SaveableReturnCommand(IEnumerable<T> model, Expression<Func<T, object>> updateColumns)
        {
            return GetDbSet<T>().SaveableReturnCommand(model, updateColumns);
        }

        /// <summary>
        /// 插入，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int InsertReturnCommand(T model)
        {
            return GetDbSet<T>().InsertWithOutNullReturnCommand(model);
        }

        /// <summary>
        /// 插入，返回影响行数
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int InsertReturnCommand(string tableName, T model)
        {
            return GetDbSet<T>().InsertWithOutNullReturnCommand(tableName, model);
        }

        /// <summary>
        /// 批量插入，返回影响行数
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public virtual int InsertRangeReturnCommand(IEnumerable<T> models)
        {
            return GetDbSet<T>().InsertRangeWithOutNullReturnCommand(models);
        }

        /// <summary>
        /// 批量插入，返回影响行数
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="models"></param>
        /// <returns></returns>
        public virtual int InsertRangeReturnCommand(string tableName, IEnumerable<T> models)
        {
            return GetDbSet<T>().InsertRangeWithOutNullReturnCommand(tableName, models);
        }

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int UpdateReturnCommand(T model)
        {
            return GetDbSet<T>().UpdateReturnCommand(model);
        }

        /// <summary>
        /// 批量更新，返回影响行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual int UpdateRangeReturnCommand(IEnumerable<T> model)
        {
            return GetDbSet<T>().UpdateRangeReturnCommand(model);
        }

        /// <summary>
        /// 更新，返回影响行数
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public virtual int UpdateRangeReturnCommand(Expression<Func<T, T>> columns,
            Expression<Func<T, bool>> whereExpression)
        {
            return GetDbSet<T>().UpdateRangeReturnCommand(columns, whereExpression);
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="ids"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> GetByIds<TResult>(IEnumerable<dynamic> ids,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().GetByIds(ids, expression);
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="ids"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> GetByIds<TResult>(string tableName, IEnumerable<dynamic> ids,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().GetByIds(tableName, ids, expression);
        }


        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListGet(whereExpression, expression);
        }

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex,
            int intPageSize, ref int totalCount, Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListPageGet(whereExpression, intPageIndex, intPageSize, ref totalCount, expression);
        }

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<TResult>(Expression<Func<T, bool>> whereExpression, int intPageIndex,
            int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>>? orderByExpression,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListPageGet(whereExpression, intPageIndex, intPageSize, ref totalCount, orderByType,
                orderByExpression, expression);
        }

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="conditionalList"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<TResult>(List<IConditionalModel> conditionalList, int intPageIndex,
            int intPageSize, ref int totalCount, OrderByType orderByType,
            Expression<Func<T, object>>? orderByExpression,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListPageGet(conditionalList, intPageIndex, intPageSize, ref totalCount, orderByType,
                orderByExpression, expression);
        }

        /// <summary>
        /// 求和
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="sumExpression"></param>
        /// <returns></returns>
        public virtual TResult Sum<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> sumExpression)
        {
            return GetDbSet<T>().Sum(whereExpression, sumExpression);
        }

        /// <summary>
        /// 最大
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="maxExpression"></param>
        /// <returns></returns>
        public virtual TResult Max<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> maxExpression)
        {
            return GetDbSet<T>().Sum(whereExpression, maxExpression);
        }

        /// <summary>
        /// 最小
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="minExpression"></param>
        /// <returns></returns>
        public virtual TResult Min<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> minExpression)
        {
            return GetDbSet<T>().Sum(whereExpression, minExpression);
        }

        /// <summary>
        /// 平均
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="avgExpression"></param>
        /// <returns></returns>
        public virtual TResult Avg<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, TResult>> avgExpression)
        {
            return GetDbSet<T>().Sum(whereExpression, avgExpression);
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<TResult>(Expression<Func<T, bool>> whereExpression,
            OrderByType orderByType, Expression<Func<T, object>> orderByExpression,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListGet(whereExpression, orderByType, orderByExpression, expression);
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGetOrderByAsc<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderByExpression, Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListGet(whereExpression, OrderByType.Asc, orderByExpression, expression);
        }

        /// <summary>
        /// 查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGetOrderByDesc<TResult>(Expression<Func<T, bool>> whereExpression,
            Expression<Func<T, object>> orderByExpression, Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListGet(whereExpression, OrderByType.Desc, orderByExpression, expression);
        }

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGetOrderByAsc<TResult>(Expression<Func<T, bool>> whereExpression,
            int intPageIndex, int intPageSize, ref int totalCount, Expression<Func<T, object>>? orderByExpression,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListPageGet(whereExpression, intPageIndex, intPageSize, ref totalCount,
                OrderByType.Asc, orderByExpression, expression);
        }

        /// <summary>
        /// 分页查询指定列
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="whereExpression"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGetOrderByDesc<TResult>(Expression<Func<T, bool>> whereExpression,
            int intPageIndex, int intPageSize, ref int totalCount, Expression<Func<T, object>>? orderByExpression,
            Expression<Func<T, TResult>> expression)
        {
            return GetDbSet<T>().ListPageGet(whereExpression, intPageIndex, intPageSize, ref totalCount,
                OrderByType.Desc, orderByExpression, expression);
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression,
            Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression)
        {
            return Db.Queryable(joinExpression).Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression,
            Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression)
        {
            return Db.Queryable(joinExpression).Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression,
            Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression,
            Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression,
            Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToPageList(pageIndex, pageSize, ref totalCount);
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<T1, T2, TResult>(Expression<Func<T1, T2, object[]>> joinExpression,
            Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToPageList(pageIndex, pageSize, ref totalCount);
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression,
            Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression)
        {
            return Db.Queryable(joinExpression).Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression,
            Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression)
        {
            return Db.Queryable(joinExpression).Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression,
            Expression<Func<T1, T2, T3, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression)
        {
            return Db.Queryable(joinExpression).Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression,
            Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression,
            Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public virtual List<TResult> ListGet<T1, T2, T3, TResult>(Expression<Func<T1, T2, T3, object[]>> joinExpression,
            Expression<Func<T1, T2, T3, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToList();
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<T1, T2, T3, TResult>(
            Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToPageList(pageIndex, pageSize, ref totalCount);
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<T1, T2, T3, TResult>(
            Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToPageList(pageIndex, pageSize, ref totalCount);
        }

        /// <summary>
        /// 联表查询
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="joinExpression"></param>
        /// <param name="whereExpression"></param>
        /// <param name="resultExpression"></param>
        /// <param name="orderByType"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public virtual List<TResult> ListPageGet<T1, T2, T3, TResult>(
            Expression<Func<T1, T2, T3, object[]>> joinExpression, Expression<Func<T1, T2, T3, bool>> whereExpression,
            Expression<Func<T1, T2, T3, TResult>> resultExpression, OrderByType orderByType,
            Expression<Func<T1, object>> orderByExpression,
            int pageIndex, int pageSize, ref int totalCount)
        {
            return Db.Queryable(joinExpression).OrderBy(orderByExpression, orderByType)
                .Where(whereExpression)
                .Select(resultExpression)
                .ToPageList(pageIndex, pageSize, ref totalCount);
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="model"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public virtual bool Update(T model, Expression<Func<T, T>> columns)
        {
            return GetDbSet<T>().Update(model, columns);
        }
        

        /// <summary>
        /// 批量更新指定列
        /// </summary>
        /// <param name="updateObjs"></param>
        /// <param name="columns"></param>+
        /// 
        /// <returns></returns>
        public virtual bool UpdateRange(IEnumerable<T> updateObjs, Expression<Func<T, T>> columns)
        {
            return GetDbSet<T>().UpdateRange(updateObjs, columns);
        }

        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="model"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public int UpdateReturnCommand(T model, Expression<Func<T, T>> columns)
        {
            return GetDbSet<T>().UpdateReturnCommand(model, columns);
        }

        /// <summary>
        /// 批量更新指定列
        /// </summary>
        /// <param name="model"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public int UpdateRangeReturnCommand(IEnumerable<T> model, Expression<Func<T, T>> columns)
        {
            return GetDbSet<T>().UpdateRangeReturnCommand(model, columns);
        }

        /// <summary>
        /// 切换数据库
        /// </summary>
        public void ChangeDataBase(string baseKey)
        {
            if (!string.IsNullOrWhiteSpace(baseKey))
            {
                Db = new SqlSugarClient(DatabaseList.DbList.First(a => a.ConfigId == baseKey)
                    .ConnectionConfig);
            }
        }

        /// <summary>
        /// 有则更新无则插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回插入数量和更新属性</returns>
        public int[] AddOrUpdate(T model)
        {
            return GetDbSet<T>().AddOrUpdate(model);
        }

        /// <summary>
        /// 有则更新无则插入
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回插入数量和更新属性</returns>
        public int[] AddOrUpdate(List<T> model)
        {
            return GetDbSet<T>().AddOrUpdate(model);
        }

        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="t">数据表</param>
        /// <returns></returns>
        public int BulkCopy(List<T> t)
        {
            return GetDbSet<T>().BulkCopy(t);
        }


       public DbSet<T> GetDb()
        {
            return GetDbSet<T>();
        }


    }

    /// <summary>
    /// 业务抽象
    /// </summary>
    public abstract class AbstractBaseService : IBaseService
    {
    }
}