import request from './request'

// 请求服务
class AjaxService {
  // 构造函数
  constructor(baseUrl) {
    this.baseUrl = baseUrl
  }

  // get
  get(module, action, params) {
    return request({
      url: `${this.baseUrl}/${module}/${action}`,
      method: 'get',
      params: params
    })
  }

  // post
  post(module, action, params) {
    return request({
      url: `${this.baseUrl}/${module}/${action}`,
      method: 'post',
      data: params
    })
  }

  // delete
  delete(module, action, params) {
    return request({
      url: `${this.baseUrl}/${module}/${action}`,
      method: 'delete',
      data: params
    })
  }
}
export default AjaxService
