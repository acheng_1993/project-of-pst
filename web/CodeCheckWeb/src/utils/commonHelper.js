

/**
 * 获取地址栏参数值
 * @param {参数名称} name 
 */
 export function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.href.substr(window.location.href.indexOf('?')+1).match(reg);
    if (r != null) return unescape(decodeURIComponent(r[2])); return null;
}




/**
    * div 内容自动向上滚动
   */
export function divAutoScroll(id, intervalArr,obj) {
    var scrollSpeed = 40;//值越大，滚动的越慢
    var son = document.getElementById(id);
    var parentNode = son.parentNode;
    if (son.offsetHeight < parentNode.offsetHeight) {
        return;
    }
    // var copySon = document.createElement("div");
    // copySon.innerHTML = son.innerHTML;
    // insertAfter(copySon, son);
    //parseInt(getStyle(parentNode, 'paddingTop')) + parseInt(getStyle(parentNode, 'marginTop'))
     function ScrollMarquee() {
        if (son.offsetHeight - (parentNode.scrollTop + parentNode.clientHeight) <= 10) {
            parentNode.scrollTop -= son.offsetHeight;
        }

        else {

            parentNode.scrollTop++;

        }

    }
     function getStyle(obj, name) {
        if (obj.currentStyle) {
            return obj.currentStyle[name];
        } else {
            return getComputedStyle(obj, false)[name];
        }
    }
    if(intervalArr){
        intervalArr.forEach(x => {
            clearInterval(x);
        });
    }
    var ScrollTime = setInterval(ScrollMarquee, scrollSpeed);
    if (intervalArr) {
        intervalArr.push(ScrollTime);
    }
    parentNode.onmouseover =  function () {
        intervalArr.forEach(x => {
            clearInterval(x);
        });
        if(obj)
        obj.isStop = true;
        // clearInterval(intervalArr); 
    };//鼠标放在上面停止滚动
    parentNode.onmouseout =  function () {
        ScrollTime = setInterval(ScrollMarquee, scrollSpeed);
        if (intervalArr) {
            intervalArr.push(ScrollTime);
        }
        if(obj)
        obj.isStop = false;
    };//鼠标离开，重新滚动
    return ScrollTime;
}
/**
 * 插入节点到目标节点后面
*/
export function insertAfter(newElement, targetElement) { // newElement是要追加的元素 targetElement 是指定元素的位置
    var parent = targetElement.parentNode; // 找到指定元素的父节点
    if (parent.lastChild == targetElement) { // 判断指定元素的是否是节点中的最后一个位置 如果是的话就直接使用appendChild方法
        parent.appendChild(newElement, targetElement);
    } else {
        parent.insertBefore(newElement, targetElement.nextSibling);
    }
}

/**
*填充字符串 direc(left or right),v(填充值 default:0),len(填充长度)
*/
export function padStr(value, len, direc, pValue) {
    try {
        pValue = pValue == null ? '0' : pValue;
        var v = value.toString();
        if (v != null && v.length < len) {
            var pStr = '';
            for (var i = 0; i < len - v.length; i++) {
                pStr += pValue;
            }
            if (direc == 'left') {
                v = pStr + v;
            } else {
                v = v + pStr;
            }
        }
        return v;
    } catch (e) {
        return value;
    }
}

export function fontSize(res) {
    let docEl = document.documentElement,
        clientWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (!clientWidth) return;
    let fontSize = 100 * (clientWidth / 1920);
    return res * fontSize;

}



export function formatNum(strNum) {
    if (strNum.length <= 3) {

        return strNum;

    }

    if (!/^(\+|-)?(\d+)(\.\d+)?$/.test(strNum)) {

        return strNum;

    }

    var a = RegExp.$1, b = RegExp.$2, c = RegExp.$3;

    var re = new RegExp();

    re.compile("(\\d)(\\d{3})(,|$)");

    while (re.test(b)) {

        b = b.replace(re, "$1,$2$3");

    }

    return a + "" + b + "" + c;

}

// Date.prototype.Format = function (fmt) { //author: meizz 
//         var o = {
//             "M+": this.getMonth() + 1, //月份 
//             "d+": this.getDate(), //日 
//             "h+": this.getHours(), //小时 
//             "m+": this.getMinutes(), //分 
//             "s+": this.getSeconds(), //秒 
//             "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
//             "S": this.getMilliseconds() //毫秒 
//         };
//         if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
//         for (var k in o)
//         if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
//         return fmt;
//     }

    export function getQueryStringArgs(qs) {
        qs = decodeURIComponent(qs);
            //保存数据的对象
            var args = {},
     
            //取得每一项
            items = qs.length ? qs.split("&") : [],
            item = null,
            name = null,
            value = null,
            //在 for 循环中使用
            i = 0,
            len = items.length;
        //逐个将每一项添加到 args 对象中
        for(i = 0; i < len; i++) {
            item = items[i].split("=");
                name = decodeURIComponent(item[0]);
                value = decodeURIComponent(item[1]);
                if(name.length) {
                    args[name] = value;
                }
            }
        
            return args;
    }