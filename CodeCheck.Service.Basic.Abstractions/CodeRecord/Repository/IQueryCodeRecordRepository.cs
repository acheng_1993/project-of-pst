

//使用平台信息: ID:NETCORE_WebApi  描述:NETCORE_WebApi
//代码版本信息: ID:V3_1  描述:V3_1按业务模块生成(泛型主键),不向前兼容 添加时间:2024/2/20 14:22:04
using System.Collections.Generic;
using System.Threading.Tasks;
using CodeCheck.Service.Core;
using System.Runtime.CompilerServices;
///限定只能由固定的程序集访问
/// 限定仓储接口只能由对应的服务程序集使用
[assembly: InternalsVisibleTo(assemblyName: "CodeCheck.Service.Basic")]
[assembly: InternalsVisibleTo(assemblyName: "CodeCheck.Service.Repository")]
namespace CodeCheck.Service.Basic.Abstractions
{
    internal interface IQueryCodeRecordRepository : IQueryBaseRepository
    {
       
            /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<QueryPagedResponseModel<T>> GetListPagedAsync<T>(int page, int size, BaseCodeRecordCondition condition = null, string field = null, string orderBy = null);


        /// <summary>
        /// 不分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="condition"></param>
        /// <param name="field"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync<T>(BaseCodeRecordCondition condition, string field = null, string orderBy = null);


            

        
            

        
            

        
            

        
            

        
            

        
            

        
                }
}
