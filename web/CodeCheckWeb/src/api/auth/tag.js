import AjaxService from '@/utils/ajaxService'
import Vue from 'vue'

const tagRouter = 'Tag'

const ajaxService = new AjaxService(Vue.prototype.BaseConfig.authApiUrl)

class Tag {
  getTagList(query) {
    return ajaxService.get(tagRouter, 'GetList', query)
  }
  getTagListPaged(query) {
    return ajaxService.get(tagRouter, 'GetListPaged', query)
  }
  createTag(model) {
    return ajaxService.post(tagRouter, 'insert', model)
  }
  updateTag(model) {
    return ajaxService.post(tagRouter, 'UpdateById/' + model.id, model)
  }
  deleteTag(model) {
    return ajaxService.delete(tagRouter, 'delete', model.id)
  }
}

export default Tag
