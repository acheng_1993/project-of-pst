﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace CodeCheck.Service.Core
{
    public class DbConnectionModel
    {
        public string ConnectionString { set; get; }
        public string DatabaseType { set; get; }
        public string ConfigId { set; get; }
    }

    /// <summary>
    /// 串口设置
    /// </summary>
    public class SerialPortConfig { 
        
        /// <summary>
        /// 串口号
        /// </summary>
        public string ComPort { set; get; }

        /// <summary>
        /// 波特率
        /// </summary>
        public int Baudrate { set; get; }

        public int DataBits { set; get; }

        public int Parity { set; get; }

        public int StopBit { set; get; }
    }

    /// <summary>
    /// 服务器配置类
    /// </summary>
    public class AppsettingsConfig
    {
        public static SerialPortConfig SerialPortConfig { set; get; }
        public static ServiceProvider ServiceProvider { set; get; }
        
               public static string WarnTagName { set; get; }
        public static string DcsApi { set; get; }
        public static List<DbConnectionModel> DBList { set; get; }
        /// <summary>
        /// 默认数据库连接
        /// </summary>
        public static string DefaultConnectionString { get; set; } = "";

        /// <summary>
        /// 数据库类型SQLSERVER，MYSQL,ORACLE
        /// </summary>
        public static string DatabaseType { get; set; } = "SQLSERVER";

        /// <summary>
        /// 文件路径
        /// </summary>
        public static string ReadFilePath { set; get; }
        public static int ReadInterval { set; get; } = 500;
        /// <summary>
        /// 生产主键机器码
        /// </summary>
        public static int? MachineId { get; set; } = 1;

        /// <summary>
        /// 生产主键数据中心id
        /// </summary>
        public static int? DataCenterId { get; set; } = 1;





        /// <summary>
        /// ApiService 配置
        /// </summary>
        public static List<ServiceApiHostModel> ServiceApiHosts { get; set; }

    }

 




}
